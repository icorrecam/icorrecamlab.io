# Cosas  por hacer

## Urgente

### Tareas

### Publicaciones

- [x] Publicar el acta de la reunión del 28/07/2023
- [x] Publicar la infografía de Isaac.

#### Header

- [x] Añadir Home
- [ ] Añadir un «Acerca de…»
- [ ] Añadir una página dedicada al «Grupo de trabajo»
- [ ] …

#### Footer

- [ ] ¿Formulario de Contacto?
- [ ] Aviso Legal
- [ ] ¿Suscripciones?[^suscripcion]
- [ ] Añadir una referencia a «Política de Cookies y Privacidad»[^cookies]
- [ ] Añadir la [licencia](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES) a la web.
- [ ] Añadir log de la campaña [not AI-generated](https://notbyai.fyi/)

### Programación

- [ ] Añadir la opción de activar comentarios.
  <!-- trunk-ignore(trufflehog/Gitlab) -->
  - Staticman
    - [Setting up Staticman comments on my Jekyll GitLab page](https://gitlab.com/nithiya/nithiya.gitlab.io/-/blob/main/_posts/2020-12-25-staticman-jekyll-gitlab.md?ref_type=heads)
    - [Getting started](https://staticman.net/docs/getting-started.html)
    - [Docker](https://hub.docker.com/r/arhef/staticman)
- [x] Anonimizar las publicaciones. Usar un usuario ficiticio para todas las
  publicaciones.
- [ ] ¿Exportación a PDF?
  - De momento queda pendiente, porque `jekyll-pdf` rompería los procesos `CI` (integración continua) :-/

## Opcionales

- [ ] Comprobar este plugin: [jekyll-preview-tag](https://stackoverflow.com/questions/57493239/opengraph-link-previews-in-jekyll)

## Documentación

### img, srcset, picture

- [How TO - Typical Device Breakpoints](https://www.w3schools.com/howto/howto_css_media_query_breakpoints.asp)
- [What Img Srcset Does In HTML5: A Quick & Simple Guide](https://html.com/attributes/img-srcset/)
- [How to Display Different Images Based on Screen Sizes in HTML](https://webtips.dev/webtips/html/different-images-based-on-screensize-in-html)
- [How to Use HTML5 “picture”, “srcset”, and “sizes” for Responsive Images](https://webdesign.tutsplus.com/quick-tip-how-to-use-html5-picture-for-responsive-images--cms-21015t)

### Cookies

- [Cookie consent](https://jekyllcodex.org/without-plugin/cookie-consent/)
- [Cookie consent, a how to guide](https://www.chuvisco.me/technology/2019/07/01/cookie-consent/)
- [cookies_eu](https://github.com/infinum/cookies_eu)
- [GDPR, Cookies, etc](https://peateasea.de/enabling-cookie-consent-with-jekyll-minimal-mistakes/)
- …

----

[^suscripcion]: [How to add a subscriber pop-up in your Jekyll blog](https://alexmanrique.com/blog/development/2020/08/01/how-to-add-subscriber-popup-jekyll.html)
[^cookies]: En este momento no hace falta, ya que no estamos usando cookies.
