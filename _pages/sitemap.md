---
layout: archive
title: Mapa de este sitio
permalink: /sitemap/
author_profile: false
toc: true
toc_label: Publicaciones
toc_icon: cog
toc_sticky: true
---

Una lista con todos los tipos de publicaciones de este sitio.

Para los visitantes no humanos, existe una [versión XML]({{ '/sitemap.xml' | relative_url }}) disponible para su 'degustación'.

## Páginas

{% for post in site.pages -%}
  {% include archive-single.html %}
{%- endfor -%}

## Posts

{% for post in site.posts -%}
  {% include archive-single.html %}
{%- endfor -%}

{% capture written_label -%}'None'{%- endcapture -%}

## Publicaciones agrupadas

{% for collection in site.collections -%}
{%- unless collection.output == false or collection.label == "posts" -%}
  {% capture label -%}{{ collection.label }}{%- endcapture -%}
  {%- if label != written_label -%}

### Publicaciones sobre «{{ label }}»

  {% capture written_label -%}{{ label }}{%- endcapture -%}
  {%- endif -%}
{%- endunless -%}

{%- for post in collection.docs -%}
  {%- unless collection.output == false or collection.label == "posts" -%}
  {%- include archive-single.html -%}
  {%- endunless -%}
{%- endfor -%}
{%- endfor -%}