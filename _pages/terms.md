---
permalink: /terms/
title: "Terms and Privacy Policy"
last_modified_at: 2016-06-06
toc: true
---

Revisar antes de publicar
{: .notice-danger}

## Política de privacidad

La privacidad de mis visitantes es extremadamente importante. Esta Política de Privacidad describe los tipos de información personal que se recibe y recopila y cómo se utiliza.

Ante todo, nunca compartiré su dirección de correo electrónico ni ninguna otra información personal con nadie sin su consentimiento directo.

### Archivos de registro

Como muchos otros sitios web, este sitio utiliza archivos de registro para ayudar a conocer cuándo, desde dónde y con qué frecuencia fluye el tráfico hacia este sitio. La información de estos archivos de registro incluye:

* Direcciones de protocolo de Internet (IP)
* Tipos de navegador
* Proveedor de servicios de Internet (ISP)
* Fecha y hora
* Páginas de referencia y de salida
* Número de clics

Toda esta información no está vinculada a nada que sea personalmente identificable.

### Cookies y balizas web

Cuando visita este sitio se almacenan cookies de "conveniencia" en su ordenador al enviar un comentario para ayudarle a iniciar sesión más rápidamente en [Disqus](http://disqus.com) la próxima vez que deje un comentario.

Los anunciantes externos también pueden colocar y leer cookies en su navegador y/o utilizar balizas web para recopilar información. Este sitio no tiene acceso ni control sobre estas cookies. Debe consultar las respectivas políticas de privacidad de todos y cada uno de los servidores de anuncios de terceros para obtener más información sobre sus prácticas y cómo darse de baja.

Si desea desactivar las cookies, puede hacerlo a través de las opciones de su navegador web. Encontrará instrucciones al respecto en los sitios web de cada navegador.

#### Google Analytics

Google Analytics es una herramienta de análisis web que utilizo para ayudar a comprender cómo interactúan los visitantes con este sitio web. Informa sobre las tendencias del sitio web utilizando cookies y balizas web sin identificar a los visitantes individuales. Puede consultar la [Política de privacidad de Google Analytics](http://www.google.com/analytics/learn/privacy.html).

#### Google Adsense

Google Adsense, una red de marketing de afiliación de terceros, utiliza cookies para asegurarse de que recibo una comisión cuando usted compra un producto tras hacer clic en un enlace o banner publicitario que le lleva al sitio de uno de sus comerciantes. Puedes leer [Política de privacidad de Google Adsense](http://support.google.com/adsense/bin/answer.py?hl=en&answer=48182).

## Política de divulgación

Gano dinero en este sitio web a través de programas de afiliados. Si haces clic en un enlace de afiliado o en un banner publicitario y compras el producto, ayudas a mantener este sitio web porque me llevaré un porcentaje de esa venta.

Actualmente soy afiliado de Amazon y Google Adsense.

Lo que esto significa para ti:

* Me convertí en un afiliado para obtener ingresos hacia los costos de funcionamiento y mantenimiento de este sitio web. En los casos en que tengo control directo sobre los anuncios que se publican en este sitio web, sólo ofrezco productos que están directamente relacionados con el tema de este sitio web y productos en los que un lector/suscriptor tendría un verdadero interés o necesidad.
* No recomiendo ni recomendaré un producto sólo para ganar dinero.
* No dejo que la remuneración que recibo influya en el contenido, los temas, las publicaciones o las opiniones expresadas en este sitio web.
* Respeto y valoro demasiado a mis lectores como para escribir otra cosa que no sean mis opiniones y consejos genuinos y objetivos.

Al igual que este sitio web, mi Política de Divulgación es un trabajo en curso. A medida que evolucionen las fuentes de ingresos, también lo hará esta página.
