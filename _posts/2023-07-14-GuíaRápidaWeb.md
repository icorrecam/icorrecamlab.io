---
title:  Guía rápida para clonar esta web
description: Sobre esta web…
# author: Luis Cabrera
date:   2023-07-13 12:18:37 +0100
last_modified_at: 2023-07-15 16:55:00 +0100
locale: es
related: true
count_visit: true
excerpt: #
  Esta guía permite instalar este sitio web en máquinas Debian con GNU Linux.
header:
  teaser: /assets/images/page-header-teaser.png
show_date: true
show_time: true
# show_author: true
categories:
  - Jekyll
tags:
  - web
  - gitlab
  - git
---

## Porqué elegí este software

| Ventajas                                                                                     | Desventajas                                                              |
| -------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| ✔ La web final se carga rápido                                                              | ✘ Sin interfaz gráfica de usuario por defecto                            |
| ✔ No es necesario instalar y mantener una base de datos y un CMS                            | ✘ Tarda mucho en compilar los archivos                                   |
| ✔ Casi no tiene vulnerabilidades                                                            | ✘ Reducido número de temas y plugins disponibles                         |
| ✔ No necesita actualizarse regularmente                                                     | ✘ Comunidad pequeña                                                      |
| ✔ Gran margen de maniobra en la programación (puede elegirse libremente la GUI y el editor) | ✘ No incluye ningún programa de edición de imagen                        |
| ✔ No depende de ciertos formatos o herramientas extra                                       | ✘ No soporta los scripts del lado del servidor (p.ej., para formularios) |
| ✔ Alojamiento propio o en los servidores de GitHub/GitLab (gratis)                          |                                                                          |
| ✔ Cuenta con un servidor de desarrollo propio                                               |                                                                          |
| ✔ Control de versiones con Git                                                              |                                                                          |

## Instalación en local

Pasos para reproducir este proyecto en local

### Clonado del repositorio

```sh
git clone https://gitlab.com/icorrecam/icorrecam.gitlab.io.git
```

### Entorno virtual para python

```sh
cd icorrecam.gitlab.io

sudo apt install python3-venv -y

python3 -m venv .venv

source .venv/bin/activate

sudo apt install graphviz cabal-install -y

sudo apt install plantuml ruby-asciidoctor-kroki ruby-asciidoctor-plantuml -y

sudo curl -fsSL https://deb.nodesource.com/setup_curren  t.x | sudo -E bash -

sudo apt install nodejs -y
```

#### Módulos de Python necesarios

```sh
pip3 install blockdiag seqdiag nwdiag
pip3 install actdiag
pip install "Pillow<=9.5.0"
```

### Entorno de Node JS

```sh
sudo npm install -g mermaid.cli
sudo npm install -g nomnoml
sudo npm install -g state-machine-cat
sudo npm install -g vega-cli vega-lite
sudo npm install -g wavedrom-cli
sudo npm fund
sudo node -v
sudo npm -v
```

### Ruby: actualizar el sistema

```sh
sudo bundle install
```

**Nota**: Algunas gemas puede que sea necesario instalarlas manualmente[^1].

### Jekyll

```sh
bundle exec jekyll serve --livereload
```

----

En este momento, deberíamos tener `Jekyll` funcionando.

[^1]: FIjarse en los mensajes de error.
