---
# author: Luis Cabrera
title:  Cronograma para una Asamblea General Ordinaria
date:   2023-07-17 12:26:14 +0100
locale: es
related: true
count_visit: true
header:
  teaser: https://www.workmeter.com/wp-content/uploads/2022/07/cronograma-scaled-e1659610949214-1024x668.jpg
categories:
  - La Laboral
tags:
  - diagramas
  - gráficos
---

## Asambleas Generales Ordinarias

<!-- ![](/assets/images/cronograma-asamblea-general-ordinaria.svg)  -->
<!--
<figure>
 <a href="/assets/images/cronograma-asamblea-general-ordinaria.svg"><img src="/assets/images/cronograma-asamblea-general-ordinaria.svg"></a>
 <figcaption>Plazos, según los actuales estatutos, para organizar una Asambles General Ordinaria</figcaption>
</figure>
-->

{% include figure image_path="/assets/images/cronograma-asamblea-general-ordinaria.svg" alt="Plazos, según los estatutos del 2006, para organizar una Asambles General Ordinaria." caption="Plazos, según los estatutos del 2006, para organizar una Asambles General Ordinaria." %}