---
# author: Luis Cabrera
title:  Cronograma de una Convocatoria Electoral
date:   2023-07-17 12:26:14 +0100
locale: es
related: true
count_visit: true
header:
  teaser: https://www.workmeter.com/wp-content/uploads/2022/07/cronograma-scaled-e1659610949214-1024x668.jpg
categories:
  - La Laboral
tags:
  - diagramas
  - gráficos
---

## Convocatorias Electorales

<!-- ![](/assets/images/cronograma-convocatoria-electoral.svg) -->
<!--
<figure>
  <a href="/assets/images/cronograma-convocatoria-electoral.svg"><img src="/assets/images/cronograma-convocatoria-electoral.svg"></a>
  <figcaption>Plazos, según los estatutos del 2006, para organizar una Convocatoria Electoral.</figcaption>
</figure>
-->

{% include figure image_path="/assets/images/cronograma-convocatoria-electoral.svg"  alt="Plazos, según los estatutos del 2006, para organizar una Convocatoria Electoral." caption="Plazos, según los estatutos del 2006, para organizar una Convocatoria Electoral." %}
