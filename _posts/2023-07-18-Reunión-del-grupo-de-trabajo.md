---
title: "Reunión del grupo de trabajo"   # MODIFICAR
description: Reunión de trabajo, por videoconferencia   # MODIFICAR
# author: Luis Cabrera
# show_author: true
date:             2023-07-18T19:30:00+01:00     # MODIFICAR
last_modified_at: 2023-08-06T23:59:59+01:00     # MODIFICAR
show_date: true
show_time: true
locale: es
related: true
count_visit: true
excerpt: #
  Reunión del grupo de trabajo, por videoconferencia.
header:
  overlay_color: #FFFFFF
  overlay_filter: rgba(39,112,148,0.65)
  show_overlay_excerpt: false
  overlay_image: assets/images/reunion-de-trabajo.jpg
  teaser: assets/images/reunion-de-trabajo.jpg
  caption: "Créditos: [Reuniones de trabajo](https://www.pxfuel.com/es/search?q=reuniones+de+trabajo), via [pxfuel](https://www.pxfuel.com/)"
  # cta_url: URL
  # cta_label: LABEL
  # actions:
  # - label: "Documento original"
  #   url: /assets/files/ESTATUTOS-UNIVERSIDAD-LABORAL-2006.pdf
toc: true
toc_icon: cog
toc_sticky: true
categories:
  - La Laboral
tags:
  - videoconferencia
  - grupo de trabajo
  - reuniones
  - actas
---

## Reunión del grupo de trabajo  <!-- MODIFICAR, SI ES NECESARIO -->

<!--
Referencias:
 - https://sonix.ai/resources/es/como-recordar-los-minutos-de-la-reunion/
 - https://www.appvizer.es/revista/organizacion-planificacion/gestion-proyectos/como-redactar-un-acta-de-reunion

Organización del contenido de una plantilla par actas

- Datos iniciales (a figurar en el acta)
    - La fecha (y lugar) de la reunión
    - Los nombres de los asistentes (incluidos los invitados)
    - Los nombres de los miembros ausentes (¿necesario?)
    - Una llamada al orden que enumere la hora de inicio de la reunión y cuál es el objetivo general

- Inicio y desarrollo de la reunión**

    - Orden del día
        - Primer punto: es el indicado para contener apartados como
            - La validación del acta de la reunión anterior.
            - El recuento de tareas y sus resultados.
            - Las acciones que quedaron pendientes,
            - Otros aspectos que requieren validación por parte de los asistentes a la reunión.
        - Punto 2 del orden del día.

            Cada punto podría contener, inicialmente, información como la siguiente:

            - Resumen
            - Un (pro)ponente o encargado de presentar.
            - En su caso, las mociones presentadas.
            - Otros detalles (los que se consideren necesarios; referencias, documentación, etc).

            Una vez terminado el punto, la información final de dicho punto podría contener:

            - Resultado.
            - Acciones propuestas.
                - Acción
                - Encargado.

        - Puntos adicionales del orden del día
            -  Siguiendo el criterio mostrado en el desarrollo el punto anterior.
        - ...
        - Último punto del día.

              Si las reuniones no son periódicas, en este punto debería fijarse, antes que nada, la fecha y hora para la celebración de la siguiente reunión.

              Si así se considera, este podría ser un punto abierto a temas que no hayan entrado en el orden del día.

- Cierre de la reunión

- Datos de los que hay que dejar constancia tras la finalización de la reunión:
    - La hora de finalización de la reunión
    - Fecha y hora de la próxima reunión
    - Lista de los documentos distribuidos durante la reunión
    - Modificación o aceptación del acta de la reunión anterior
    - Mociones aceptadas o rechazadas
    - Resultados de las votaciones o elecciones

-->

### 1\. Datos identificativos

| --              | --               |
|**Localización** | Videoconferencia |
|**Fecha**        | 18/07/2023       |
|**Inicio**       | 19:30h           |
|**Fin**          | 21:25h           |

----

| Participantes             |
| :------------------------ |
| Javier Santana            |
| Dulce Auxiliadora         |
| Begoña Castellano         |
| Isaac Godoy               |
| José Miguel Canino        |
| Luis Cabrera [^moderador] |

----

### 2\. Puntos del orden del día

#### Punto 1.- Metodología

Sin entrar en profundidad, se reconoce que para poder avanzar, se necesita…  

* Información  
* Análisis  
* Toma de decisiones  

Interesa, pero igual es demasiado para la primera reunión.  

Se tiene en cuenta pero se prefiere empezar a comentar libremente los siguientes puntos.

Se deja pendiente para tratar más adelante.  

#### Punto 2.- Constitución de la Asociación o reanudar el tracto de la ya constituida

Comenzamos a comentar las diferentes alternativas, básicamente retomar la asociación actual o crear una nueva nueva partiendo desde 0.  

Isaac hace una presentación que intenta desglosar las opciones posibles, con sus pros y sus contras.  

Canino propone seguir la forma más ágil y dinámica para:  

* Buscar la forma más ágil y eficiente…
* Que agrupe a todos los alumnos, tanto los antiguos alumnos de la época de La Laboral, como a los nuevos alumnos del actual Felo Monzón…
* Crear una Comision de trabajo para sacar adelante lo anterior.

Los diferentes análisis conducen en la dirección de que la mejor opción es la creación de una nueva asociación, partiendo desde 0, pero también se quiere explorar las posibles opciones de mantener la asociación actual.

Se comenta que, de la asociación actual, lo único que interesa es el nombre, pero que incluso el nombre tiene problemas. Los estatutos de la asociación actual también tienen problemas serios y algunos artículos deberían volver a ser reescritos.

Isaac propone, en relación a la creación de la nueva asociación, ampliar la base de interesados en crearla, para ampliar los participantes en la junta fundacional

Luis aporta una web donde poder colgar documentación para poder ser consultada con tranquilidad.

Isaac comenta aportar la documentación presentada para que sea accesible al resto del grupo

#### Punto 3.- Convocatoria para la Asamblea General

En este punto, Isaac destaca la necesidad obvia de conseguir socios para la asociación.

Propone revisar una encuesta que ya tiene creada para presentársela a todos los participantes en el 50º aniversario.

Queda como trabajo pendiente.  

#### Punto 4.- Presentación de Isaac

La presentación se realizó durante el desarrollo del punto 2, ya que su contenido estaba muy relacionado con dicho punto.  

#### Punto final: Ruegos, preguntas y charla informal

Al final, se mantuvo un rato de charla que complementó lo hablado en puntos anteriores.  

Añado algunas matizaciones que ayudarán a entender mejor lo comentado durante la reunión:  

* Metodología: Hablamos sobre cómo mejorar el flujo de trabajo mediante la centralización de la información en un sitio web. Esta información incluiría:  
  * a) el acta de la reunión,  
  * b) los estatutos,  
  * c) los documentos elaborados por Isaac con análisis de opciones para actualizar la documentación de la asociación en el Registro de Asociaciones, y  
  * d) el modelo de encuesta también elaborado por Isaac para recopilar datos de los alumnos-socios.  
       Todo esto se organizaría en el sitio web para que el resto del grupo "Recuperar la Laboral" pueda consultar y mejorar antes de la próxima reunión.  

* También discutimos el procedimiento de trabajo para los próximos pasos, aunque no estoy seguro si se llegó a una conclusión concreta. Como sugerencia, Jose Miguel Canino propone lo siguiente para que lo consideremos y modifiquemos en la próxima reunión de trabajo:  

  * a) Pasos y calendario para obtener la documentación acreditativa de la asociación (elección de procedimiento, actualización o redacción de nuevos estatutos, etc.).  
  * b) Pasos y calendario para actualizar los datos de contacto de futuros socios y recopilar información a través de encuestas.  
  * c) Propuestas para ampliar el grupo de trabajo y asignar responsabilidades.

----

[^moderador]: Actua como moderador de la reunión.
