﻿---
title: Sobre las distintas opciones para disponer de una asociación de antigu@S alumn@S De La Universidad Laboral de Las Palmas y otras cuestiones relacionadas
description: Opciones existentes para la reactivación de una asociación de antiguos alumnos de la Universidad Laboral de Las Palmas
# author: Isaac Godoy
date:   2023-07-22 12:49:37 +0100
last_modified_at: 2023-08-05 23:59:59 +0100
locale: es
related: true
count_visit: true
excerpt: #
  Opciones existentes para la reactivación de una asociación de antiguos alumnos de la Universidad Laboral de Las Palmas.

header:
  overlay_color: "#9FBAC4"
  overlay_filter: 0.2     # rgba(255, 0, 0, 0.5)
  # overlay_image: /assets/images/quehacemosahora.jpg
  teaser: /assets/images/quehacemosahora.jpg
  # caption: "Créditos: [**IDG**](https://www.gutierrez-rubi.es/2020/08/03/diez-ventajas-de-las-videoconferencias/)"
  actions:
    - label: Presentación
      url: /assets/files/DEBATE-ASOCIACIÓN-UNIVERSIDAD-LABORAL.pdf

# header:
#   teaser: /assets/images/page-header-teaser.png
#   overlay_color: '#333'
#   #overlay_image: /assets/images/quehacemosahora.jpg
#   #caption: "Photo credit: [**depro**](https://deproconsultores.com/tomarte-un-tiempo-para-reflexionar-te-ayudara-a-ser-mas-productivo/)"


show_date: true
show_time: true
# show_author: true
categories:
  - La Laboral
tags:
  - asociación
  - fundación
  - procedimientos
  - decisiones
toc: true
# toc_label: Índice de contenidos
toc_icon: cog
toc_sticky: true
---

<!-- ## Sobre Las Distintas Opciones Para Disponer De Una Asociación De Antigu@S Alumn@S De La Universidad Laboral De Las Palmas Y Otras Cuestiones Relacionadas -->

### EL PUNTO DE PARTIDA

El punto de partida del proceso actual de debate y de toma de decisiones en torno a la necesidad de una Asociación de Alumnos de la Universidad Laboral de Las Palmas, independientemente de los que se hayan dado con anterioridad, y que intentan valorar las opciones de reactivar la creada en 2006 o de crear una nueva ha sido indiscutiblemente el intento de canalizar las emociones despertadas tras la celebración del encuentro del 50 aniversario y especialmente como reacción a los sentimientos surgidos ante la comparación entre el estado actual del espacio guardado en nuestros recuerdos y la nostalgia de las experiencias vividas en ese espacio y sus instalaciones en los tiempos en que cursamos estudios en él.

Todo ello activó una intensa descarga de energías que se plasmaron en ideas, propuestas e iniciativas entre las que destaca especialmente la constitución de una especie de grupo catalizador de dichas inquietudes en torno a varias ideas fuerza: recuperar la laboral; devolverle su esplendor; apoyar los proyectos de futuro, facilitar la existencia de una comunidad de marca "antiguos alumnos, etc.

Esas ideas fuerzas expresadas como objetivos pasaban además por establecer unas dinámicas de actuación que giraban sobre las prioridades que debían presidir y dirigir los esfuerzos para lograr la consecución **de una finalidad común "salvar la laboral"**.

Y en ese proceso adquiere cada día mas importancia el convencimiento de que para ello lo mas importante y necesario es contar con una Asociación de Antiguos Alumnos de la Laboral que vehiculizase las iniciativas tanto individuales como colectivas, las propuestas propias de los antiguos alumnos y las de quienes hoy dirigen los destinos de este centro que en el devenir de las últimas décadas ha perdido incluso su denominación inicial.

El debate se llena de preguntas sobre el para qué, el porqué, el cómo, el cuándo o el dónde, por lo que situamos esta reflexión en lo que parece haberse constituido en el centro del mismo: **LA CREACIÓN DE UNA ASOCIACIÓN**

### UNA CUESTIÓN PREVIA. FUNDACIÓN VS ASOCIACIÓN

Aunque para algunos pueda parecer que, dadas las propuestas e iniciativas aparecidas en los grupos de WhatsApp, en diferentes reuniones y conversaciones de distinto índole, sería más propio promover una FUNDACIÓN dado que lo pretendido es un fin ligado a una acción social que parte de la idea de coparticipar con el sector público en el sostenimiento y estímulo de actividades de interés general.

No obstante, como el propio preámbulo de la Ley de Fundaciones señala: "Mediante el negocio fundacional el fundador o fundadores manifiestan su voluntad de crear una fundación, dotándola de medios económicos".

Dado que todo ello conlleva la disposición de unos medios, unos tiempos y unos procesos de maduración de las emociones iniciales y su expresión en propuestas concretas poniendo a su disposición unos recursos sin establecer, que se verían dispuestos y afectados definitivamente al cumplimiento de unos fines de interés general sin ánimo de lucro aun por clarificar[^1].

Por todo ello, sin renunciar a una futura iniciativa en ese sentido, entendemos dada la experiencia y el momento que estamos compartiendo, en el que es importante aprovechar el potencial de los sentimientos despertados por la celebración del 50 aniversario de la creación de la Universidad Laboral de Las Palmas, dar respuesta a la voluntad de quienes con el fin de desarrollar organizadamente una serie de iniciativas, servir de soporte a una comunidad y constituir una solución de continuidad a la red de relaciones restablecidas entre el antiguo alumnado de la Laboral, gracias al trabajo de quienes hicieron posible dicho encuentro a través de **la puesta en marcha de una Asociación** de antigu@s alumn@s o del rescate de la creada en 2006 y ahora sin actividad.

Es, en suma, **un acto de pragmatismo**, que sin embargo debe valorar aun la oportunidad de cada decisión sobre el fondo y las formas, en el ánimo de hacer posible dicha asociación, en un plazo aceptable y cuya existencia responda si no a todas, si a la mayor parte de las inquietudes planteadas a lo largo del poco mas de un mes desde la celebración de dicho encuentro.

### PREGUNTAS Y RESPUESTAS

#### ¿PARA QUÉ CONTAR CON UNA ASOCIACIÓN?

Para dar salida a una inquietud y una necesidad mayoritaria de contar con un "algo" que permita a todos los que así se han manifestado, poder servir de espacio de encuentro, para mantener y restablecer contactos entre todos aquellos que así lo deseen, para organizar eventos, para hacer cosas, para ofertar actividades a sus componentes, pero también y sobre todo para satisfacer el sentimiento expr4esado por muchas de las personas que asistieron al encuentro de mantener el recuerdo de la Laboral, participar en su mejora y rehabilitación de forma tanto activa como pasiva, contribuir a su desarrollo y evolución desde una perspectiva tanto educativa como social, En suma por que entendemos que debemos actuar para lo que se ha denominado "recuperar la laboral" en su más amplia acepción.

#### ¿POR QUÉ FORMAR UNA ASOCIACIÓN?

Por qué es una estructura con personalidad jurídica válida para dinamizar a un colectivo amplio de personas que comparten un mismo fin, susceptible de acceder a recursos y de participar desde la sociedad civil en temas de interés general.

Pero sobre todo, porque para un colectivo como el nuestro, formado potencialmente por miles de personas que compartieron en algún momentos de los 20 o 50 años de su historia, según se mire, una experiencia común de desarrollo personal y social que necesita de una estructura estable, viable organizativamente y que se adapte a las posibilidades de sus componentes para mantenerla económicamente, para dinamizar actividades desde una dedicación y compromiso personal marcado por el voluntarismo y la disponibilidad de tiempo.

El resto de opciones, desde plataformas hasta fundaciones determinan bien ciertos niveles de inestabilidad en el tiempo o escasa capacidad de abordar la dinamización de tantas personas como ocurre con las plataformas o exigen afectar recursos patrimoniales y estructuras jurídico administrativas sujetas a cierta complejidad que, ahora mismo, superan nuestra capacidad.

### ¿CÓMO DOTARNOS DE UNA ASOCIACIÓN?

Dada la información que todos manejamos hay dos opciones que, además, determinan el cuando y el dónde: intentar rescatar y reactivar la asociación creada en 2006 e inactiva desde entonces (al menos a efectos de la administración) o crear una nueva asociación bien desde cero o aprovechando algunos de los elementos que como los estatutos o el nombre puedan considerarse útiles.

Para poder adoptar ante las opciones que se nos presentan quisiera realizar un análisis de dichas opciones:

#### OPCIÓN 1. RESCATAR Y REACTIVAR LA ASOCIACIÓN EXISTENTE

##### DOCUMENTACIÓN DISPONIBLE

- **ESTATUTOS**. Ha sido posible contar con una copia [^original] [^Estatutos] de los estatutos (de 2006) que posiblemente deba adaptarse al Reglamento de Asociaciones (2007) y otras posibles mejoras o actualizaciones

- **Certificado de domiciliación** (En el actual IES Felo Monzón Grau Bassas)[^Domicilio]

- **Certificado de inscripción** en el registro de asociaciones (Incluye nº de registro y poco más)[^Inscripcion]

##### DOCUMENTACIÓN NO DISPONIBLE

- **LIBRO DE SOCIOS**. Posiblemente no se hizo dado los escasos socios iniciales (6 al parecer).

- **LIBRO DE ACTAS**. Ídem lo anterior y la falta de actividad.

- **ACTA FUNDACIONAL**. Donde se recogen los datos de los socios promotores y de la primera junta directiva

- **RESOLUCIÓN de la DG correspondiente** donde se reconoce a la asociación y se determina su inscripción en el registro de asociaciones (Posiblemente se enviaría al domicilio social de la asociación, es decir al Felo Monzón)

##### OPCIÓN 1A

1. Que un grupo de personas se dirijan a la actual directiva solicitando hacerse socios.

2. Que la actual Junta en el uso de las atribuciones que les otorgan sus estatutos proceda a admitir como socios a ese grupo de personas

3. Que la actual Junta convoque una Asamblea General en donde se den cuenta de las altas y bajas, se realice un balance de situación a los efectos de justificar la interrupción del tracto sucesivo y procedan a dar cumplimiento a los estatutos eligiendo a una nueva Junta Directiva conforme a los mismos.

4. Se podría acordar en primera instancia la reforma de los estatutos en aquellos aspectos que se acuerden previamente.

5. Una vez elegida la junta se debe comunicarse a la administración y esperar su resolución

6. Si para entonces no aparecen los documentos que faltan se debería pedir un duplicado a la Dirección General.

##### OPCIÓN 1B

Lo mismo, pero abriendo un periodo más largo de captación de socios. Para luego proceder a la reactivación de las Asociación existente.

Abrir una inscripción online a todos los interesados y durante unos treinta días mantener abierto el proceso para luego llevar a cabo los puntos del 1 al 6

#### OPCIÓN 2. CREAR UNA NUEVA ASOCIACIÓN PARTIENDO DE CERO

- **Abrir un proceso constituyente a través de un formulario de inscripción como socios**

- **Mantener el proceso abierto durante un mes**

- **Convocar un acto constituyente** donde:

1. se aprobarían unos estatutos (se podría partir del modelo simplificado e incluir algunos aspectos de los de la asociación existente adelgazando y adecuando a la norma algunos de sus artículos)

2. se nombraría UNA **JUNTA DIRECTIVA** nueva paritaria y representativa de épocas y visiones

3. se redactaría el **ACTA FUNDACIONAL** y se comunicaría a la DG correspondiente a efectos de inscripción e inclusión en el registro de asociaciones

4. Con esa documentación (copias selladas por la DG): certificado de registro y número de inscripción, estatutos sellados etc., los cargos habilitados para ello procederían a **solicitar un CIF** y con el la apertura de **una cuenta bancaria** a los efectos establecidos

##### NECESIDADES Y DECISIONES

**ÁMBITO**. Sólo antiguo alumnado de la Universidad laboral o también del IES Felo Monzón

**NOMBRE Y DENOMINACIÓN**. La creación de una nueva asociación de antiguos alumnos choca con la existencia de una con la misma denominación (Puede buscarse otra denominación, ponerle un nombre "*LADERAS DEL GUINIGUADA*" y una denominación).

También se podía solicitar (por parte de los socios de la anterior) **su disolución** dejando libre la denominación

**FORMULARIO**. Típico formulario Google en el que inscribirse y dejar datos personales a los para formar parte de la asociación y a través del cual autorizar su uso a los efectos del cumplimiento de los fines de la asociación, especialmente localización, información y comunicación entre asociados

----

[^1]: De la [Ley Canaria de Fundaciones](https://derechodelacultura.org/wp-content/uploads/2018/06/LEY-2-DE-1998-CANARIAS.pdf). Artículo 1. Concepto. A los efectos de esta ley, son fundaciones las personas jurídicas resultantes de afectar permanentemente por voluntad de sus fundadores un patrimonio al cumplimiento de fines de interés general sin ánimo de lucro.

[^original]: [Copia de los estatutos originales del 2006](/assets/files/ESTATUTOS-UNIVERSIDAD-LABORAL-2006.pdf).

[^Estatutos]: Se ha realizado una transcripción de estos estatutos, estando disponibles en el siguiente enlace: [Estatutos de la Asociación de Antiguos Alumnos de la Universidad Laboral de Las Palmas](https://icorrecam.gitlab.io/la%20laboral/Estatutos-2006/).

[^Domicilio]: CARRETERA DEL LOMO BLANCO, Nº 48 - IES FELO MONZÓN GRAU BASSAS, ANTIGUA UNIVERSIDAD LABORAL DE LAS PALMAS, código postal, del término municipal de LAS PALMAS DE GRAN CANARIA.
    
    El documento oficial (pdf) se puede descargar al realizar la consulta en el [buscador de asociaciones](https://www3.gobiernodecanarias.org/entidadesjuridicas/asociaciones/), buscando por «Universidad Laboral» o directamente, en json, al pinchar esta [otra consulta](https://www3.gobiernodecanarias.org/entidadesjuridicas/asociaciones/api/asociacionesSearch?page=0&showAllElements=false&orderBy=nci&orderSort=asc&nombre=Universidad%20Laboral).

[^Inscripcion]: Número Canario de Inscripción 13783 (G1/S1/13783-06/GC).
    
    El documento oficial (pdf) se puede descargar al realizar la consulta en el [buscador de asociaciones](https://www3.gobiernodecanarias.org/entidadesjuridicas/asociaciones/), buscando por «Universidad Laboral» o directamente, en json, al pinchar esta [otra consulta](https://www3.gobiernodecanarias.org/entidadesjuridicas/asociaciones/api/asociacionesSearch?page=0&showAllElements=false&orderBy=nci&orderSort=asc&nombre=Universidad%20Laboral).

*[DG]: Dirección General
*[pdf]: Portable Document Format (Formato portable de documentos)
