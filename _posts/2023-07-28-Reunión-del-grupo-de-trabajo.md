---
title: "Reunión del grupo de trabajo"   # MODIFICAR
description: Reunión de trabajo, por videoconferencia   # MODIFICAR
# author: Luis Cabrera
# show_author: true
date:             2023-07-28T20:00:00+01:00     # MODIFICAR
last_modified_at: 2023-08-06T23:59:59+01:00     # MODIFICAR
show_date: true
show_time: true
locale: es
related: true
count_visit: true
excerpt: #
  Reunión del grupo de trabajo, por videoconferencia.
header:
  overlay_color: #FFFFFF
  overlay_filter: rgba(39,112,148,0.65)
  show_overlay_excerpt: false
  overlay_image: assets/images/reunion-de-trabajo.jpg
  teaser: assets/images/reunion-de-trabajo.jpg
  caption: "Créditos: [Reuniones de trabajo](https://www.pxfuel.com/es/search?q=reuniones+de+trabajo), via [pxfuel](https://www.pxfuel.com/)"
  # cta_url: URL
  # cta_label: LABEL
  # actions:
  # - label: "Documento original"
  #   url: /assets/files/ESTATUTOS-UNIVERSIDAD-LABORAL-2006.pdf
toc: true
# toc_label: Índice de contenidos
toc_icon: cog
toc_sticky: true
categories:
  - La Laboral
tags:
  - videoconferencia
  - grupo de trabajo
  - reuniones
  - actas
---

## Reunión del grupo de trabajo  <!-- MODIFICAR, SI ES NECESARIO -->

<!--
Referencias:  
 - https://sonix.ai/resources/es/como-recordar-los-minutos-de-la-reunion/
 - https://www.appvizer.es/revista/organizacion-planificacion/gestion-proyectos/como-redactar-un-acta-de-reunion

Organización del contenido de una plantilla par actas
  
- Datos iniciales (a figurar en el acta)
    - La fecha (y lugar) de la reunión
    - Los nombres de los asistentes (incluidos los invitados)
    - Los nombres de los miembros ausentes (¿necesario?)
    - Una llamada al orden que enumere la hora de inicio de la reunión y cuál es el objetivo general  

- Inicio y desarrollo de la reunión**  

    - Orden del día  
        - Primer punto: es el indicado para contener apartados como  
            - La validación del acta de la reunión anterior.
            - El recuento de tareas y sus resultados.
            - Las acciones que quedaron pendientes,
            - Otros aspectos que requieren validación por parte de los asistentes a la reunión.
        - Punto 2 del orden del día.    

            Cada punto podría contener, inicialmente, información como la siguiente:  

            - Resumen
            - Un (pro)ponente o encargado de presentar.
            - En su caso, las mociones presentadas.
            - Otros detalles (los que se consideren necesarios; referencias, documentación, etc).

            Una vez terminado el punto, la información final de dicho punto podría contener:  

            - Resultado.
            - Acciones propuestas.
                - Acción
                - Encargado.

        - Puntos adicionales del orden del día
            -  Siguiendo el criterio mostrado en el desarrollo el punto anterior.
        - ...  
        - Último punto del día.  

              Si las reuniones no son periódicas, en este punto debería fijarse, antes que nada, la fecha y hora para la celebración de la siguiente reunión.  

              Si así se considera, este podría ser un punto abierto a temas que no hayan entrado en el orden del día.  

- Cierre de la reunión

- Datos de los que hay que dejar constancia tras la finalización de la reunión:
    - La hora de finalización de la reunión
    - Fecha y hora de la próxima reunión
    - Lista de los documentos distribuidos durante la reunión
    - Modificación o aceptación del acta de la reunión anterior
    - Mociones aceptadas o rechazadas
    - Resultados de las votaciones o elecciones

-->  

### 1\. Datos identificativos

| --              | --               |
|**Localización** | Videoconferencia |
|**Fecha**        | 28/07/2023       |
|**Inicio**       | 20:00h           |
|**Fin**          | 21:10h           |

----

| Participantes             |
| :------------------------ |
| Isaac Godoy               |
| José Miguel Canino        |
| Javier Santana            |
| Luis Cabrera [^moderador] |

----

### 2\. Puntos del orden del día

#### Punto 1.- Aprobación del acta anterior y resumen de tareas pendientes

Acordamos:

- Añadir una encuesta de 4 puntos[^puntos] (que pasaremos primero por aquí[^aqui]) a lo que ya está escrito y publicado en la web.
- Que cuando la encuesta anterior esté probada y aprobada, (rápido, en no más de 24h desde que la comencemos a probar), publicaremos una nota en el grupo «Recuperemos…» explicando todo lo que hemos hecho hasta ahora.
- Canino comentaba la opción de poder añadir comentarios en cada publicación que se haga en la web. «Se puede hacer, pero tengo que mirar el como…» (luis)

#### Punto 2.- Siguiente paso administrtivo a dar…

La encuesta que hemos decidido incluir en el punto anterior, deberá servir para aclararnos cual debería ser el siguiente «paso administrativo» a dar.

La encuesta no tiene fecha de finalización (entre otras cosas por las fechas en las que estamos), pero debe servirnos para obtener una primera valoración para la siguiente reunión que hagamos con posterioridad a su publicación.

#### Punto 3.- Petición de voluntarios para este grupo de trabajo

Se acordó que cada vez que publicitemos las reuniones que haremos con este grupo de trabajo, animemos a la gente a participar en ella.

#### Punto 4.- Petición del orden del día y convocatorias del grupo de trabajo

Acordamos hacer reuniones de periodicidad semanal, los viernes, a las 20:00h (hora canaria, por supuesto).

El orden del día se puede ir sugiriendo en los días previos.

Me permito una sugerencia: comenzar las reuniones revisando tareas pendientes o informando de tareas realizadas, y terminar las reuniones con un punto abierto a cualquier tipo de cuestión (Luis).

#### Punto final: ruegos y preguntas

En este punto nos permitimos un poco de relax y terminamos la reunión.

----

[^moderador]: Actua como moderador de la reunión.
[^puntos]: Al final, pasó de ser un cuestionario con 4 preguntas, a dos encuestas individuales.
[^aqui]: Hace referencia a que se pasen las encuestas primero por el grupo de trabajo antes de pasarlas al grupo «Recuperemos…»
