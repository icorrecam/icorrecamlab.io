---
title: "Reunión del grupo de trabajo"   # MODIFICAR
description: Reunión de trabajo, por videoconferencia   # MODIFICAR
# author: Luis Cabrera
# show_author: true
date:             2023-08-04T20:00:00+01:00     # MODIFICAR
last_modified_at: 2023-08-06T23:59:59+01:00     # MODIFICAR
show_date: true
show_time: true
locale: es
related: true
count_visit: true
excerpt: #
  Reunión del grupo de trabajo, por videoconferencia.
header:
  overlay_color: #FFFFFF
  overlay_filter: rgba(39,112,148,0.65)
  show_overlay_excerpt: false
  overlay_image: assets/images/reunion-de-trabajo.jpg
  teaser: assets/images/reunion-de-trabajo.jpg
  caption: "Créditos: [Reuniones de trabajo](https://www.pxfuel.com/es/search?q=reuniones+de+trabajo), via [pxfuel](https://www.pxfuel.com/)"
  # cta_url: URL
  # cta_label: LABEL
  # actions:
  # - label: "Documento original"
  #   url: /assets/files/ESTATUTOS-UNIVERSIDAD-LABORAL-2006.pdf
toc: true
# toc_label: Índice de contenidos
toc_icon: cog
toc_sticky: true
categories:
  - La Laboral
tags:
  - videoconferencia
  - grupo de trabajo
  - reuniones
  - actas
---

## Reunión del grupo de trabajo  <!-- MODIFICAR, SI ES NECESARIO -->

<!--
Referencias:  
 - https://sonix.ai/resources/es/como-recordar-los-minutos-de-la-reunion/
 - https://www.appvizer.es/revista/organizacion-planificacion/gestion-proyectos/como-redactar-un-acta-de-reunion

Organización del contenido de una plantilla par actas
  
- Datos iniciales (a figurar en el acta)
    - La fecha (y lugar) de la reunión
    - Los nombres de los asistentes (incluidos los invitados)
    - Los nombres de los miembros ausentes (¿necesario?)
    - Una llamada al orden que enumere la hora de inicio de la reunión y cuál es el objetivo general  

- Inicio y desarrollo de la reunión**  

    - Orden del día  
        - Primer punto: es el indicado para contener apartados como  
            - La validación del acta de la reunión anterior.
            - El recuento de tareas y sus resultados.
            - Las acciones que quedaron pendientes,
            - Otros aspectos que requieren validación por parte de los asistentes a la reunión.
        - Punto 2 del orden del día.    

            Cada punto podría contener, inicialmente, información como la siguiente:  

            - Resumen
            - Un (pro)ponente o encargado de presentar.
            - En su caso, las mociones presentadas.
            - Otros detalles (los que se consideren necesarios; referencias, documentación, etc).

            Una vez terminado el punto, la información final de dicho punto podría contener:  

            - Resultado.
            - Acciones propuestas.
                - Acción
                - Encargado.

        - Puntos adicionales del orden del día
            -  Siguiendo el criterio mostrado en el desarrollo el punto anterior.
        - ...  
        - Último punto del día.  

              Si las reuniones no son periódicas, en este punto debería fijarse, antes que nada, la fecha y hora para la celebración de la siguiente reunión.  

              Si así se considera, este podría ser un punto abierto a temas que no hayan entrado en el orden del día.  

- Cierre de la reunión

- Datos de los que hay que dejar constancia tras la finalización de la reunión:
    - La hora de finalización de la reunión
    - Fecha y hora de la próxima reunión
    - Lista de los documentos distribuidos durante la reunión
    - Modificación o aceptación del acta de la reunión anterior
    - Mociones aceptadas o rechazadas
    - Resultados de las votaciones o elecciones

-->  

### 1\. Datos identificativos

| --              | --               |
|**Localización** | Videoconferencia |
|**Fecha**        | 04/08/2023       |
|**Inicio**       | 20:00h           |
|**Fin**          | 22:00h           |

----

| Participantes             |
| :------------------------ |
| Javier Santana            |
| Dulce Auxiliadora         |
| Isaac Godoy               |
| José Miguel Canino        |
| Luis Cabrera [^moderador] |

----

### 2\. Puntos del orden del día

#### Punto 1.- Aprobación del acta anterior y resumen de tareas pendientes

Un resumen rápido, para situarnos.

Se hizo un resumen rápido y se habló de que no siempre podremos cumplir los plazos que nos intentamos marcar, porque todo depende del tiempo libre del que podamos disponer.

La voluntad sigue exactamente igual.

#### Punto 2.- Revisión y visto bueno para publicar la infografía

Todos la ven correcta.

Canino comenta de ponerla en la web.

Javier propone abrir la publicación de noticias al resto de grupos y Canino le responde que es mejor ser «más intensos» a partir de Septiembre.

Dulce aporta que, precisamente, por estar en fecha de vacaciones, igual la gente tiene más tiempo para estas cosas.

Isaac retoma el tema de la infografía y explica los cambios realizados entre las dos versiones de la infografía.

Comenta sobre la conveniencia de la presencia, o no, de los indicadores positivo/negativo en la infografía. Deja la pregunta abierta.

RESUMEN

- Publicar la infografía
- Acompañarlo con el texto concensuado.
- Se acuerda publicar la infografía en todos los grupos de whatsapp al cabo de una semana.
- Se le encarga a Luis la publicación de la infografía.

#### Punto 3.- Revisión y visto bueno para publicar las 2 encuestas

- Debate largo, con respecto a la elección de las preguntas a realizar.

- No se debatió la introducción a la publicación a las encuestas. Asumo el riesgo de enviarlo.

- Se concensuaron las preguntas y las opciones de las 2 encuestas.

- Se le encarga a Luis el hacer la publicación.

#### Punto 4.- Propuestas para ver qué pasos podemos ir avanzando para la siguiente reunión

Se habló poco (o nada) de este punto.

#### Punto final: ruegos y preguntas

- Se comenta de colgar el resumen de la reunión del 28 de Julio en la web.

----
[^moderador]: Actua como moderador de la reunión.
