---
title: "Infografía - reactivar vs crear"
description: #
  Infografía que trata de resumir las principales razones, a favor y en contra,
  sobre el tema de reactivar la actual asociación de ex-alumnos de la
  Universidad Laboral, frente a la opción de crear una nueva asociación
  completamente desde cero.
# author: Luis Cabrera
# show_author: true
date:             2023-08-04T20:00:00+01:00
last_modified_at: 2023-08-04T23:59:59+01:00
show_date: true
show_time: true
locale: es
related: true
count_visit: true
class: wide
excerpt: #
  Infografía que trata de resumir las principales razones, a favor y en contra,
  sobre el tema de reactivar la actual asociación de ex-alumnos de la
  Universidad Laboral, frente a la opción de crear una nueva asociación
  completamente desde cero.
header:
  # cta_url: URL
  # cta_label: LABEL
  overlay_color: #000000
  # overlay_filter: 0.1
  overlay_filter: rgba(0,175,255,0.3)
  show_overlay_excerpt: false
  overlay_image: assets/images/graficos.jpg
  teaser: assets/images/graficos.jpg
  caption: "Créditos: [Estadísticas](https://www.pxfuel.com/es/search?q=estad%C3%ADsticas), via [pxfuel](https://www.pxfuel.com/)"
  # actions:
  # - label: "Documento original"
  #   url: /assets/files/ESTATUTOS-UNIVERSIDAD-LABORAL-2006.pdf
categories:
  - La Laboral
tags:
  - diagramas
  - gráficos
  - infografías
---

## Reactivar la asociación de 2006, vs crear una asociación desde cero

<!-- ![](/assets/images/cronograma-asamblea-general-ordinaria.svg)  -->
<!--
<figure>
 <a href="/assets/images/cronograma-asamblea-general-ordinaria.svg"><img src="/assets/images/cronograma-asamblea-general-ordinaria.svg"></a>
 <figcaption>Plazos, según los actuales estatutos, para organizar una Asambles General Ordinaria</figcaption>
</figure>
-->

{% include figure image_path="/assets/images/infografia-isaac.webp" alt="Infografía informativa. Comparativa entre seguir con la antigua asociación, o crear una nueva, desde cero." caption="Infografía informativa. Comparativa entre seguir con la antigua asociación, o crear una nueva, desde cero." %}

