---
title: Borrador para nuevos estatutos
description: Adaptación al entorno web de la plantilla recomendada por el Gobierno de Canarias para la creación de nuevos estatutos para las asociaciones canarias. Sobre dicha plantilla estamos habilitando las propuestas que vamos haciendo...
# author: Luis Cabrera
author_profile: true
date: 2023-10-08 10:20:00 +0100
last_modified_at: 2023-10-09 21:35:00 +0100
locale: es
count_visit: true
related: true
excerpt: #
  Adaptación al entorno web de la plantilla recomendada por el Gobierno de Canarias para la creación de nuevos estatutos para las asociaciones canarias.
tagline: #
  Adaptación al entorno web de la plantilla recomendada por el Gobierno de Canarias para la creación de nuevos estatutos para las asociaciones canarias.
header:
  overlay_color: "#FFF"
  overlay_filter: 0.3
  overlay_image: https://focse.es/wp-content/uploads/2016/03/titulo-estatutos.jpg
  teaser: https://focse.es/wp-content/uploads/2016/03/titulo-estatutos.jpg
  caption: "Créditos: [**FOCSE**](https://focse.es/estatutos-de-la-focse/)"
  actions:
   - label: "Versión PDF"
     url: /assets/files/2023-10-08-Plantilla-nuevos-estatutos.pdf

show_date: true
show_time: true
# show_author: true
categories:
  - La Laboral
tags:
  - estatutos
  - transcripción
toc: true
# toc_label: Índice de contenidos
toc_icon: cog
toc_sticky: true
---


<!--
From ODT to markdown
pandoc --from odt --to markdown_phpextra --standalone --embed-resources --citeproc --wrap=none --no-highlight
-->

<div><span class="notice--danger">EL PRESENTE MODELO ES ÚNICAMENTE ORIENTATIVO. POR TANTO NO ES OBLIGATORIA SU UTILIZACIÓN PERO LE FACILITARÁ CUMPLIMENTAR SUS ESTATUTOS SOCIALES.</span></div>

----

**AS 102** [MODELO ORIENTATIVO DE ESTATUTOS DE UNA ASOCIACIÓN SIN ÁNIMO DE LUCRO][0]

----

**POR FAVOR LEA DETENIDAMENTE LAS SIGUIENTES INSTRUCCIONES**

- EL PRESENTE MODELO ES ÚNICAMENTE ORIENTATIVO. POR TANTO NO ES OBLIGATORIA SU UTILIZACIÓN PERO LE FACILITARÁ CUMPLIMENTAR SUS ESTATUTOS SOCIALES.
- SI TIENE DUDAS RESPECTO DE LOS REQUISITOS PARA CONSTITUIR UNA ASOCIACIÓN POR FAVOR CONSULTE LA PÁGINA WEB [_www.gobiernodecanarias.org/entidadesjuridicas/Servicios/asociaciones_][1] DONDE ENCONTRARÁ FOLLETOS INFORMATIVOS QUE LE AYUDARÁN A RESOLVER SUS DUDAS.
- EN ESTE DOCUMENTO USTED ENCONTRARÁ ZONAS SOMBREADAS EN VERDE QUE DEBE RELLENAR. ~~PARA FACILITARLES LA LABOR ENCONTRARÁ ZONAS SOMBREADAS EN ROSADO CON INFORMACIÓN ORIENTATIVA.~~ [^1]
- RELLENE ESTOS ESTATUTOS EN SU ORDENADOR. UNA VEZ RELLENO ESTE DOCUMENTO, ELIMINE LAS ZONAS SOMBREADAS EN COLORES E IMPRIMA DOS EJEMPLARES.
- SI LOS ESTATUTOS SE APRUEBAN EL MISMO DÍA QUE SE CONSTITUYE LA ASOCIACIÓN COMPRUEBE QUE EL ACTA FUNDACIONAL Y LOS ESTATUTOS (ULTIMA HOJA) TIENEN LA MISMA FECHA.
- LOS DOS EJEMPLARES DE ESTATUTOS DEBEN SER FIRMADOS POR EL/LA PRESIDENTE/A Y EL/LA SECRETARIO/A DE LA COMISIÓN GESTORA DE LA ASOCIACIÓN EN TODAS Y CADA UNA DE SUS HOJAS.
- UN ORIGINAL QUEDARÁ DEPOSITADO Y DILIGENCIADO EN EL REGISTRO DE ASOCIACIONES Y OTRO SERÁ DEVUELTO A LA ASOCIACIÓN CON EL CORRESPONDIENTE SELLO DE DILIGENCIA DE INSCRIPCIÓN.

----

<!-- \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ -->
# <span id="anchor"></span>ESTATUTOS SOCIALES {#estatutos-sociales}

## <span id="anchor-1"></span><span id="anchor-2"></span>CAPÍTULO I .- DE LA ASOCIACIÓN EN GENERAL {#capítulo-i-.--de-la-asociación-en-general}

#### <span id="anchor-3"></span><span id="anchor-4"></span>Artículo 1.- Denominación {#artículo-1.--denominacion}

De conformidad con el [artículo 22 de la Constitución Española][2] se constituye la entidad, no lucrativa, denominada Asociación <span style="color: #141414; background-color: lightgreen;">\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_</span>[^2], dotada de personalidad jurídica propia y capacidad de obrar y que se regirá por la [Ley Orgánica 1/2002, de 22 de marzo][3], reguladora del Derecho de Asociación, [la Ley 4/2003, de 28 de febrero, de Asociaciones de Canarias][4], el [Decreto 12/2007, de 5 de febrero, por el que se aprueba el Reglamento de Asociaciones de Canarias][5], demás disposiciones complementarias y por los presentes Estatutos

#### <span id="anchor-5"></span><span id="anchor-6"></span>Artículo 2.- Fines de la asociación. {#artículo-2.--fines-de-la-asociación.}

Los fines de la Asociación serán los siguientes[^3]:

1. <span style="color: #141414; background-color: lightgreen;">Servir de punto de encuentro y de contacto entre todas aquellas personas que en algún momento formaron parte de la comunidad educativa (alumnado, profesorado y personal de administración y servicios) de la Universidad Laboral de Las Palmas o del IES Felo Monzon Grau Bassas tras el cambio de denominación de aquella.&nbsp;</span>
1. <span style="color: #141414; background-color: lightgreen;">Mantener y conservar la memoria de ambos centros mediante especialmente como referencia de los valores democráticos y solidarios que forjaron la idiosincrasia del centro en sus diferentes épocas y de los componentes de la comunidad educativa desde su creación hasta la actualidad&nbsp;</span>
1. <span style="color: #141414; background-color: lightgreen;">Contribuir a la conservación de la memoria académica del centro, el mantenimiento de su legado como institución educativa de referencia desde su creación y hasta el día de hoy como ejemplo del compromiso con el progreso y desarrollo de la sociedad canaria y de varias generaciones de estudiantes de todas las islas.&nbsp;</span>
1. <span style="color: #141414; background-color: lightgreen;">Fomentar el intercambio de experiencias entre el alumnado egresado del centro, las diferentes generaciones de docentes y la propia institución como vía tanto de implementación de la formación recibida, la posibilidad de servir de apoyo y recursos al alumnado actual y a los proyectos que se pongan en marcha por el centro&nbsp;</span>
1. <span style="color: #141414; background-color: lightgreen;">Facilitar la realización de actividades de carácter cultural, solidario y participativo entre los miembros de la asociación que fomenten el espíritu histórico del centro, la relación entre las distintas generaciones del alumnado y el profesorado y la puesta en marcha de todas aquellas acciones que garanticen la continuidad de su proyección social, cultural y formativa&nbsp;</span>
1. <span style="color: #141414; background-color: lightgreen;">Colaborar con todos aquellos proyectos que partiendo de los asociados tengan como objetivo recuperar los contactos entre el alumnado y el profesorado de las distintas épocas, enseñanzas, promociones mediante la promoción de actividades de encuentro, intercambio de experiencias, organización de eventos, celebración de cursos, seminarios y jornadas que tengan como destinatarios a los propios asociados, a los actuales miembros de la comunidad educativa del Felo Monzón Grau Bassas.&nbsp;</span>
1. <span style="color: #141414; background-color: lightgreen;">Organizar, participar y colaborar con aquellas actividades de interés general que, en beneficio de la sociedad canaria puedan tener lugar en las instalaciones del centro, especialmente ligadas a los valores compartidos por todos los asociados y que se expresan en la defensa del medio ambiente, el desarrollo sostenible, el fomento de la cultura en todas sus facetas, el fomento de la tolerancia y la defensa de los derechos humanos, la solidaridad y la interculturalidad que ha caracterizado desde su origen a la Universidad Laboral y en la actualidad al CIFP Felo Monzón Grau Bassas.&nbsp;</span>
1. <span style="color: #141414; background-color: lightgreen;">Establecer un marco estable de coordinación entre el actual centro y los miembros de la asociación para el desarrollo de todas aquellas acciones de apoyo necesarias para la mejora, conservación y mantenimiento de las instalaciones, equipamientos e infraestructuras del centro que le permitan seguir siendo un referente educativo de primer nivel, incrementar y diversificar su oferta educativa en respuesta a las necesidades de nuestra tierra, así como albergar proyectos de indudable valor y referencia en cuanto a innovación y formación para Canarias y su entorno.&nbsp;</span>

<!--
|   |                                                                                                                                                                                       |
|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
| 2 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
| 3 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
| 4 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
| 5 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
| 6 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
| 7 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
| 8 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
-->

#### <span id="anchor-7"></span><span id="anchor-8"></span>Artículo 3.- Actividades {#artículo-3.--actividades}

1. Para el cumplimiento de sus fines la Asociación organizará las siguientes actividades[^4]:  
    
    a) <span style="color: #141414; background-color: lightgreen;">&nbsp;Organización de encuentros abiertos de antiguos miembros de la comunidad educativa (alumnado, profesorado, personal no docente).&nbsp;</span>  
    b) <span style="color: #141414; background-color: lightgreen;">&nbsp;Realización de actividades culturales, deportivas y de ocio dirigidas a mantener una oferta diversificada para el antiguo alumnado sus diferentes promociones y enseñanzas así como del personal docente y no docente.&nbsp;</span>  
    c) <span style="color: #141414; background-color: lightgreen;">&nbsp;Creación de un directorio de asociados que respetando la normativa en materia de protección de datos permita disponer de una red para la información y difusión de contactos, iniciativas y actividades que se desarrollen.&nbsp;</span>  
    d) <span style="color: #141414; background-color: lightgreen;">&nbsp;Recolección de anécdotas, imágenes y registros gráficos, orales o escritos para la reconstrucción de una memoria histórica, afectiva y emocional de las distintas épocas, etapas, promociones y hechos más relevantes del centro a lo largo de su existencia.&nbsp;</span>  
    e) <span style="color: #141414; background-color: lightgreen;">&nbsp;Fomento de la conexión entre el mundo profesional y empresarial desarrollado por el antiguo alumnado con el centro, sus enseñanzas y su actual alumnado como via para promover acciones de formación dual, practicas en empresas, bolsas de trabajo y otras iniciativas de inmersión social y laboral de las nuevas promociones.&nbsp;</span>  
    f) <span style="color: #141414; background-color: lightgreen;">&nbsp;Diseño y ejecución de actividades formativas en colaboración con el centro para el reciclaje y actualización del antiguo alumnado destinadas a mejorar la cualificación profesional y facilitar el acceso a nuevas modalidades de empleo y emprendeduría.&nbsp;</span>  
    g) <span style="color: #141414; background-color: lightgreen;">&nbsp;Organización y desarrollo de actividades de mecenazgo y voluntariado para la conservación, rehabilitación o mejora de instalaciones, equipamientos y espacios.&nbsp;</span>  
    h) <span style="color: #141414; background-color: lightgreen;">&nbsp;Realización de eventos destinados a la recaudación de fondos para el desarrollo de las actividades y consecución de los fines de la asociación.&nbsp;</span>  
    i) <span style="color: #141414; background-color: lightgreen;">&nbsp;Diseño de un espacio digital que sirva para mantener una continua conexión con los asociados y de comunicación social de las actividades de la asociación asi como de los proyectos del centro educativo.&nbsp;</span>  
    j) <span style="color: #141414; background-color: lightgreen;">&nbsp;Desarrollo de debates, seminarios, jornadas y encuentros destinados a la difusión y búsqueda de apoyo a proyectos de marcado interés educativo y social, relacionados con los objetivos del centro y los fines de la asociacion.&nbsp;</span>  
    
    <span style="color: Red; background-color: lightyellow;">:bulb:&nbsp;SACAR DE LA ENCUESTA MAS U OTRAS&nbsp;</span>  
    
    <!--
  |   |                                                                                                                                                                                       |
  |---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
  | 1 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
  | 2 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
  | 3 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
  | 4 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
  | 5 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
  | 6 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
  | 7 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |
  | 8 | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |  
  -->
2. Los beneficios eventualmente obtenidos por el desarrollo de estas actividades, se dedicarán a realizar otras de la misma naturaleza, conforme a los fines sociales.

#### <span id="anchor-9"></span><span id="anchor-10"></span>Artículo 4.- Domicilio {#artículo-4.--domicilio}

a) La asociación tendrá su domicilio social en la siguiente dirección postal:  
  <span style="color: Red; background-color: lightyellow;">:bulb:&nbsp;VALORAR CON EL CENTRO LA POSIBILIDAD DE USAR SU DIRECCION&nbsp;</span>  
  
<!--  
|               |                                                                                                                                                                                                     |  
|---------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|  
| Vía[^5]       | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |  
| Número[^6]    | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |  
| Localidad[^7] | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |  
| Municipio     | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |  
| Código Postal | <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span> |  
-->  
b) A los efectos de favorecer la comunicación telemática su dirección electrónica será <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_&nbsp;</span>  
  
c) Su variación será comunicada al Registro de Asociaciones a efectos de publicidad.  

#### <span id="anchor-11"></span><span id="anchor-12"></span>Artículo 5.- Ámbito territorial y duración {#artículo-5.--ámbito-territorial-y-duración}

La Asociación tendrá como ámbito territorial de actuación <span style="color: #141414; background-color: lightgreen;">&nbsp;autonómico&nbsp;</span>.[^8]  
<span style="color: Red; background-color: lightyellow;">:bulb:&nbsp;(Valorar el extender su actividad fuera si se entiende procedente)&nbsp;</span>

La duración será por tiempo indefinido. El acuerdo de disolución se adoptará conforme a lo indicado en los presentes Estatutos.

## <span id="anchor-13"></span><span id="anchor-14"></span>CAPÍTULO II.- DE LOS ÓRGANOS DIRECTIVOS Y ADMINISTRACIÓN. {#CAPÍTULO-ii.--de-los-órganos-directivos-y-administración.}

#### <span id="anchor-15"></span><span id="anchor-16"></span>Artículo 6.- Órganos sociales. {#artículo-6.--órganos-sociales.}

Son órganos de la asociación:

1. La Asamblea General.
2. <span style="color: Red; background-color: lightyellow;">&nbsp;Junta Directiva&nbsp;</span>.

### <span id="anchor-17"></span><span id="anchor-18"></span>SECCIÓN PRIMERA.- DE LA ASAMBLEA GENERAL {#sección-primera.--de-la-asamblea-general}

#### <span id="anchor-19"></span><span id="anchor-20"></span>Artículo 7.- Carácter y composición de la Asamblea general. {#artículo-7.--carácter-y-composición-de-la-asamblea-general.}

La Asamblea General es el órgano supremo de la Asociación, integrada por todos los asociados, que adoptará sus acuerdos bajo el principio democrático de mayorías de los votos válidamente emitidos.

Deberá ser convocada al menos en sesión ordinaria una vez al año, dentro del primer trimestre anual, para examinar y aprobar la liquidación anual de las cuentas del ejercicio anterior, el presupuesto del ejercicio corriente y la memoria de actividades del ejercicio anterior.

Asimismo se podrá convocar en sesión extraordinaria cuando así lo acuerde <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> y cuando lo soliciten un número de asociados no inferior a <span style="color: #141414; background-color: lightgreen;">&nbsp;10%&nbsp;</span>[^9].

En el supuesto de que la convocatoria se efectúe a iniciativa de los asociados, la reunión deberá celebrarse en el plazo de treinta días naturales desde la presentación de la solicitud.

#### <span id="anchor-21"></span><span id="anchor-22"></span>Artículo 8.- Convocatorias y Orden del Día. {#artículo-8.--convocatorias-y-orden-del-día.}

Las Asambleas Generales serán convocadas por el Presidente, haciendo expresa indicación del orden del día establecido por <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> o por los asociados que hayan solicitado su convocatoria.

En ambos casos, se incluirán en el orden del día aquéllos asuntos que propongan los asociados, cuando así lo soliciten un número/porcentaje no inferior a <span style="color: #141414; background-color: lightgreen;">&nbsp;a 50 soci@s o el 10%&nbsp;</span>.[^10]

#### <span id="anchor-23"></span><span id="anchor-24"></span>Artículo 9.- Constitución de la Asamblea General. {#artículo-9.--constitución-de-la-asamblea-general.}

Las Asambleas Generales, tanto ordinarias como extraordinarias, quedarán válidamente constituidas, en primera convocatoria cuando concurran a ella, presentes o representados, un tercio de los asociados, y en la segunda convocatoria cualquiera que sea el número de asociados concurrentes.

Los asociados podrán conferir, con carácter especial para cada reunión, su representación a otro asociado o persona que estimen procedente, mediante escrito dirigido a la Presidencia.

Entre la convocatoria y el día señalado para la celebración de la Asamblea General en primera convocatoria habrán de mediar al menos <span style="color: #141414; background-color: lightgreen;">&nbsp;15&nbsp;</span>[^11] días, pudiendo asimismo hacerse constar la fecha en la que, si procediera se reunirá la Asamblea General en segunda convocatoria, sin que entre una y otra reunión no pueda mediar un plazo inferior a <span style="color: #141414; background-color: lightgreen;">&nbsp;una hora&nbsp;</span>.

<span style="color: Red; background-color: lightyellow;">Por razones de urgencia y previo acuerdo de la Junta Directiva podrán reducirse los mencionados plazos a un mínimo de 48 horas las convocatorias que, en todo caso serán extraordinarias.</span>

<span style="color: Red; background-color: lightyellow;">Por acuerdo expreso de la Asamblea se podrán celebrar en caso necesario reuniones telemáticas haciendo uso de plataformas digitales.</span>

#### <span id="anchor-25"></span><span id="anchor-26"></span>Artículo 10.- Adopción de acuerdos. {#artículo-10.--adopción-de-acuerdos.}

Los acuerdos de la Asamblea General se adoptarán por mayoría simple de los asociados presentes o representados. Se entiende que se produce mayoría simple cuando los votos afirmativos superen los negativos y las abstenciones.

Requerirán mayoría absoluta de los asociados presentes o representados, los siguientes acuerdos:

1. Relativos a la disolución de la entidad
1. Modificación de los Estatutos
1. Disposición o enajenación de bienes
1. Adopción de una cuestión de confianza a <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>
1. Remuneración de los miembros de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.
1. <span style="color: Red; background-color: lightyellow;">Integración o inclusión en federaciones o confederaciones de asociaciones.</span>
1. …[^12]

Se entiende por mayoría absoluta el voto favorable de al menos la mitad más uno de todos los miembros asistentes y representados con derecho a voto.

<span style="color: Red; background-color: lightyellow;">Los acuerdos adoptados en las Asambleas Generales se harán constar en las respectivas actas, que serán certificadas y firmadas por la Secretaría y la Presidencia de la asociación.</span>

#### <span id="anchor-27"></span><span id="anchor-28"></span>Artículo 11.- Funciones de la Asamblea general. {#artículo-11.--funciones-de-la-asamblea-general.}

Corresponde a la Asamblea General, deliberar y tomar acuerdos sobre los siguientes asuntos:

1. Examinar y aprobar el Plan General de actuación y la Memoria anual de actividades que le presente <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.
2. Aprobar el Presupuesto anual de gastos e ingresos del siguiente año y el estado de cuentas del ejercicio anterior.
3. Decidir sobre la disposición o enajenación de bienes.
4. Elegir y separar a los miembros de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.
5. Solicitar la declaración de utilidad pública o interés público canario.
6. Acordar la integración de la asociación en Federaciones o Confederaciones, así como la separación de las mismas.
7. Controlar la actividad de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> y aprobar su gestión.
8. Modificar los Estatutos.
9. Acordar la disolución de la Asociación.
10. Designar la Comisión Liquidadora en caso de disolución.
11. Acordar la remuneración de los miembros de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>, en su caso.
12. Ratificar las altas acordadas por <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> y conocer las bajas voluntarias de los asociados.
13. Resolver, en última instancia, los expedientes relativos a sanción y separación de los asociados, tramitados conforme al procedimiento disciplinario establecido en los presentes Estatutos
14. Otras que le sean de su competencia en atención a la normativa aplicable.
15. …[^13]

#### <span id="anchor-29"></span><span id="anchor-30"></span>Artículo 12.- Certificación de acuerdos. {#artículo-12.--certificación-de-acuerdos.}

En las Asambleas Generales actuarán como Presidente/a y Secretario/a quienes lo sean de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.

Los acuerdos adoptados por la Asamblea General serán recogidos en un acta elaborada y firmada por el Secretario de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> y deberá contar con el visto bueno, mediante su firma, del Presidente de la asociación.

Cualquier miembro asistente tiene derecho a solicitar que su intervención o propuestas sean incluidas en el acta <span style="color: #141414; background-color: lightgreen;">&nbsp;aportando, para ello, su intervención o su propuesta por escrito&nbsp;</span>.[^14]

### <span id="anchor-31"></span><span id="anchor-32"></span>SECCIÓN SEGUNDA.- DEL ÓRGANO DE REPRESENTACIÓN {#sección-segunda.--del-órgano-de-representación}

#### <span id="anchor-33"></span><span id="anchor-34"></span>Artículo 13.- Definición del órgano de representación {#artículo-13.--definición-del-órgano-de-representación}

El órgano de representación de la asociación se denomina <span style="color: #141414; background-color: lightgreen;">&nbsp;Junta Directiva&nbsp;</span>.[^15]

Este órgano gestiona y representa los intereses de la Asociación, de acuerdo con las disposiciones y directivas de la Asamblea General.

Sólo podrán formar parte de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> los asociados.

Los cargos directivos serán gratuitos y carecerán de interés por si mismos o a través de personas interpuestas, en los resultados económicos de la realización de las actividades, salvo que la asamblea general por acuerdo adoptado con mayoría absoluta acuerde lo contrario.

#### <span id="anchor-35"></span><span id="anchor-36"></span>Artículo 14.- Miembros del órgano de representación. {#artículo-14.--miembros-del-órgano-de-representación.}

Serán requisitos indispensables para ser miembro de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>:

1. Ser mayor de edad
2. Estar en pleno uso de los derechos civiles
3. No estar incurso en los motivos de incompatibilidad establecidos en la legislación vigente.

#### <span id="anchor-37"></span><span id="anchor-38"></span>Artículo 15.- Convocatorias, orden del día y constitución. {#artículo-15.--convocatorias-orden-del-día-y-constitución.}

Las reuniones de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> se celebrarán previa convocatoria del Presidente con <span style="color: #141414; background-color: lightgreen;">&nbsp;7&nbsp;</span> días de antelación acompañada del orden del día consignando lugar, fecha y hora. En el caso de que la convocatoria no incluyese el lugar de celebración se entenderá a todos los efectos el domicilio social.

<span style="color: Red; background-color: lightyellow;">Por acuerdo expreso de la Asamblea se podrán celebrar en caso necesario reuniones telemáticas de la Junta Directiva, haciendo uso para ello de plataformas digitales en las condiciones que se determinen</span>

Se reunirá de forma periódica por lo menos <span style="color: #141414; background-color: lightgreen;">&nbsp;4&nbsp;</span> vez/veces al año y siempre que lo estime necesario el Presidente o lo soliciten la mayoría de sus miembros.

Para su válida constitución será precisa la asistencia de, al menos, un tercio de sus componentes, presentes o representados.

Los acuerdos se adoptarán por mayoría simple de los miembros presentes o representados, salvo aquéllos relativos a sanción o separación de los asociados, en los cuáles se precisará mayoría absoluta de los miembros presentes o representados.

La representación solamente podrá conferirse a otro miembro de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> con carácter especial para cada reunión y mediante carta dirigida al Presidente.

#### <span id="anchor-39"></span><span id="anchor-40"></span>Artículo 16.- Composición, duración y vacantes. {#artículo-16.--composición-duración-y-vacantes.}

<span style="color: Red; background-color: lightyellow;">&nbsp;La Junta Directiva&nbsp;</span> estará integrada por los siguientes miembros:

1. Un/a Presidente/a
2. Un/a Vicepresidente/a
3. Un/a Secretario/a
4. Un/a Tesorero/a
5. <span style="color: #141414; background-color: lightgreen;">Un mínimo de 5 y un máximo de 15 Vocalías a las que podrán asociarse funciones específicas</span>

Dichos cargos, que serán voluntarios, y por tanto deberán ser aceptados expresamente en documento firmado por las personas designadas, tendrán una duración de <span style="color: #141414; background-color: lightgreen;">&nbsp;cuatro&nbsp;</span>[^16] año/s, pudiendo ser reelegidos.

Los miembros de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> comenzarán a ejercer sus funciones una vez aceptado el mandato para el que hayan sido designados por la Asamblea General.

Las vacantes que se produzcan en <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>, antes de terminar su período de mandato, serán cubiertas por los asociados que designe la propia Junta Directiva, dando cuenta de las sustituciones en la primera Asamblea general que se celebre, debiendo ratificarse dicho acuerdo por la Asamblea. En caso contrario, se procederá a la elección del asociado que debe cubrir la vacante en la misma sesión de la Asamblea.

#### <span id="anchor-41"></span><span id="anchor-42"></span>Artículo 17.- Causas de cese. {#artículo-17.--causas-de-cese.}

Los miembros de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> podrán ser separados de sus cargos por los siguientes motivos:

1. Por renuncia voluntaria
2. Por muerte o declaración de fallecimiento, enfermedad o cualquier otra causa que le impida el ejercicio de sus funciones.
3. Por pérdida de la cualidad de socio.
4. Por incapacidad, inhabilitación o incompatibilidad de acuerdo con la legislación vigente.
5. Por el transcurso del período de su mandato.
6. Por separación acordada por la Asamblea General.
7. La comisión de una infracción muy grave, conforme al [Artículo 40](#artículo-40.--infracciones-de-losas-miembros-de-el-órgano-de-representación) de los presentes estatutos.

<span style="color: Red; background-color: lightyellow;">&nbsp;La Junta Directiva&nbsp;</span> dará cuenta a la Asamblea General de la separación de sus miembros, debiendo ratificarse por la Asamblea cuando el acuerdo de separación haya sido adoptado por el motivo expresado en la letra g).

#### <span id="anchor-43"></span><span id="anchor-44"></span>Artículo 18.- Atribuciones del órgano de representación. {#artículo-18.--atribuciones-del-órgano-de-representación.}

Las facultades de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> se extenderán, con carácter general, a todos los actos propios de las finalidades de la asociación, siempre que no requieran, conforme a los presentes Estatutos, autorización expresa de la Asamblea General.

En particular son facultades de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>:

1. Velar por el cumplimiento de los Estatutos y ejecutar los acuerdos tomados en las Asambleas Generales.
2. Confeccionar las Memorias, cuentas, inventarios, balances y presupuestos de la Asociación.
3. Elaborar el borrador del Reglamento de Régimen Interior.
4. Acordar la celebración de actividades.
5. Tener a disposición de los asociados el Libro de Registro de Asociados.
6. Tener a disposición de los asociados los libros de Actas y de Contabilidad; así como la documentación de la entidad.
7. Recaudar la cuota de los asociados y administrar los fondos sociales.
8. Instruir los expedientes relativos a la sanción y separación de los asociados y adoptar, de forma cautelar, la resolución que proceda de los mismos, hasta su resolución definitiva por la Asamblea General.
9. Proponer a la Asamblea General para su aprobación las cuentas anuales y el presupuesto formulados por el tesorero/a, así como la memoria de actividades formuladas por el secretario/a.
10. <span style="color: Red; background-color: lightyellow;">Admitir con carácter provisional a nuevas personas como asociadas una vez presentada la correspondiente solicitud hasta su ratificación por la Asamblea General y resolver las dudas que se planteen sobre la concurrencia de los requisitos de admisión.</span>

#### <span id="anchor-45"></span><span id="anchor-46"></span>Artículo 19.- Funciones del/a Presidente/a. {#artículo-19.--funciones-dela-presidentea.}

Serán atribuciones del/a Presidente/a:

1. Ostentar la representación legal de la Asociación.
2. Convocar y presidir las reuniones de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> y de la Asamblea General de acuerdo con lo establecido en los presentes Estatutos.
3. Velar por el cumplimiento de los fines sociales.
4. Autorizar con su firma las actas, certificaciones y demás documentos de la Asociación.
5. Solicitar, percibir o hacer efectivas las ayudas, subvenciones o patrocinios que por cualquier concepto provengan de organismos públicos o entidades privadas, así como contratar y convenir con entes públicos de cualquier naturaleza y privados.
6. Y cuantas facultades se le confieran, no expresamente asignadas a otros órganos.

#### <span id="anchor-47"></span><span id="anchor-48"></span>Artículo 20.- Funciones del/a Vicepresidente/a. {#artículo-20.--funciones-dela-vicepresidentea.}

Serán facultades del/la Vicepresidente/a:

1. Sustituir al/la Presidente/a en caso de vacante, ausencia o enfermedad asumiendo sus funciones, con carácter provisional, cuando el titular cesare en el cargo.
2. Las que les deleguen el/la Presidente/a o la Asamblea General.

#### <span id="anchor-49"></span><span id="anchor-50"></span>Artículo 21.- Funciones del/a Secretario/a {#artículo-21.--funciones-dela-secretarioa}

Corresponderá al/la Secretario/a las siguientes funciones:

1. Redactar y certificar las actas de las sesiones de las Asambleas Generales y de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.
2. Llevar el libro del Registro de Asociados, consignando en ellos la fecha de su ingreso y las bajas que hubieren.
3. Recibir y tramitar las solicitudes de ingreso.
4. Llevar una relación del inventario de la Asociación.
5. Tener bajo su custodia los documentos y archivos de la Asociación.
6. Expedir certificaciones.
7. Formular la memoria de actividades.

#### <span id="anchor-51"></span><span id="anchor-52"></span>Artículo 22.- Funciones del/la Tesorero/a {#artículo-22.--funciones-della-tesoreroa}

Son facultades del/la Tesorero/a:

1. Tendrá a su cargo los fondos pertenecientes a la Asociación.
2. Elaborar los presupuestos, balances e inventarios de la Asociación.
3. Firmará los recibos, cobrará las cuotas de los asociados y efectuará todos los cobros y pagos.
4. Llevar y custodiar los Libros de Contabilidad.
5. Formular las cuentas anuales y el presupuesto

#### <span id="anchor-53"></span><span id="anchor-54"></span>Artículo 23.- Funciones de los/as Vocales {#artículo-23.--funciones-de-losas-vocales}

Los/as vocales tendrán el derecho y la obligación de asistir a las sesiones de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> con voz y con voto así como, en régimen de delegación, podrán desempeñar las funciones que les confiera <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.

### <span id="anchor-55"></span><span id="anchor-56"></span>SECCIÓN TERCERA.- DEL RÉGIMEN ELECTORAL Y MOCIÓN DE CENSURA {#sección-tercera.--del-régimen-electoral-y-moción-de-censura}

#### <span id="anchor-57"></span><span id="anchor-58"></span>Artículo 24.- Elección del órgano de representación {#artículo-24.--elección-del-órgano-de-representación}

Los/as miembros de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> serán elegidos/as entre los/as asociados/as mediante sufragio universal, libre, directo y secreto.

Procederá la convocatoria de elecciones en los siguientes casos:

1. Por expiración del mandato.
2. En caso de prosperar una cuestión de confianza acordada en Asamblea general extraordinaria por mayoría absoluta.
3. En caso de cese, por cualquier causa, de la mayoría de los miembros de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.

#### <span id="anchor-59"></span><span id="anchor-60"></span>Artículo 25.- Junta Electoral y Calendario {#artículo-25.--junta-electoral-y-calendario}

Concluido el mandato de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> o aprobada una cuestión de confianza, en el plazo de 30 días, el/a Presidente/a en funciones convocará elecciones y constituirá la Junta Electoral, que estará formada por dos asociados/as que, voluntariamente se presten para esta función, dichos/as asociados/as no podrán formar parte de alguna de las candidaturas presentadas.

En caso de no presentarse voluntarios/as formarán la citada Junta, los/as dos asociados/as de mayor y de menor edad, componiéndose de un total de cuatro miembros.

Corresponde a la Junta Electoral:

1. Organizar las elecciones, resolviendo sobre cualquier asunto que atañe a su desarrollo.
2. Aprobar definitivamente el censo electoral.
3. Resolver las impugnaciones que se presenten en relación al proceso electoral.

#### <span id="anchor-61"></span><span id="anchor-62"></span>Artículo 26.- Calendario Electoral {#artículo-26.--calendario-electoral}

El plazo entre la convocatoria de elecciones y la celebración de las mismas no sobrepasará los treinta días hábiles, siendo los cinco primeros días hábiles de exposición de la lista de los/as asociados/as con derecho a voto. En dicho período podrá impugnarse la lista.

Transcurrido el plazo de exposición e impugnación, en los tres días siguientes se resolverán las impugnaciones al censo y se procederá a la aprobación definitiva del censo electoral.

En los doce días siguientes podrán presentarse las candidaturas. Una vez concluido el plazo de presentación de candidaturas, en los cinco días hábiles siguientes se resolverá sobre la validez de las mismas y su proclamación definitiva.

Si no se presenta candidatura alguna, se convocarán por parte del Presidente en funciones, nuevamente elecciones, en el plazo máximo de quince días desde el momento de cierre del plazo de presentación de aquéllas.

#### <span id="anchor-63"></span><span id="anchor-64"></span>Artículo 27.- Moción de censura. {#artículo-27.--moción-de-censura.}

La moción de censura a <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> deberá ser tratada por la Asamblea general, siempre que hubiese sido solicitada, mediante escrito razonado, como mínimo, por un tercio de los miembros asociados.

A tal efecto deberá ser convocada en el plazo de diez días hábiles desde que se formalice la solicitud.

Será precisa para que prospere la moción de censura que la misma sea adoptada por la mayoría absoluta de los asociados, presentes o representados, en Asamblea general extraordinaria.

Caso de prosperar, el Presidente en funciones deberá convocar elecciones en el plazo máximo de cinco días, si bien continuará en funciones hasta que tome posesión la nueva Junta que resulte proclamada definitivamente en las elecciones.

## <span id="anchor-65"></span><span id="anchor-66"></span>CAPÍTULO III.- DE LOS/AS ASOCIADOS/AS. {#CAPÍTULO-iii.--de-losas-asociadosas.}

#### <span id="anchor-67"></span><span id="anchor-68"></span>Artículo 28.- Asociados/as. {#artículo-28.--asociadosas.}

Podrán ser miembros de la Asociación:

~~1. Las personas físicas, con capacidad de obrar, no sujetas a ninguna condición legal para el ejercicio del derecho de asociación así como los/as menores de edad, con catorce o más años, siempre que cuenten con el consentimiento escrito y expreso de sus representantes legales.~~
1. <span style="color: Red; background-color: lightyellow;">Las personas físicas, mayores de edad, con capacidad de obrar, no sujetas a ninguna condición legal para el ejercicio del derecho de asociación que hayan cursado estudios o formado parte del personal docente o no docente de la Universidad Laboral de Las Palmas o del Centro Felo Monzón Grau Bassas.</span>
2. Las personas jurídicas.

#### <span id="anchor-69"></span><span id="anchor-70"></span>Artículo 29.- Procedimiento de admisión {#artículo-29.--procedimiento-de-admisión}

La condición de asociado/a se adquirirá, de forma provisional, a solicitud de la persona interesada, por escrito, dirigido a <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> manifestando su voluntad de contribuir al logro de los fines asociativos.

El/la Presidente o el/la Secretario/a deberán entregar a la persona interesada constancia escrita de su solicitud e incluirá en el orden del día de la próxima reunión de la Asamblea General la relación de todas las solicitudes presentadas, correspondiendo a la Asamblea ratificar la admisión de los/as asociados/as.

#### <span id="anchor-71"></span><span id="anchor-72"></span>Artículo 30.- Clases de Asociados {#artículo-30.--clases-de-asociados}

Los/as asociados/as pueden ser:

1. Fundadores/as: Quienes suscribieron el acta fundacional de la asociación.
2. De número: Quienes hayan <span style="color: Red; background-color: lightyellow;">ingresado</span> con posterioridad a la firma del acta fundacional de la asociación y se les admita como tales conforme a estos Estatutos.
3. Honoríficos/as: Quienes por decisión de la Asamblea General colaboren de forma notable en el desarrollo de los fines de la Asociación y/o quienes destaquen por ayudar con medios económicos y materiales a la Asociación.

#### <span id="anchor-73"></span><span id="anchor-74"></span>Artículo 31.- Derechos de los/as asociados/as fundadores/as y de número. {#artículo-31.--derechos-de-losas-asociadosas-fundadoresas-y-de-número.}

Los/as asociados/as fundadores/as y de números tendrán los siguientes derechos:

1. Asistir, participar y votar en las Asambleas generales.
2. Formar parte de los órganos de la asociación.
3. Tener información del desarrollo de las actividades de la entidad, de su situación patrimonial y de la identidad de los asociados.
4. Participar en los actos de la asociación.
5. Conocer los estatutos, los reglamentos y normas de funcionamiento de la asociación.
6. Consultar los libros de la asociación, conforme a las normas que determinen su acceso a la documentación de la entidad.
7. Separarse libremente de la asociación.
8. Ser oído/a con carácter previo a la adopción de medidas disciplinarias contra su persona y a ser informado/a de los hechos que den lugar a tales medidas, debiendo ser motivado, en su caso, el acuerdo que imponga la sanción.
9. Impugnar los acuerdos de los órganos de la asociación, cuando los estime contrarios a la Ley o a los Estatutos.

#### <span id="anchor-75"></span><span id="anchor-76"></span>Artículo 32.- Obligaciones de los/as asociados/as fundadores/as y de número. {#artículo-32.--obligaciones-de-losas-asociadosas-fundadoresas-y-de-número.}

Serán obligaciones de los/as asociados/as fundadores/as y numerarios/as:

1. Compartir las finalidades de la asociación y colaborar para la consecución de las mismas.
2. Pagar las cuotas, derramas y otras aportaciones que se determinen mediante acuerdo adoptado por la Asamblea General.
3. Cumplir el resto de obligaciones que resulten de las disposiciones estatutarias.
4. Acatar y cumplir los acuerdos válidamente adoptados por los órganos de gobierno y representación de la Asociación.

#### <span id="anchor-77"></span><span id="anchor-78"></span>Artículo 33.- Asociados/as Honoríficos/as. {#artículo-33.--asociadosas-honoríficosas.}

Los/as asociados/as honoríficos/as tienen derecho a participar en las actividades de la Asociación, así como a asistir a las Asambleas, con derecho a voz, pero no a voto.

#### <span id="anchor-79"></span><span id="anchor-80"></span>Artículo 34.- Pérdida de la cualidad de asociado/a. {#artículo-34.--pérdida-de-la-cualidad-de-asociadoa.}

Se perderá la condición de socio/a:

1. Por voluntad de la persona, manifestada por escrito a <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.
2. Por acuerdo adoptado por el órgano competente de la asociación, conforme al régimen disciplinario establecido en el [capítulo IV](#CAPÍTULO-iv--régimen-disciplinario-infracciones-sanciones-procedimiento-y-prescripción.) de estos Estatutos.

## <span id="anchor-81"></span><span id="anchor-82"></span>CAPÍTULO IV- RÉGIMEN DISCIPLINARIO: INFRACCIONES, SANCIONES, PROCEDIMIENTO Y PRESCRIPCIÓN. {#CAPÍTULO-iv--régimen-disciplinario-infracciones-sanciones-procedimiento-y-prescripción.}

#### <span id="anchor-83"></span><span id="anchor-84"></span>Artículo 35.- Normas generales. {#artículo-35.--normas-generales.}

En el ejercicio de la potestad disciplinaria se respetarán los siguientes principios generales:

1. Proporcionalidad con la gravedad de la infracción, atendiendo a la naturaleza de los hechos, las consecuencias de la infracción y la concurrencia de circunstancias atenuantes o agravantes.
2. Inexistencia de doble sanción por los mismos hechos.
3. Aplicación de los efectos retroactivos favorables
4. Prohibición de sancionar por infracciones no tipificadas en el momento de su comisión.

La responsabilidad disciplinaria se extingue en todo caso por:

1. El cumplimiento de la sanción.
2. La prescripción de la infracción.
3. La prescripción de la sanción.
4. El fallecimiento del infractor.

Para la imposición de las correspondientes sanciones disciplinarias se tendrán en cuenta las circunstancias agravante de la reincidencia y atenuante de arrepentimiento espontáneo.

Hay reincidencia cuando la persona autora de la falta hubiese sido sancionado/a anteriormente por cualquier infracción de igual gravedad, o por dos o más que lo fueran de menor.

La reincidencia se entenderá producida en el transcurso de un año, contado a partir de la fecha en que se haya cometido la primera infracción.

#### <span id="anchor-85"></span><span id="anchor-86"></span>Artículo 36.- Infracciones {#artículo-36.--infracciones}

Las infracciones contra el buen orden social susceptibles de ser sancionadas se clasifican en leves, graves y muy graves.

#### <span id="anchor-87"></span><span id="anchor-88"></span>Artículo 37.- Infracciones Muy Graves {#artículo-37.--infracciones-muy-graves}

Tienen la consideración de infracciones disciplinarias muy graves:

1. Todas aquellas actuaciones que perjudiquen u obstaculicen la consecución de los fines de la asociación, cuando tengan consideración de muy graves.
2. El incumplimiento o las conductas contrarias a las disposiciones estatutarias y/o reglamentarias de la Asociación, cuando se consideren como muy graves.
3. El incumplimiento de los acuerdos válidamente adoptados por los órganos de la asociación, cuando se consideren muy graves.
4. La protesta o actuaciones airadas y ofensivas que impidan la celebración de asambleas o reuniones de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.
5. Participar, formular o escribir, mediante cualquier medio de comunicación social, manifestaciones que perjudiquen de forma muy grave la imagen de la asociación.
6. La usurpación ilegítima de atribuciones o competencias sin contar con la preceptiva autorización del órgano competente de la entidad.
7. Agredir, amenazar o insultar gravemente a cualquier asociado/a.
8. La inducción o complicidad, plenamente probada, a cualquier socio/a en la comisión de las faltas contempladas como muy graves.
9. El quebrantamiento de sanciones impuestas por falta grave o muy grave.
10. Todas las infracciones tipificadas como leves o graves y cuyas consecuencias físicas, morales o económicas, plenamente probadas, sean consideradas como muy graves.
11. En general, las conductas contrarias al buen orden social, cuando se consideren muy graves.

#### <span id="anchor-89"></span><span id="anchor-90"></span>Artículo 38.- Infracciones graves {#artículo-38.--infracciones-graves}

Son infracciones punibles dentro del orden social y serán consideradas como graves:

1. El quebrantamiento de sanciones impuestas por infracciones leves.
2. Participar, formular o escribir mediante cualquier medio de comunicación social, manifestaciones que perjudiquen de forma grave la imagen de la asociación.
3. La inducción o complicidad, plenamente probada, de cualquier asociado en la comisión de cualquiera de las faltas contempladas como graves.
4. Todas las infracciones tipificadas como leves y cuyas consecuencias físicas, morales o económicas, plenamente probadas, sean consideradas graves.
5. La reiteración de una falta leve.
6. El incumplimiento o las conductas contrarias a las disposiciones estatutarias y/o reglamentarias de la asociación, cuando se consideren como graves.
7. El incumplimiento de los acuerdos válidamente adoptados por los órganos de la asociación, cuando tengan la consideración de grave.
8. En general, las conductas contrarias al buen orden social, cuando se consideren como graves.

#### <span id="anchor-91"></span><span id="anchor-92"></span>Artículo 39.- Infracciones Leves {#artículo-39.--infracciones-leves}

Se consideran infracciones disciplinarias leves:

1. La falta de asistencia durante tres ocasiones a las asambleas generales, sin justificación alguna.
2. El impago de tres cuotas consecutivas, salvo que exista causa que lo justifique a criterio de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.
3. Todas aquéllas conductas que impidan el correcto desarrollo de las actividades propias de la asociación, cuando tengan la consideración de leve.
4. El maltrato de los bienes muebles o inmuebles de la Asociación.
5. Toda conducta incorrecta en las relaciones con los/as socios/as.
6. La inducción o complicidad, plenamente probada, de cualquier asociado/a en la comisión de las faltas contempladas como leves.
7. El incumplimiento o las conductas contrarias a las disposiciones estatutarias y/o reglamentarias de la entidad, cuando se consideren como leves.
8. En general, las conductas contrarias al buen orden social, cuando se consideren como leves.

#### <span id="anchor-93"></span><span id="anchor-94"></span>Artículo 40.- Infracciones de los/as miembros de el órgano de representación: {#artículo-40.--infracciones-de-losas-miembros-de-el-órgano-de-representación}

1. Se consideran infracciones muy graves susceptibles de ser cometidas por los/as miembros de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>:

   1.

   2. La no convocatoria en los plazos y condiciones legales, de forma sistemática y reiterada, de los órganos de la asociación.

   3. La incorrecta utilización de los fondos de la entidad.

   4. El abuso de autoridad y la usurpación ilegítima de atribuciones o competencias.

   5. La inactividad o dejación de funciones que suponga incumplimiento muy grave de sus deberes estatutarios y/o reglamentarios.

   6. La falta de asistencia, en tres ocasiones y sin causa justificada, a las reuniones de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.

2. Se consideran infracciones graves:

   1.

   2. No facilitar a los/as asociados/as la documentación de la entidad que por éstos le sea requerida (estatutos, actas, normas de régimen interno, etc.).

   3. No facilitar el acceso de los/as asociados/as a la documentación de la entidad.

   4. La inactividad o dejación de funciones cuando causen perjuicios de carácter grave al correcto funcionamiento de la entidad.

3. Tienen la consideración de infracciones leves:

   1.
   2. La inactividad o dejación de funciones, cuando no tengan la consideración de muy grave o grave.
   3. La no convocatoria de los órganos de la asociación en los plazos y condiciones legales.
   4. Las conductas o actuaciones contrarias al correcto funcionamiento de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>.
   5. La falta de asistencia a una reunión de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>, sin causa justificada.

#### <span id="anchor-95"></span><span id="anchor-96"></span>Artículo 41.- Sanciones {#artículo-41.--sanciones}

Las sanciones susceptibles de aplicación por la comisión de infracciones muy graves, relacionadas en el [artículo 37](#artículo-37.--infracciones-muy-graves), serán la pérdida de la condición de asociado/a o la suspensión temporal en tal condición durante un período de un año a cuatro años, en adecuada proporción a la infracción cometida.

Las infracciones graves, relacionadas en el [artículo 38](#artículo-38.--infracciones-graves), darán lugar a la suspensión temporal en la condición de asociado/a durante un período de un mes a un año.

La comisión de las infracciones de carácter leve dará lugar, por lo que a las relacionadas en el [artículo 39](#artículo-39.--infracciones-leves) se refieren, a la amonestación o a la suspensión temporal del/a asociado/a por un período de un mes.

Las infracciones señaladas en el [artículo 40](#artículo-40.--infracciones-de-losas-miembros-de-el-órgano-de-representación) darán lugar, en el caso de las muy graves al cese en sus funciones de miembro de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> y, en su caso, a la inhabilitación para ocupar nuevamente cargos en el órgano de gobierno; en el caso de las graves, el cese durante un período de un mes a un año, y si la infracción cometida tiene el carácter de leve en la amonestación o suspensión por el período de un mes.

#### <span id="anchor-97"></span><span id="anchor-98"></span>Artículo 42.- Procedimiento sancionador {#artículo-42.--procedimiento-sancionador}

Para la adopción de las sanciones señaladas en los artículos anteriores, se tramitará un expediente disciplinario en el cual, de acuerdo con el [artículo 31](#Art31) de estos estatutos, el/a asociado/a tiene derecho a ser oído/a con carácter previo a la adopción de medidas disciplinarias contra su persona y a ser informado/a de los hechos que den lugar a tales medidas, debiendo ser motivado, en su caso, el acuerdo que imponga la sanción.

La instrucción de los procedimientos sancionadores corresponde a <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>, nombrándose a tal efecto por ésta, los/as miembros de la misma que tengan encomendada dicha función.

Caso de tramitarse un expediente contra un/a miembro de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> éste/a no podrá formar parte del órgano instructor, debiendo abstenerse de intervenir y votar en la reunión de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> que decida la resolución provisional del mismo.

El órgano instructor de los procedimientos disciplinarios estará formado por un/a Presidente y un/a Secretario/a. El/la Presidente/a ordenará al/a Secretario/a la práctica de aquéllas diligencias previas que estime oportunas al objeto de obtener la oportuna información sobre la comisión de infracción por parte del/a asociado/a.

A la vista de esta información <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span> podrá mandar archivar las actuaciones o acordar la incoación de expediente disciplinario.

En este último caso, el/la Secretario/a pasará a la persona expedientada un escrito en el que pondrá de manifiesto los cargos que se le imputan, a los que podrá contestar alegando en su defensa lo que estime oportuno en el plazo de 15 días, transcurridos los cuales, se pasará el asunto a la primera sesión de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>, la cual acordará lo que proceda; el acuerdo debe ser adoptado por la mayoría absoluta de los/as miembros de dicho <span style="color: Red; background-color: lightyellow;">&nbsp;Junta Directiva&nbsp;</span>.

La resolución que se adopte tendrá carácter provisional. El/a asociado/a podrá formular recurso ante la Asamblea General en el plazo de quince días a contar desde el día siguiente a aquél en que reciba la resolución. De no formularse recurso en el plazo indicado, la resolución deviene firme.

La Asamblea general, adoptará la resolución que proceda en relación con el expediente disciplinario o sancionador.

#### <span id="anchor-99"></span><span id="anchor-100"></span>Artículo 43.- Prescripción {#artículo-43.--prescripción}

Las infracciones prescribirán a los tres años, al año o al mes, según se trate de las muy graves, graves o leves, comenzándose a contar el plazo de prescripción al día siguiente a la comisión de la infracción.

El plazo de prescripción se interrumpirá por la iniciación del procedimiento sancionador, con conocimiento del interesado, pero si éste permaneciese paralizado durante un mes por causa no imputable al asociado, volverá a correr el plazo correspondiente.

Las sanciones prescribirán a los tres años, al año o al mes, según se trate de las que correspondan a infracciones muy graves, graves o leves, comenzándose a contar el plazo de prescripción desde el día siguiente a aquél en que adquiera firmeza la resolución por la que se impuso la sanción.

## <span id="anchor-101"></span><span id="anchor-102"></span>CAPÍTULO V.- LIBROS Y DOCUMENTACIÓN {#CAPÍTULO-v.--libros-y-documentación}

#### <span id="anchor-103"></span><span id="anchor-104"></span>Artículo 44.- Libros y documentación contable. {#artículo-44.--libros-y-documentación-contable.}

La Asociación dispondrá de un Libro de Registro de Socios/as y de aquellos Libros de Contabilidad que permitan obtener la imagen fiel del patrimonio, del resultado y de la situación financiera de la entidad.

Llevará también un libro de actas de las reuniones de la Asamblea general y de <span style="color: Red; background-color: lightyellow;">&nbsp;la Junta Directiva&nbsp;</span>, en las que constarán, al menos:

1. Todos los datos relativos a la convocatoria y a la constitución del órgano.
2. Un resumen de los asuntos debatidos.
3. Las intervenciones de las que se haya solicitado constancia.
4. Los acuerdos adoptados.
5. Los resultados de las votaciones.

#### <span id="anchor-105"></span><span id="anchor-106"></span>Artículo 45.- Derecho de acceso a los libros y documentación. {#artículo-45.--derecho-de-acceso-a-los-libros-y-documentación.}

<span style="color: Red; background-color: lightyellow;">&nbsp;La Junta Directiva&nbsp;</span>, encargada de la custodia y llevanza de los libros, deberá tener a disposición de los/as socios/as, los libros y documentación de la entidad, facilitando el acceso por parte de los/as mismos/as.

A tal efecto, una vez recibida la solicitud por el/la Presidente, se pondrá a disposición del/a asociado/a en el plazo máximo diez días.

## <span id="anchor-107"></span><span id="anchor-108"></span>CAPÍTULO VI.- RÉGIMEN ECONÓMICO. {#CAPÍTULO-vi.--régimen-económico.}

#### <span id="anchor-109"></span><span id="anchor-110"></span>Artículo 46.- Patrimonio Inicial {#artículo-46.--patrimonio-inicial}

La Asociación cuenta con un patrimonio inicial de <span style="color: #141414; background-color: lightgreen;">&nbsp;\_\_\_\_\_\_\_&nbsp;</span> euros conformado por los siguientes bienes[^17]:

1. …
2. …
3. …
4. …

#### <span id="anchor-111"></span><span id="anchor-112"></span>Artículo 47.- Ejercicio económico {#artículo-47.--ejercicio-económico}

El ejercicio económico será anual y su cierre tendrá lugar el 31 de diciembre de cada año.

#### <span id="anchor-113"></span><span id="anchor-114"></span>Artículo 48.- Recursos económicos {#artículo-48.--recursos-económicos}

Constituirán los recursos económicos de la Asociación:

1. Las cuotas de los miembros, periódicas o extraordinarias.
2. Las aportaciones, subvenciones, donaciones a título gratuito, herencias y legados recibidos.
3. Bienes muebles e inmuebles
4. Cualquier otro recurso susceptible de valoración económica y admitido en derecho.

## <span id="anchor-115"></span><span id="anchor-116"></span>CAPÍTULO VII.- MODIFICACIÓN DE ESTATUTOS Y NORMAS DE RÉGIMEN INTERNO {#capítulo-vii.--modificación-de-estatutos-y-normas-de-régimen-interno}

#### <span id="anchor-117"></span><span id="anchor-118"></span>Artículo 49.- Modificación de Estatutos {#artículo-49.--modificación-de-estatutos}

Los Estatutos de la asociación podrán ser modificados cuando resulte conveniente a los intereses de la misma, por acuerdo de la asamblea general convocada específicamente al efecto.

El acuerdo de modificar los estatutos requiere mayoría absoluta de los socios presentes o representados.

#### <span id="anchor-119"></span><span id="anchor-120"></span>Artículo 50.- Normas de régimen interno. {#artículo-50.--normas-de-régimen-interno.}

Los presentes estatutos podrán ser desarrollados mediante normas de régimen interno, aprobadas por acuerdo de la Asamblea general por mayoría simple de los socios presentes o representados.

## <span id="anchor-121"></span><span id="anchor-122"></span>CAPÍTULO VIII.- DISOLUCIÓN DE LA ASOCIACIÓN. {#CAPÍTULO-viii.--disolución-de-la-asociación.}

#### <span id="anchor-123"></span><span id="anchor-124"></span>Artículo 51.- Causas. {#artículo-51.--causas.}

La Asociación puede disolverse:

1. Por Sentencia judicial firme.
2. Por acuerdo de la Asamblea General Extraordinaria.
3. Por las causas determinadas en el [artículo 39 del Código Civil][6].

#### <span id="anchor-125"></span><span id="anchor-126"></span>Artículo 52.- Comisión Liquidadora {#artículo-52.--comisión-liquidadora}

Acordada la disolución de la asociación, la Asamblea General Extraordinaria designará a una Comisión Liquidadora que tendrá las siguientes funciones:

1. Velar por la integridad del patrimonio de la asociación.
2. Concluir las operaciones pendientes y efectuar las nuevas, que sean precisas para la liquidación de la asociación.
3. Cobrar los créditos de la entidad.
4. Liquidar el patrimonio y pagar a los acreedores.
5. Aplicar los bienes sobrantes a los fines previstos en los presentes Estatutos.
6. Solicitar la inscripción de la disolución en el Registro de Asociaciones.

## <span id="anchor-127"></span><span id="anchor-128"></span>DISPOSICIÓN FINAL {#disposición-final}

Los presentes Estatutos han sido aprobados el mismo día de celebración del acta fundacional, de cuyo contenido dan testimonio y firman al margen de cada una de las hojas que lo integran, las siguientes personas.

<table>
  <tbody>
    <tr class="odd">
      <td>
        <p>Vº Bº – PRESIDENTE/A</p>
        <p><br><br><br><br></p>
      </td>
      <td>
        <p>SECRETARIO/A</p>
        <p><br><br><br><br></p>
      </td>
    </tr>
  </tbody>
</table>

----

[^1]:
    > Todas las ayudas se han trasladado como **notas a pié de página**, para facilitar la legibilidad del documento.

[^2]:
    > Inclúyase la denominación exacta de la asociación que debe coincidir con la que consta en el acta fundacional. La denominación deberá hacer referencia en todo caso al objeto principal de la asociación (es decir, incluyendo el adjetivo que hace referencia a su actividad principal (cultural, social, musical, ecológica, etc.) y no podrá contener denominaciones que comporten discriminación por razón de género, edad, origen racial, etc.

[^3]:
    > Enumerar en párrafos separados los fines de la asociación de la forma más clara, concisa y precisa que sea posible.

[^4]:
    > Enumerar las actividades principales que desarrollará la asociación como medio para el cumplimiento de los fines señalados anteriormente, de una forma clara, concisa y precisa, y siempre que dichas actividades no comporten el desarrollo de una actividad mercantil, o en cualquier caso, donde exista un ánimo de lucro.

[^5]:
    > Señalar el tipo de vía (calle, avenida, pasaje, etc.) y a continuación su denominación postal exacta.

[^6]:
    > Incluir todas las señales numéricas y letras de identificación del inmueble.

[^7]:
    > En caso de que dentro de un municipio se encuentre en una localidad o núcleo urbano diferenciado.

[^8]:
    > Conforme a la Ley el ámbito territorial podrá ser autonómico, insular o municipal. En caso de señalarse un ámbito insular o municipal deberá indicarse a qué isla o municipio se refiere.

[^9]:
    > Indicar el número de socios/as o el porcentaje de socios/as necesario para solicitar la convocatoria de la asamblea general.

[^10]:
    > Indicar el número mínimo o el porcentaje mínimo de socios exigible para solicitar puntos del orden del día.

[^11]:
    > Señalar el número de días mínimo que se entienda adecuado para convocar la asamblea.

[^12]:
    > Incluir en su caso otros acuerdos que se considere adecuado la mayoría absoluta.

[^13]:
    > Los Estatutos pueden atribuirles otras funciones a la Asamblea. Incluirlos en caso procedente.

[^14]:
    > Incluir la forma en que debe solicitarlo. Generalmente se incluye que el interesado aporte por escrito su intervención o su propuesta.

[^15]:
    > Generalmente se suele denominar Junta Directiva si es un órgano colegiado, es decir, formado por varias personas.

[^16]: > Máxima de cuatro años

[^17]: > Relacionar en su caso los bienes de forma individualizada.

[0]: https://www.gobiernodecanarias.org/cmsgobcan/export/sites/entidadesjuridicas/_galerias/tpc_documentos/tpc_asociaciones_docs/AS_102_MODELO_ESTATUTOS_ASOCIACION.odt
[1]: https://www.gobiernodecanarias.org/entidadesjuridicas/Servicios/asociaciones/
[2]: https://app.congreso.es/consti/constitucion/indice/titulos/articulos.jsp?ini=22&tipo=2
[3]: https://www.boe.es/eli/es/lo/2002/03/22/1
[4]: <https://www.boe.es/buscar/pdf/2003/BOE-A-2003-6500-consolidado.pdf>
[5]: <https://www.gobiernodecanarias.org/libroazul/pdf/56334.pdf>
[6]: <https://www.conceptosjuridicos.com/codigo-civil-articulo-39/>

*[HTML]: Hyper Text Markup Language The HTML specification is maintained by the W3C.
