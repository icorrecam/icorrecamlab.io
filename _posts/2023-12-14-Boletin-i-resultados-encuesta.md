---
title: Boletín I. Resultados de la encuesta
description: Resumen de la encuesta celebrada durante noviembre de 2023, en la que se realizaban una serie de preguntas relacionadas con las preferencias, en relación a la nueva asociación, de los participantes en diferentes grupos relacionados con la antigua Universidad Laboral.
# author: Luis Cabrera
author_profile: true
date: 2023-12-14 11:20:00 +0100
last_modified_at: 2023-12-19 21:35:00 +0100
locale: es
count_visit: true
related: true

header:
  teaser: assets/images/reunion-de-trabajo.jpg
  og_image: assets/images/reunion-de-trabajo.jpg
  overlay_image: assets/images/reunion-de-trabajo.jpg
  overlay_color: "#333"
  overlay_filter: 0.3
  caption: "Créditos: [Reuniones de trabajo](https://www.pxfuel.com/es/search?q=reuniones+de+trabajo), via [pxfuel](https://www.pxfuel.com/)"
  actions:
   - label: "Versión PDF"
     url: /assets/files/2023-12-14-Boletín-informativo-Resumen-encuesta.pdf

excerpt: |
  Adaptación al entorno web del resumen de la encuesta celebrada a finales de Octubre/23 en la que se realizaban una serie de preguntas relacionadas con las preferencias, en relación a la nueva asociación, de los participantes en diferentes grupos relacionados con la antigua Universidad Laboral.
tagline: |
  Adaptación al entorno web del resumen de la encuesta enviada durante *Noviembre de 2023* en la que se realizaban una serie de preguntas relacionadas con las preferencias, en relación a la nueva asociación, de los participantes en diferentes grupos relacionados con la antigua **Universidad Laboral**.
  {: style="text-align: justify; font-size:1.25em; color: #ffffff; background-color: #88888880; opacity: 1; padding: 12px;"}

show_date: true
show_time: true
# show_author: true
categories:
  - La Laboral
tags:
  - estatutos
  - encuesta
toc: true
# toc_label: Índice de contenidos
toc_icon: cog
toc_sticky: true
---


<!--
From ODT to markdown
pandoc --from odt --to markdown_phpextra --standalone --embed-resources --citeproc --wrap=none --no-highlight
-->

## Boletín Informativo 1

<span id="author" class="author">Equipo Alternativo de Trabaj🧭 ULFMonzón ⏳</span>  
<span id="revdate">2023-12-14 10:17:18 UTC</span>  
<span id="revremark">Draft</span>

LA LABORAL Y EL FELO

<span class="image">![image9](/assets/images/b01/image9.png)</span> <span class="image">![image4](/assets/images/b01/image4.png)</span> <span class="image">![image25](/assets/images/b01/image25.png)</span>

[EL BALCÓN DEL GUINIGUADA]: #

------------------------------------------------------------------------

Cuando les pedimos que nos echaran una mano contestando a nuestra encuesta, lo hicimos sabiendo que, una vez calmadas algunas de las emociones despertadas con el *cincuenta aniversario*, era posible que mucha gente no participara en ella. Sin embargo, ***fueron 85 las respuestas*** recibidas en la aplicación y al menos *cinco personas* que nos comunicaron su apoyo, pese a que tenían algunas dificultades para poder acceder a la misma, pero que deseaban mantenerse en contacto y que se contara con ellas para *futuras acciones*.

<span class="image">![image34](/assets/images/b01/image34.png "Cincuenta aniversario")</span>

En todo caso, de nuevo, muchísimas gracias.

------------------------------------------------------------------------

## <a href="#id_ya_tenemos_los_resultados_de_la_encuesta" class="anchor"></a><a href="#id_ya_tenemos_los_resultados_de_la_encuesta" class="link">Ya tenemos los resultados de la encuesta</a>

### <a href="#id_nos_echas_una_mano_contamos_contigo" class="anchor"></a><a href="#id_nos_echas_una_mano_contamos_contigo" class="link">¿Nos echas una mano? - Contamos contigo</a>

Con ese título y bajo una sencilla cabecera dedicada a *La Laboral* y al *Felo*, intentamos conocer tu opinión y asomarnos, como lo hacen algunas de las viejas residencias a lo que fue y es nuestro centro, un *balcón que se asoma al Guiniguada*, recordando nuestro pasado y pensando en el futuro, haciéndonos preguntas y reflexionando las respuestas, o simplemente contestando lo que el viento nos soplaba al oído y al corazón.

Una encuesta anónima con preguntas dirigidas a conocer tus recuerdos, tus intereses y tu opinión sobre la posibilidad de constituir una asociación como la que se había hablado en diferentes grupos de WhatsApp y en una anterior consulta realizada a comienzos del verano.

### <a href="#id_una_encuesta_motivadora" class="anchor"></a><a href="#id_una_encuesta_motivadora" class="link">Una encuesta motivadora</a>

Tras cerrar la participación en la encuesta que hicimos este noviembre, después de varios días sin recordar en los grupos y conocidos su existencia y sin hacer llamadas a que la contestasen, nos pusimos a valorar los resultados de la encuesta.

No obstante, antes de proceder a comentarlos aquí, nos gustaría señalar algunos aspectos del camino recorrido desde que hace unos meses, bien entrado el mes de julio y pasada la resaca del encuentro. Tras dejar que se calmasen las emociones despertadas, empezamos a afrontar la posibilidad de crear algo que le diese continuidad a lo sentido aquel diez de junio, entre quienes acudieron y también en quienes no pudieron ir o no se enteraron del encuentro.

Lo primero fue resolver, mediante una pequeña consulta, la posibilidad de crear una asociación o “algo así” que sirviese al más confesable de los deseos, recuperar el centro, sus espacios, los recuerdos, los contactos, volver a ser cómplices y renovar, en cierta medida, esa forma de ser cuando éramos lo que fuimos, compañeros.

Viendo las primeras respuestas y propuestas que aparecían en los grupos, entendimos que existían razones suficientes para intentar generar algo y se decidió que fuese una asociación. Así que nos pusimos a explorar las dificultades y las opciones, pero desde el inicio, ya había algunas dudas sobre qué tipo de asociación, para quienes, para hacer qué, etc.

Esperar para hacer esta encuesta tenía su razón de ser, calmar los ánimos y no tomar decisiones “en caliente”. Además, había que estudiar las alternativas y las posibilidades de crear algo que respondiese a lo que realmente queremos y sobre todo algo que pudiéramos gestionar desde un compromiso personal.

Total, que nos decidimos hacer una encuesta y preguntarles a ustedes y esto es lo que contestaron.

### <a href="#id_una_buena_participación" class="anchor"></a><a href="#id_una_buena_participación" class="link">Una buena participación</a>

***Ochenta y cinco personas*** contestaron a la encuesta, unas *50* el primer día y el resto a lo largo de una semana en la que se iba recordando la existencia de la encuesta en el WhatsApp de los grupos.

En todo caso, una buena respuesta a nuestro entender, pero sobre todo satisfactoria por las contestaciones obtenidas.

Además de esas *ochenta y cinco respuestas*, otras personas que manifestaron haber tenido alguna dificultad para poder contestarla, contactaron para mostrar su interés con el resultado y el contenido de la encuesta.

## <a href="#id_bloque_i_este_encuentro_y_muchos_más" class="anchor"></a><a href="#id_bloque_i_este_encuentro_y_muchos_más" class="link">Bloque I. Este encuentro y muchos más</a>

1. <span id="p01">**¿Estuviste en el encuentro de los cincuenta años?**.</span>

    *85 respuestas*

    Algo más de la mitad (*63.5%*) de los *85* encuestados estuvo en el encuentro y solo un *8%* de los encuestados no se había enterado de su celebración. Otros, un *28%* no pudo asistir, pero conocía su existencia.

    <span class="image">![¿Estuviste en el encuentro de los cincuenta años?](/assets/images/b01/image6.png)</span>

1. <span id="p02">**Si estuviste en el encuentro, ¿cómo te enteraste?.**</span>

    *67 respuestas*

    La mayor parte de los encuestados que asistieron al encuentro (*67*) se enteró por *WhatsApp*, (casi un *75%* si sumamos respuestas parecidas) y otro *9%* por redes sociales. Está claro que ese es el medio más utilizado y quizás el que debamos utilizar en el futuro.

    <span class="image"><img src="/assets/images/b01/image10.png" alt="Si estuviste en el encuentro" /></span>

1. <span id="p03">**Estuve y me encantó el encuentro. Deberían celebrarse cada …**</span>

    *65 respuestas*

    Además, de todas las personas asistentes encuestadas, casi un *80%* cree que este tipo de encuentros deberían celebrarse cada **4** ó **5** años si sumamos ambas respuestas (*29%* y *48%* respectivamente).

    <span class="image">![Estuve y me encantó el encuentro. Deberían celebrarse cada …](/assets/images/b01/image1.png)</span>

1. <span id="p04">**Me gustaría que además de un encuentro general, pudieran organizarse encuentros de promociones por años o por enseñanzas (de primaria, secundaria, bachiller, administrativo, metal etc.**</span>

    *79 respuestas*

    Asimismo, a un *91%* de las personas que contestaron les gustaría celebrarán encuentros de sus promociones o de su modalidad de estudios (bachillerato, formación profesional, familias o ramas).

    <span class="image"><img src="/assets/images/b01/image2.png" alt="Me gustaría que además de un encuentro general" /></span>

1. <span id="p05">**Si se hacen nuevos encuentros de todos o de mi promoción me presto a formar parte de la comisión organizadora o colaborar con ella.**</span>

    *82 respuestas*

    Un dato importante es que al menos *24* personas se ofrecen a colaborar en la organización de próximos encuentros, casi un *30%* de las *82* personas que contestaron a esta pregunta.

    <span class="image">![Si se hacen nuevos encuentros de todos o de mi promoción me presto a formar parte de la comisión organizadora o colaborar con ella.](/assets/images/b01/image28.png)</span>

## <a href="#id_bloque_ii_aquellos_años_y_nuestros_recuerdos" class="anchor"></a><a href="#id_bloque_ii_aquellos_años_y_nuestros_recuerdos" class="link">Bloque II. Aquellos años y nuestros recuerdos</a>

1. <span id="p06">**En estos cincuenta años de historia, sus aulas han visto pasar a mucha gente. ¿En qué años estuviste en el centro?.**</span>

    *82 respuestas*

    Las respuestas a este ítem de la encuesta han tenido que ser estudiadas de forma diferenciada, dado que el formato de las respuestas no permitía su agrupamiento automático.

    Sin embargo, se observa que, si bien la mayoría corresponde a los primeros *15* años, también han participado personas que estuvieron bien entrados los *80* y hasta en los *90*. Esto es natural si pensamos que los grupos donde circuló la encuesta corresponden, mayormente, a alumnado de los primeros años de existencia.

    <span class="image"><img src="/assets/images/b01/image11.png" alt="En estos cincuenta años de historia" /></span>

1. <span id="p07">**En esos años en el centro cursé diferentes estudios:**</span>

    *81 respuestas*

    La mayor parte de las personas encuestadas cursó estudios de bachillerato (*59%*) así como de Formación Profesional, aunque los grupos se solapan, explicándose en el hecho de que parte del alumnado culminaba unos estudios y continuaba en otros. No aporta gran información respecto a la trayectoria en los estudios, pero sí sobre los perfiles de quienes han participado y cursaron ambos ciclos de FP.

    <span class="image">![En esos años en el centro cursé diferentes estudios](/assets/images/b01/image16.png)</span>

1. <span id="p08">**En mi época curse estudios en régimen de alumnado:**</span>

    *82 respuestas*

    Quizás no se recordaba bien que, en sus inicios, había alumnado interno, mediopensionista (asistía, almorzaba y volvía a sus casas) y externos (solo a clases como el nocturno).

    <span class="image">![En mi época curse estudios en régimen de alumnado](/assets/images/b01/image15.png)</span>

    Pero al igual que en la pregunta anterior, los números se solapan). Está claro que era un descriptor de solo de la etapa de *La Laboral*.

1. <span id="p09">**¿Estuviste adscrito o adscrita a alguna residencia?.**</span>

    *78 respuestas*

    <span class="image">![¿Estuviste adscrito o adscrita a alguna residencia?](/assets/images/b01/image29.png)</span>

    Al igual que en anteriores preguntas sobre nuestro paso por el centro, parece que algunos aspectos se han olvidado o no se recuerdan con facilidad, como el hecho de que al menos hasta finales de los *70* el alumnado matriculado, independientemente de si era interno o mediopensionista, estaba adscrito a una residencia.

1. <span id="p10">**¿Recuerdas su nombre?.**</span>

    *28 respuestas*

    En la misma línea podemos afirmar que *57* personas no contestaron a esta pregunta, de las *28* que, si lo hicieron, *11* contestaron que no se acordaban, otras recordaban el número y otras a las tutoras o al tutor al cargo.

    <span class="image">![¿Recuerdas su nombre?](/assets/images/b01/image8.png)</span>

1. <span id="p11">**¿Participaste en alguna actividad cuando estabas en el centro?.**</span>

    *82 respuestas*

    <span class="image">![¿Participaste en alguna actividad cuando estabas en el centro?](/assets/images/b01/image19.png)</span>

    Gran parte de los que contestan recuerdan haber participado en actividades y en la gran oferta que había para realizar en el mismo centro, otros no se acuerdan de ellas… pero justo después, en la siguiente pregunta, se acordaron de muchas.

1. <span id="p12">**¿Recuerdas alguna de ellas?, ¿cuál?.**</span>

    *50 respuestas*

    <span class="image"><img src="/assets/images/b01/image27.png" alt="¿Recuerdas alguna de ellas?" /></span>

    *Cincuenta* personas se atrevieron a hacer memoria y recordarnos una lista de actividades que tuvimos la oportunidad de desarrollar en esos años, muchas personas iban a varias e incluso hubo quien pasaba más tiempo en ellas que en clase.

    Claro, con una oferta tan variada, hay quien iba hasta los sábados al centro.

## <a href="#id_bloque_iii_una_asociación_para_un_centro" class="anchor"></a><a href="#id_bloque_iii_una_asociación_para_un_centro" class="link">Bloque III. Una asociación para un centro</a>

1. <span id="p13">**Como hemos dicho, tras mucho pensarlo, vamos a poner en marcha una asociación para quienes en algún momento fuimos parte del centro. ¿Qué te parece a ti?.**</span>

    *77 respuestas*

    Mejor motivación imposible, *64* personas, casi tantas como las que dieron sus datos en la [pregunta *29*](#p20), para colaborar con la creación de una asociación, se manifiestan a favor de poner en marcha una asociación para quienes formamos parte tanto de La Laboral como del Felo Monzón.

    <span class="image"><img src="/assets/images/b01/image21.png" alt="Como hemos dicho" /></span>

1. <span id="p14">**Creemos que debe estar abierta no solo al alumnado que ha estado matriculado en el centro, sino que debe admitir también a quienes trabajaron en el centro a lo largo de los años. ¿Qué te parece?.**</span>

    *83 respuestas*

    <span class="image"><img src="/assets/images/b01/image31.png" alt="Creemos que debe estar abierta no solo al alumnado que ha estado matriculado en el centro" /></span>

    Y mejor aún, un *96%* cree, que debe estar abierta no solo a quienes allí estudiaron, sino a quienes trabajaron en el centro. Incluso hay quienes estudiaron y trabajaron en el centro.

1. <span id="p15">**Entre las finalidades de la asociación hemos pensado en las siguientes. ¿Qué te parecen?.**</span>

    *76 respuestas de media*

    De una media de *76* respuestas a cada una de las finalidades propuestas para la asociación, se obtuvieron prácticamente el *99%* de respuestas favorables a dichas propuestas que se incorporarán a la redacción de los estatutos de la asociación.

    <span class="image">![Entre las finalidades de la asociación hemos pensado en las siguientes. ¿Qué te parecen?](/assets/images/b01/image17.png)</span>

1. <span id="p16">**De las actividades propuestas para desarrollar por la asociación, hemos pensado algunas. ¿Qué te parecen?.
    (Valora del 1 al 4 su interés para ti).**</span>

    *78 respuestas*

    Algo parecido ocurrió con las actividades, la mayoría de las propuestas fue respaldada por unas *60* personas que situaron su interés en valores de *3-4* despertando mucho interés las dedicadas a actividades de carácter sociocultural y de ocio.

    <span class="image"><img src="/assets/images/b01/image3.png" alt="De las actividades propuestas para desarrollar por la asociación" /></span>

## <a href="#id_bloque_iv_participar_y_hacer_cosas" class="anchor"></a><a href="#id_bloque_iv_participar_y_hacer_cosas" class="link">Bloque IV. Participar y hacer cosas</a>

1. <span id="p17">**¿Participarías en la elaboración de una memoria gráfica y afectiva de nuestro centro?, ¿cómo un libro de con fotos y recuerdos?.**</span>

    *76 respuestas*

    <span class="image"><img src="/assets/images/b01/image18.png" alt="¿Participarías en la elaboración de una memoria gráfica y afectiva de nuestro centro?" /></span>

1. <span id="p18">**¿Mantienes o conservas fotos de esa época que estuvieses dispuesto a facilitar para un archivo histórico e incluso su publicación?.**</span>

    *76 respuestas*

    <span class="image">![¿Mantienes o conservas fotos de esa época que estuvieses dispuesto a facilitar para un archivo histórico e incluso su publicación?](/assets/images/b01/image30.png)</span>

    En este punto, se han de interpretar conjuntamente ambas preguntas y sus respuestas, las preguntas número [*17*](#p17) y [*18*](#p18), pues puede que el hecho de no disponer de materiales o fotos de aquella época que poder aportar (solo el *45%* dice tener), haya condicionado la disponibilidad a participar de un tercio (*34%*) de los que contestan la primera de estas dos preguntas, en la que por contraste ha sido la actividad que mayor apoyo tuvo en la pregunta anterior (la [*16*](#p16) de que actividades te gustaría afrontase la asociación). Pero seguro que recuerdos y anécdotas sí que existen y las fotos ya irán apareciendo, pero no estarán en el móvil.

1. <span id="p19">**¿Te gustaría poder contar con un directorio de los miembros de esta asociación para localizar antiguos compañeros y compañeras?.**</span>

    *78 respuestas*

    Curiosamente, esta propuesta ha sido la que mayor interés despierta, *74* de las *78* personas que contestan quisieran poder acceder, a través de un directorio, a los datos de contacto de antiguos compañeros y compañeras, tarea que no será fácil pero tampoco imposible con respeto a las normas sobre protección de datos.

    <span class="image">![¿Te gustaría poder contar con un directorio de los miembros de esta asociación para localizar antiguos compañeros y compañeras?](/assets/images/b01/image5.png)</span>

1. <span id="p20">**Si se organizasen, me encantaría poder participar en actividades.**</span>

    *73 respuestas*

    <span class="image"><img src="/assets/images/b01/image33.png" alt="Si se organizasen" /></span>

    De las personas que contestan, *71* están interesadas en hacer excursiones y senderismo, más de la mitad en organizar viajes y visitas a otras islas, unas *30* tareas de voluntariado ambiental y *20* acciones con valor social.

    Pero también las hay interesadas en actividades musicales, artísticas y culturales, destacando entre ellas un club de lectura.

1. <span id="p21">**Además de las del punto anterior me gustaría proponer…**</span>

    *7 respuestas*

    Solo hubo *7* personas que propusieron actividades, entre las que se destaca organizar escuelas de verano, talleres de manualidades, encuentros en islas orientales, actividades deportivas y equipos seniors, club de cine, jornadas de transmisión de experiencias al alumnado actual, etc.

## <a href="#id_bloque_v_nosotros_el_centro_y_la_nueva_asociación" class="anchor"></a><a href="#id_bloque_v_nosotros_el_centro_y_la_nueva_asociación" class="link">Bloque V. Nosotros, el centro y la nueva asociación.</a>

1. <span id="p22">**¿Conoces el proyecto que está desarrollando el centro para convertirse en un referente de buenas prácticas el marco de la sostenibilidad, de las energías renovables y crear una comunidad energética?.**</span>

    *82 respuestas*

    De las *82* personas que contestaron *sesenta y una*, es decir, el *74%* manifestó desconocer uno de los proyectos más emblemáticos del actual centro, especialmente relacionado con algunos aspectos más vinculados al futuro y a la comunidad (energía-medioambiente) y eso junto a otros proyectos que también desarrolla el centro, deberían ser de nuestro interés.

    <span class="image"><img src="/assets/images/b01/image24.png" alt="¿Conoces el proyecto que está desarrollando el centro para convertirse en un referente de buenas prácticas el marco de la sostenibilidad" /></span>

1. <span id="p23">**¿Te gustaría recibir información de esta iniciativa y de otras que se desarrollan ya en el centro?.**</span>

    *76 respuestas*

    <span class="image">![¿Te gustaría recibir información de esta iniciativa y de otras que se desarrollan ya en el centro?](/assets/images/b01/image32.png)</span>

    Esa necesidad de que la asociación sirva para conectar con el centro y visibilizar nuestra capacidad e interés en lo que se hace en él viene expresado en la respuesta a esta pregunta donde casi el *90%* quiere estar informado de lo que se hace y, posiblemente, como veremos ahora, de cómo podemos ayudar.

1. <span id="p24">**¿Te gustaría participar o colaborar con el centro para que estos proyectos salieran adelante?.**</span>

    *71 respuestas*

    En el mismo sentido, un buen grupo de personas expresan su deseo de participar y colaborar con el centro para contribuir a que los proyectos en marcha en el centro y otros que puedan surgir, como el mencionado en la pregunta [*22*](#p22).

    <span class="image">![¿Te gustaría participar o colaborar con el centro para que estos proyectos salieran adelante?](/assets/images/b01/image35.png)</span>

1. <span id="p25">**¿Te gustaría y estás dispuesto a participar en proyectos, pequeñas tareas o trabajos voluntarios para recuperar o restaurar algunos entornos y enclaves del centro?.**</span>

    *70 respuestas*

    Esta pregunta, fruto de algunas de las inquietudes y propuestas de prestarse a colaborar, realizadas en los comentarios de varios grupos de *WhatsApp* tras el encuentro, se incorporó a la encuesta y ha sido acogida favorablemente por casi un *70%* de los participantes, hecho que podrá comunicarse al centro por si desea organizar alguna actividad altruista en ese sentido.

    <span class="image"><img src="/assets/images/b01/image13.png" alt="¿Te gustaría y estás dispuesto a participar en proyectos" /></span>

1. <span id="p26">**¿Qué acciones podrían abordarse de forma altruista y voluntaria para el arreglo de distintos lugares del centro?.**</span>

    *70 respuestas*

    La voluntad, expresada en las respuestas a la pregunta anterior, de colaborar en tareas de mejora y rehabilitación se intentó concretar proponiendo algunas de las ideas existentes en los foros de los participantes en el encuentro y donde se destacan las acciones relacionadas con recuperar zonas verdes y la rehabilitación de algunos espacios emblemáticos.

    <span class="image">![¿Qué acciones podrían abordarse de forma altruista y voluntaria para el arreglo de distintos lugares del centro?](/assets/images/b01/image22.png)</span>

1. <span id="p20">**¿Estarías en disposición de participar desde tu experiencia personal o profesional en acciones, en colaboración con la asociación, que contribuyan a la mejora del centro o al desarrollo de los proyectos de interés que así se decidan?.**</span>

    *71 respuestas*

    <span class="image"><img src="/assets/images/b01/image14.png" alt="¿Estarías en disposición de participar desde tu experiencia personal o profesional en acciones" /></span>

    De la misma forma, y como expresión de la voluntad de los participantes, aparece una disposición de casi un *70%* en poder desarrollar estas acciones de forma colaborativa y desde la asociación como mecanismo de interlocución con el centro y mecanismo estable de relación.

1. <span id="p28">**Me gustaría recibir información directamente de la asociación (eventos, formación, ofertas de actividades, etc.) a través de…**</span>

    *76 respuestas*

    <span class="image"><img src="/assets/images/b01/image12.png" alt="Me gustaría recibir información directamente de la asociación (eventos" /></span>

    Esta pregunta, sin más interés que conocer como quieren las personas, que han participado en la encuesta, mantener el contacto (podían marcar varias), para acciones y decisiones futuras, lo cual será tenido en cuenta para generar una lista de correo con boletines, borradores de estatutos, convocatorias, etc.

1. <span id="p29">**Si quieres recibir información o colaborar con la creación de la asociación, ¿nos podrías facilitar aquí un email o un número para el whatsapp para contactar (y un nombre)?.**</span>

    *68 respuestas*

    Lo más significativo de esta pregunta de la encuesta, es que pese a tener carácter anónimo, *sesenta y ocho personas* facilitaron voluntariamente un email o un número de móvil para mantenerse en contacto y colaborar con la creación de la asociación y esa es posiblemente la mejor noticia.

<br>
<br>
<br>

Gracias a todas y a todos.
