---
title: Versión preliminar de los nuevos estatutos
description: Adaptación al entorno web de la versión preliminar de los estatutos de la asociación.
# author: Luis Cabrera
author_profile: true
date: 2023-12-14 10:20:00 +0100
last_modified_at: 2023-12-19 21:35:00 +0100
locale: es
count_visit: true
related: true

header:
  teaser: https://focse.es/wp-content/uploads/2016/03/titulo-estatutos.jpg
  og_image: https://focse.es/wp-content/uploads/2016/03/titulo-estatutos.jpg
  overlay_image: https://focse.es/wp-content/uploads/2016/03/titulo-estatutos.jpg
  overlay_color: "#333"
  overlay_filter: 0.3
  caption: "Créditos: [**FOCSE**](https://focse.es/estatutos-de-la-focse/)"
  actions:
   - label: "Versión PDF"
     url: /assets/files/2023-12-14-Estatutos-version-preliminar.pdf

excerpt: |
  **Versión preliminar** de los futuros estatutos de la asociación.
tagline: |
  **:warning:Versión preliminar:warning:** de los futuros estatutos de la asociación.
  {: style="text-align: justify; font-size:1.25em; color: #ffffff; background-color: #88888880; opacity: 1; padding: 12px;"}

show_date: true
show_time: true
# show_author: true
categories:
  - La Laboral
tags:
  - estatutos
  - transcripción
toc: true
# toc_label: Índice de contenidos
toc_icon: cog
toc_sticky: true
---

<!--
From ODT to markdown
pandoc --from odt --to markdown_phpextra --standalone --embed-resources --citeproc --wrap=none --no-highlight
-->

## Estatutos de la <ins>NOMBRE DEFINITIVO POR DETERMINAR</ins>[^nombre]<br><br>Estatutos sociales

<span id="author" class="author">Equipo Alternativo de Trabaj🧭 ULFMonzón ⏳</span>  
<span id="revdate">2023-12-14 10:20:07 UTC</span>  
<span id="revremark">Draft</span>

> :warning:  **IMPORTANTE:** Este documento sigue siendo un borrador. Si ven alguna errata o algún otro problema reseñable, agradeceríamos que nos lo comunicaran.  
<br>
Gracias.
{: .notice--danger}
{: style="text-align: left; font-size:1.75em; color: #000000;"}

[CAPÍTULO I - DE LA ASOCIACIÓN EN GENERAL.](#id_capítulo_i_de_la_asociación_en_general "#id_capítulo_i_de_la_asociación_en_general")
------------------------------------------------------------------------------------------------------------------------------------

#### [Artículo 1.— Denominación.](#id_artículo_1_denominación "#id_artículo_1_denominación")

De conformidad con el [artículo 22 de la Constitución Española](https://app.congreso.es/consti/constitucion/indice/titulos/articulos.jsp?ini=22&tipo=2 "https://app.congreso.es/consti/constitucion/indice/titulos/articulos.jsp?ini=22&tipo=2") se constituye la entidad, no lucrativa, denominada <ins>NOMBRE DEFINITIVO POR DETERMINAR</ins>[^nombre], dotada de personalidad jurídica propia y capacidad de obrar y que se regirá por la [Ley Orgánica 1/2002 de 22 de marzo](https://www.boe.es/eli/es/lo/2002/03/22/1 "https://www.boe.es/eli/es/lo/2002/03/22/1"), reguladora del Derecho de Asociación, [la Ley 4/2003, de 28 de febrero, de Asociaciones de Canarias](https://www.boe.es/buscar/pdf/2003/BOE-A-2003-6500-consolidado.pdf "https://www.boe.es/buscar/pdf/2003/BOE-A-2003-6500-consolidado.pdf"), [el Decreto 12/2007, de 5 de febrero, por el que se aprueba el Reglamento de Asociaciones de Canarias](https://www.gobiernodecanarias.org/libroazul/pdf/56334.pdf "https://www.gobiernodecanarias.org/libroazul/pdf/56334.pdf"), demás disposiciones complementarias y por los presentes Estatutos.

#### [Artículo 2.— Fines de la asociación.](#id_artículo_2_fines_de_la_asociación "#id_artículo_2_fines_de_la_asociación")

La propuesta de la creación de esta asociación nace tras el encuentro realizado durante la celebración del 50º aniversario de la antigua «***Universidad Laboral de Las Palmas***», luego denominada como «*Centro de Enseñanzas Integradas*», y en el momento de redactar este documento, conocido como «*CIFP Felo Monzón Grau Bassas*», situada en la ***Carretera Lomo Blanco, nº 48, Tafira Baja, 35015, de Las Palmas de Gran Canaria*** y que nosotros, para simplificar, y a partir de este momento, en este documento, denominaremos como «*el centro*».

Los fines de la Asociación serán los siguientes:

1. Servir de punto de encuentro y de contacto para todas aquellas personas que en algún momento formaron parte de la comunidad educativa «*del centro*».
2. Mantener y conservar la memoria «*del centro*» y los valores democráticos y solidarios que forjaron la idiosincrasia del mismo en sus diferentes épocas, desde su creación hasta la actualidad.
3. Contribuir a la conservación de la memoria académica «*del centro*» y su legado como institución educativa de referencia desde su creación como ejemplo del compromiso con el progreso y desarrollo de la sociedad canaria.
4. Fomentar el intercambio de experiencias entre las personas formadas en «*el centro*», las diferentes generaciones de docentes y la propia institución como vía tanto de implementación de la formación recibida, la posibilidad de servir de apoyo y recursos al alumnado actual y a los proyectos que se pongan en marcha por «*el centro*».
5. Facilitar la realización de actividades de carácter cultural, solidario y participativo entre los miembros de la asociación, que fomenten la relación entre las distintas generaciones de alumnado, el profesorado y la continuidad de su proyección social, cultural y formativa.
6. Colaborar con todos aquellos proyectos que, partiendo de los asociados, tengan como objetivo recuperar el contacto entre el alumnado y el profesorado de las distintas épocas, enseñanzas, promociones.
7. Organizar, participar y colaborar con aquellas actividades de interés general que, en beneficio de la sociedad canaria, puedan tener lugar en las instalaciones «*del centro*».
8. Actuar en favor de valores como la defensa del medio ambiente, la sostenibilidad, el fomento de la cultura, el ejercicio de la tolerancia y la defensa de los derechos humanos, la solidaridad y la interculturalidad.
9. Establecer una coordinación entre el actual «*centro*» y la asociación para el desarrollo de todas aquellas acciones de apoyo necesarias para la mejora, conservación y mantenimiento de las instalaciones, equipamientos e infraestructuras «*del centro*».
10. Apoyar «*al centro*» para que pueda incrementar y diversificar su oferta educativa, así como para el desarrollo de proyectos de innovación y formación para Canarias y su entorno.

#### [Artículo 3.— Actividades.](#id_artículo_3_actividades "#id_artículo_3_actividades")

Para el cumplimiento de sus fines, la Asociación podrá desarrollar, entre otras, las siguientes actividades:

1. Organización de encuentros abiertos de antiguos miembros de la comunidad educativa (alumnado, profesorado, personal no docente).
2. Realización de actividades culturales, deportivas y de ocio, dirigidas a antiguos miembros de la comunidad educativa.
3. Creación de un directorio de asociados que, respetando la normativa en materia de protección de datos, permita disponer de una red para la información y difusión de contactos, iniciativas y actividades que se desarrollen.
4. Recolección de anécdotas, imágenes y registros gráficos, orales o escritos para la reconstrucción de una memoria histórica, afectiva y emocional de las distintas épocas, etapas, promociones y hechos más relevantes «del centro» a lo largo de su existencia.
5. Creación de un foro para compartir experiencias laborales y profesionales de personas formadas en «el centro» y el actual alumnado del mismo.
6. Promoción de bolsas de empleo, prácticas en empresas, formación dual e iniciativas de inmersión social y laboral de nuevas promociones «del centro».
7. Diseño y ejecución de actividades formativas en colaboración con «el centro» para el reciclaje y actualización del antiguo alumnado, destinadas a mejorar la cualificación profesional y facilitar el acceso a nuevas modalidades de empleo y emprendeduría.
8. Organización y desarrollo de actividades de mecenazgo y voluntariado para la conservación, rehabilitación o mejora de instalaciones, equipamientos y espacios.
9. Realización de eventos destinados a la recaudación de fondos para el desarrollo de las actividades y consecución de los fines de la asociación.
10. Diseño de un espacio digital que sirva para mantener el contacto entre asociados y de comunicación de las actividades de la asociación.
11. Desarrollo de debates, seminarios, jornadas y encuentros destinados a la difusión y búsqueda de apoyo a proyectos de interés educativo y social.

Los beneficios eventualmente obtenidos por el desarrollo de estas actividades, se dedicarán a realizar otras de la misma naturaleza, conforme a los fines sociales.

#### [Artículo 4.— Domicilio.](#id_artículo_4_domicilio "#id_artículo_4_domicilio")

1. La asociación tendrá su domicilio social en la siguiente [dirección postal](https://es.wikipedia.org/wiki/Direcci%C3%B3n_postal#Espa%C3%B1a "https://es.wikipedia.org/wiki/Direcci%C3%B3n_postal#Espa%C3%B1a")[^domicilio]:

Tabla 1. Dirección postal de la asociación.

| DIRECCIÓN O RAZÓN SOCIAL |
| ------------------- |
| CALLE, PLAZA o VÍA |
| LOCALIDAD, ALDEA, URBANIZACION… |
| CÓDIGO, POSTAL Y MUNICIPIO |
| PROVINCIA, PAÍS |

2. El citado domicilio podrá ser modificado, mediante acuerdo de la Asamblea General, de conformidad con lo dispuesto en estos Estatutos y deberá ser notificado al [Registro de Asociaciones](https://www.gobiernodecanarias.org/entidadesjuridicas/Servicios/asociaciones/ "https://www.gobiernodecanarias.org/entidadesjuridicas/Servicios/asociaciones/"), por la Secretaría, a efectos de publicidad.
3. A los efectos de favorecer la comunicación telemática y el uso de medios electrónicos para la gestión y relación con sus asociados y con las administraciones e instituciones públicas y/o privadas, podrá dotarse de una dirección de correo electrónico que a dichos efectos comunicará al Registro de Asociaciones.

#### [Artículo 5.— Ámbito territorial y duración.](#id_artículo_5_ámbito_territorial_y_duración "#id_artículo_5_ámbito_territorial_y_duración")

La Asociación tendrá como ámbito territorial de actuación el ámbito autonómico.

La duración será por tiempo indefinido. El acuerdo de disolución se adoptará conforme a lo indicado en los presentes Estatutos.

[CAPÍTULO II.— DE LOS ÓRGANOS DIRECTIVOS Y ADMINISTRACIÓN.](#id_capítulo_ii_de_los_órganos_directivos_y_administración "#id_capítulo_ii_de_los_órganos_directivos_y_administración")
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#### [Artículo 6.— Órganos sociales.](#id_artículo_6_órganos_sociales "#id_artículo_6_órganos_sociales")

Son órganos de la asociación:

* La Asamblea General.
* Junta Directiva.

Adicionalmente, la Asociación podrá constituir en su seno órganos destinados a la promoción de sus fines, tales como «*Comisiones de actividades*» o cualesquiera otros órganos de carácter consultivo.

### [SECCIÓN PRIMERA.— DE LA ASAMBLEA GENERAL.](#id_sección_primera_de_la_asamblea_general "#id_sección_primera_de_la_asamblea_general")

#### [Artículo 7.— Carácter y composición de la Asamblea general.](#id_artículo_7_carácter_y_composición_de_la_asamblea_general "#id_artículo_7_carácter_y_composición_de_la_asamblea_general")

La Asamblea General es el órgano supremo de la Asociación, integrada por todos los asociados, que adoptará sus acuerdos bajo el principio democrático de mayorías de los votos válidamente emitidos.

Para sus deliberaciones y acuerdos se podrá reunir con carácter ordinario o extraordinario, a propuesta de su Junta Directiva, o a solicitud de un porcentaje representativo de sus miembros, con carácter extraordinario.

Deberá ser convocada al menos en carácter ordinario una vez al año, dentro del primer trimestre anual, para examinar y aprobar la liquidación anual de las cuentas del ejercicio anterior, el presupuesto del ejercicio corriente y la memoria de actividades del ejercicio anterior.

Asimismo, se podrá convocar en carácter extraordinario cuando así lo acuerde la Junta Directiva o cuando lo soliciten un número de asociados ***no inferior a un 25%***, acompañada del punto del orden del día que la justifica.

En el supuesto de que la convocatoria se efectúe a iniciativa de los asociados, la reunión deberá celebrarse en el plazo de ***treinta días naturales*** desde la presentación de la solicitud.

#### [Artículo 8.— Convocatorias y Orden del Día.](#id_artículo_8_convocatorias_y_orden_del_día "#id_artículo_8_convocatorias_y_orden_del_día")

Las Asambleas Generales, sea cual sea su carácter, serán convocadas por el Presidente, haciendo expresa indicación del orden del día establecido por la Junta Directiva o por los asociados que hayan solicitado su convocatoria.

Entre la convocatoria y el día señalado para la celebración de la Asamblea General en primera convocatoria habrán de mediar al menos ***15 días***, pudiendo asimismo hacerse constar la fecha en la que, si procediera, se reunirá la Asamblea General en segunda convocatoria, sin que entre una y otra reunión no pueda mediar un plazo inferior a una hora.

Por razones de urgencia y previo acuerdo de la Junta Directiva podrán reducirse los mencionados plazos a ***un mínimo de 48 horas*** las convocatorias que, en todo caso, serán extraordinarias.

Con el fin de facilitar la presencia del mayor número de asociados, la asistencia a las Asambleas podrá realizarse presencialmente o mediante la utilización de medios telemáticos de la forma que se determine en su convocatoria, haciendo uso de plataformas digitales.

En todos los casos, deberá contemplarse en la convocatoria, los plazos necesarios para la inclusión, en el orden del día, de aquellos asuntos que propongan los asociados, cuando así lo solicite un porcentaje ***no inferior al 10%*** de las personas asociadas.

#### [Artículo 9.— Constitución de la Asamblea General.](#id_artículo_9_constitución_de_la_asamblea_general "#id_artículo_9_constitución_de_la_asamblea_general")

Las Asambleas Generales, tanto ordinarias como extraordinarias, presenciales, semipresenciales, o telemáticas, quedarán válidamente constituidas, en primera convocatoria, cuando concurran a ella, presentes o representados, ***un tercio de los asociados***, y en la segunda convocatoria cualquiera que sea el número de asociados concurrentes, debidamente acreditados.

Los asociados podrán conferir, con carácter especial para cada reunión, su representación a otro asociado o persona que estimen procedente, mediante escrito dirigido a la Presidencia.

Como caso excepcional, la Asamblea General Extraordinaria queda válidamente constituida, sin convocatoria previa, si están presentes todos y cada uno de los miembros de la asociación y acuerdan, por unanimidad, la celebración de la misma y el orden del día a tratar.

#### [Artículo 10.— Adopción de acuerdos.](#id_artículo_10_adopción_de_acuerdos "#id_artículo_10_adopción_de_acuerdos")

Los acuerdos de la Asamblea General se adoptarán por mayoría simple de los asociados presentes o representados. Se entiende que se produce mayoría simple cuando los votos afirmativos *superen* los negativos.

Requerirán mayoría absoluta de los asociados presentes o representados, los siguientes acuerdos:

1. Relativos a la disolución de la entidad.
2. Modificación de los Estatutos.
3. Disposición o enajenación de bienes.
4. Debate y votación de mociones de censura a la Junta Directiva.
5. Remuneración de los miembros de la Junta Directiva.
6. Integración o inclusión en federaciones o confederaciones de asociaciones.

Se entiende por mayoría absoluta el voto favorable de al menos la mitad más uno de todos los miembros asistentes y representados con derecho a voto.

Los acuerdos adoptados en las Asambleas Generales se harán constar en las respectivas actas, que serán certificadas y firmadas por la Secretaría y la Presidencia de la Asociación.

#### [Artículo 11.— Funciones de la Asamblea general.](#id_artículo_11_funciones_de_la_asamblea_general "#id_artículo_11_funciones_de_la_asamblea_general")

Corresponde a la Asamblea General, deliberar y tomar acuerdos sobre los siguientes asuntos:

1. Examinar y aprobar el Plan General de actuación y la Memoria anual de actividades que le presente la Junta Directiva.
2. Aprobar el Presupuesto anual de gastos e ingresos del siguiente año y el estado de cuentas del ejercicio anterior.
3. Decidir sobre la disposición o enajenación de bienes.
4. Elegir y separar a los miembros de la Junta Directiva.
5. Solicitar la declaración de utilidad pública o interés público canario.
6. Acordar la integración de la asociación en Federaciones o Confederaciones, así como la separación de las mismas.
7. Controlar la actividad de la Junta Directiva y aprobar su gestión.
8. Modificar los Estatutos.
9. Acordar la disolución de la Asociación.
10. Designar la Comisión Liquidadora en caso de disolución.
11. Acordar la remuneración de los miembros de la Junta Directiva, en su caso.
12. Ratificar las altas acordadas por la Junta Directiva y conocer las bajas voluntarias de los asociados.
13. Resolver, en última instancia, los expedientes relativos a sanción y separación de los asociados, tramitados conforme al procedimiento disciplinario establecido en los presentes Estatutos.
14. Otras que le sean de su competencia en atención a la normativa aplicable.

#### [Artículo 12.— Certificación de acuerdos.](#id_artículo_12_certificación_de_acuerdos "#id_artículo_12_certificación_de_acuerdos")

En las Asambleas Generales actuarán como Presidente o Presidenta y Secretario o Secretaria, quienes lo sean de la Junta Directiva.

Los acuerdos adoptados por la Asamblea General serán recogidos en un acta elaborada y firmada por el Secretario de la Junta Directiva y deberá contar con el visto bueno, mediante su firma, de la Presidencia de la Asociación.

Cualquier miembro asistente tiene derecho a solicitar que su intervención o propuestas sean incluidas en el acta, aportando, para ello, su intervención o su propuesta, por escrito.

### [SECCIÓN SEGUNDA.— DEL ÓRGANO DE REPRESENTACIÓN.](#id_sección_segunda_del_órgano_de_representación "#id_sección_segunda_del_órgano_de_representación")

#### [Artículo 13.— Definición del órgano de representación.](#id_artículo_13_definición_del_órgano_de_representación "#id_artículo_13_definición_del_órgano_de_representación")

El órgano de representación de la asociación se denomina «Junta Directiva».

Este órgano gestiona y representa los intereses de la Asociación, de acuerdo con las disposiciones y directivas de la Asamblea General.

La Junta directiva será órgano competente para interpretar los preceptos de estos Estatutos y cubrir sus lagunas, siempre sometiéndose a la normativa legal vigente en materia de asociaciones.

Solo podrán formar parte de la Junta Directiva los asociados.

Los cargos directivos no serán retribuidos y carecerán de interés por sí mismos o a través de personas interpuestas, en los resultados económicos de la realización de las actividades, salvo que la asamblea general, por acuerdo adoptado con mayoría absoluta, acuerde lo contrario.

#### [Artículo 14.— Miembros del órgano de representación.](#id_artículo_14_miembros_del_órgano_de_representación "#id_artículo_14_miembros_del_órgano_de_representación")

Serán requisitos indispensables para ser miembro de la Junta Directiva:

1. Ser miembro de la Asociación.
2. Ser mayor de edad.
3. Estar en pleno uso de los derechos civiles.
4. No estar incurso en los motivos de incompatibilidad establecidos en la legislación vigente.

#### [Artículo 15.— Convocatorias, orden del día y constitución.](#id_artículo_15_convocatorias_orden_del_día_y_constitución "#id_artículo_15_convocatorias_orden_del_día_y_constitución")

Las reuniones de la Junta Directiva se celebrarán, previa convocatoria de la Presidencia, con ***7 días de antelación*** acompañada del orden del día, consignando lugar, fecha y hora.

En el caso de que la convocatoria no incluyese el lugar de celebración, se entenderá a todos los efectos el domicilio social.

Por motivos de urgencia justificada podrán convocarse ***con 48 horas de antelación*** las reuniones de la Junta Directiva.

Las reuniones de la Junta Directiva podrán celebrarse de manera telemática, siguiendo el mismo procedimiento establecido para las reuniones presenciales, con la salvedad de que para las reuniones telemáticas, deberá incluirse, de forma obligatoria, la manera en la que se podrá obtener acceso a la reunión en la plataforma que haya sido seleccionada.

Se reunirá de forma periódica por lo menos ***4 veces al año*** y siempre que lo estime necesario la Presidencia o lo soliciten la mayoría de sus miembros.

Para su válida constitución será precisa la asistencia de, al menos, ***un tercio*** de sus componentes, presentes o representados, incluida la presencia de la persona que ostente la presidencia y la que haga las funciones de la secretaría de la Asociación, quien dará fe de lo tratado.

Los acuerdos se adoptarán por mayoría simple de los miembros presentes o representados, salvo aquellos relativos a sanción o separación de los asociados, en los cuales se precisará mayoría absoluta de los miembros presentes o representados.

La representación solamente podrá conferirse a otro miembro de la Junta Directiva, con carácter especial para cada reunión y mediante carta dirigida a la presidencia.

#### [Artículo 16.— Composición, duración y vacantes.](#id_artículo_16_composición_duración_y_vacantes "#id_artículo_16_composición_duración_y_vacantes")

Estará integrada por las personas que ostenten las responsabilidades de los siguientes cargos:

1. Presidencia
2. Vicepresidencia
3. Secretaría
4. Tesorería
5. Un máximo de 10 Vocalías

Dichos cargos, que serán voluntarios, y, por tanto, deberán ser aceptados expresamente en documento firmado por las personas designadas, tendrán una duración de ***cuatro años***, pudiendo ser reelegidos.

Los miembros de la Junta Directiva comenzarán a ejercer sus funciones una vez aceptado el mandato para el que hayan sido designados por la Asamblea General.

Las vacantes que se produzcan en la Junta Directiva, antes de terminar su período de mandato, serán cubiertas por los asociados que designe la propia Junta Directiva, dando cuenta de las sustituciones en la primera Asamblea General que se celebre, debiendo ratificarse dicho acuerdo por la Asamblea. En caso contrario, se procederá a la elección del asociado que debe cubrir la vacante en la misma sesión de la Asamblea.

#### [Artículo 17.— Causas de cese.](#id_artículo_17_causas_de_cese "#id_artículo_17_causas_de_cese")

Los miembros de la Junta Directiva podrán ser separados de sus cargos por los siguientes motivos:

1. Por renuncia voluntaria.
2. Por muerte o declaración de fallecimiento, enfermedad o cualquier otra causa que le impida el ejercicio de sus funciones.
3. Por pérdida de la cualidad de socio.
4. Por incapacidad, inhabilitación o incompatibilidad de acuerdo con la legislación vigente.
5. Por el transcurso del período de su mandato.
6. Por separación acordada por la Asamblea General.
7. La comisión de una infracción muy grave, conforme al [Artículo 40](#articulo-40 "#articulo-40") de los presentes estatutos.

La Junta Directiva dará cuenta a la Asamblea General de la separación de sus miembros, debiendo ratificarse por la Asamblea cuando el acuerdo de separación haya sido adoptado por el motivo expresado en el apartado 7 de este mismo artículo.

#### [Artículo 18.— Atribuciones del órgano de representación.](#id_artículo_18_atribuciones_del_órgano_de_representación "#id_artículo_18_atribuciones_del_órgano_de_representación")

Las facultades de la Junta Directiva se extenderán, con carácter general, a todos los actos propios de las finalidades de la asociación, siempre que no requieran, conforme a los presentes Estatutos, autorización expresa de la Asamblea General.

En particular, son facultades de la Junta Directiva:

1. Velar por el cumplimiento de los Estatutos y ejecutar los acuerdos tomados en las Asambleas Generales.
2. Confeccionar las memorias, cuentas, inventarios, balances y presupuestos de la Asociación.
3. Elaborar el borrador del Reglamento de Régimen Interior.
4. Acordar la celebración de actividades.
5. Tener a disposición de los asociados el Libro de Registro de Asociados.
6. Tener a disposición de los asociados los Libros de Actas y de Contabilidad; así como la documentación de la entidad.
7. Recaudar la cuota de los asociados y administrar los fondos sociales.
8. Instruir los expedientes relativos a la sanción y separación de los asociados y adoptar, de forma cautelar, la resolución que proceda de los mismos, hasta su resolución definitiva por la Asamblea General.
9. Proponer a la Asamblea General para su aprobación las cuentas anuales y el presupuesto formulados desde la Tesorería, así como la memoria de actividades formuladas desde la Secretaría.
10. Admitir, con carácter provisional,a nuevas personas como asociadas, una vez presentada la correspondiente solicitud, hasta su ratificación por la Asamblea General y resolver las dudas que se planteen sobre la concurrencia de los requisitos de admisión.

#### [Artículo 19.— Funciones de la Presidencia.](#id_artículo_19_funciones_de_la_presidencia "#id_artículo_19_funciones_de_la_presidencia")

Serán funciones de la persona que ocupe la presidencia:

1. Ostentar la representación legal de la Asociación.
2. Convocar y presidir las reuniones de la Junta Directiva y de la Asamblea General, de acuerdo con lo establecido en los presentes Estatutos.
3. Velar por el cumplimiento de los fines sociales.
4. Autorizar con su firma las actas, certificaciones y demás documentos de la Asociación.
5. Solicitar, percibir o hacer efectivas las ayudas, subvenciones o patrocinios que, por cualquier concepto, provengan de organismos públicos o entidades privadas, así como contratar y convenir con entes públicos de cualquier naturaleza y privados.
6. Y cuantas facultades se le confieran, no expresamente asignadas a otros órganos.

#### [Artículo 20.— Funciones de la Vicepresidencia.](#id_artículo_20_funciones_de_la_vicepresidencia "#id_artículo_20_funciones_de_la_vicepresidencia")

Serán funciones de la persona que ocupe la Vicepresidencia:

1. Sustituir a la persona que ocupe la Presidencia en caso de vacante, ausencia o enfermedad, asumiendo sus competencias, con carácter provisional, cuando la persona titular cese en el cargo.
2. Las que le deleguen la Presidencia o la Asamblea General.

#### [Artículo 21.— Funciones de la Secretaría.](#id_artículo_21_funciones_de_la_secretaría "#id_artículo_21_funciones_de_la_secretaría")

Serán funciones de la persona encargada de la Secretaría:

1. Redactar y certificar las actas de las sesiones de las Asambleas Generales y de la Junta Directiva.
2. Llevar el Libro del Registro de Asociados, consignando en ellos la fecha de su ingreso y las bajas que hubiere.
3. Recibir y tramitar las solicitudes de ingreso.
4. Llevar una relación del inventario de la Asociación.
5. Tener bajo su custodia los documentos y archivos de la Asociación.
6. Expedir certificaciones.
7. Formular la Memoria de Actividades.
8. Gestión de los recursos telemáticos (email, cuentas sociales, dominio y alojamiento web, etc.).
9. Notificación del cambio de domicilio al [organismo adecuado](#domicilio "#domicilio").

#### [Artículo 22.— Funciones de la Tesorería.](#id_artículo_22_funciones_de_la_tesorería "#id_artículo_22_funciones_de_la_tesorería")

Son funciones de la persona que ocupe la Tesorería:

1. Tendrá a su cargo los fondos pertenecientes a la Asociación.
2. Elaborar los presupuestos, balances e inventarios de la Asociación.
3. Firmará los recibos, cobrará las cuotas de los asociados y efectuará todos los cobros y pagos.
4. Llevar y custodiar los Libros de Contabilidad.
5. Formular las cuentas anuales y el presupuesto.

### [Artículo 23.— Funciones de las Vocalías.](#id_artículo_23_funciones_de_las_vocalías "#id_artículo_23_funciones_de_las_vocalías")

Las personas que ocupen las vocalías tendrán el derecho y la obligación de asistir a las sesiones de la Junta Directiva con voz y con voto, así como, en régimen de delegación, podrán desempeñar las funciones que les confiera la Junta Directiva o la Asamblea General para coordinar las posibles comisiones que se creen.

### [SECCIÓN TERCERA.— DEL RÉGIMEN ELECTORAL Y MOCIÓN DE CENSURA](#id_sección_tercera_del_régimen_electoral_y_moción_de_censura "#id_sección_tercera_del_régimen_electoral_y_moción_de_censura")

#### [Artículo 24.— Elección del órgano de representación.](#id_artículo_24_elección_del_órgano_de_representación "#id_artículo_24_elección_del_órgano_de_representación")

Los miembros de la Junta Directiva serán elegidos entre los asociadas y asociados mediante sufragio universal, libre, directo y secreto.

Procederá la convocatoria de elecciones en los siguientes casos:

1. Por expiración del mandato.
2. En caso de prosperar una moción de censura en Asamblea General Extraordinaria por mayoría absoluta.
3. En caso de cese, por cualquier causa, de la mayoría de los miembros de la Junta Directiva.

#### [Artículo 25.— Junta Electoral y Calendario](#id_artículo_25_junta_electoral_y_calendario "#id_artículo_25_junta_electoral_y_calendario")

Concluido el mandato de la Junta Directiva o aprobada una moción de censura, en el plazo de ***30 días***, la persona que ejerza la Presidencia en funciones, convocará elecciones y constituirá la Junta Electoral, que estará formada por dos personas que, voluntariamente, se presten para esta función.

Estas personas no podrán formar parte de ninguna de las candidaturas presentadas.

En caso de no presentarse voluntarios, formarán la citada Junta los dos asociados de mayor y de menor edad.

Corresponde a la Junta Electoral:

1. Organizar las elecciones, resolviendo sobre cualquier asunto que atañe a su desarrollo.
2. Aprobar definitivamente el censo electoral.
3. Resolver las impugnaciones que se presenten en relación con el proceso electoral.

#### [Artículo 26.— Calendario Electoral](#id_artículo_26_calendario_electoral "#id_artículo_26_calendario_electoral")

El plazo entre la convocatoria de elecciones y la celebración de las mismas, no sobrepasará los ***treinta días hábiles***, siendo los ***cinco primeros días hábiles*** de exposición de la lista de personas asociadas con derecho a voto. En dicho período podrá impugnarse la lista.

Transcurrido el plazo de exposición e impugnación, en los ***tres días siguientes*** se resolverán las impugnaciones al censo y se procederá a la aprobación definitiva del censo electoral.

En los ***doce días siguientes*** podrán presentarse las candidaturas. Una vez concluido el plazo de presentación de candidaturas, en los ***cinco días hábiles*** siguientes se resolverá sobre la validez de las mismas y su proclamación definitiva.

Si no se presenta candidatura alguna, se convocarán, por parte de la persona que ocupe la Presidencia en funciones, nuevamente elecciones, en el plazo máximo de ***quince días*** desde el momento de cierre del plazo de presentación de aquellas.

#### [Artículo 27.— Moción de censura.](#id_artículo_27_moción_de_censura "#id_artículo_27_moción_de_censura")

La moción de censura a la Junta Directiva, deberá ser tratada por la Asamblea General, siempre que hubiese sido solicitada, mediante escrito razonado, como mínimo, por ***un tercio*** de las personas asociadas.

A tal efecto deberá ser convocada en el plazo de ***diez días hábiles*** desde que se formalice la solicitud.

Será precisa la mayoría absoluta de los asociados, presentes o representados, en Asamblea General Extraordinaria, para que prospere la moción de censura.

Caso de prosperar, la Presidencia en funciones deberá convocar elecciones en el plazo máximo de ***cinco días***, si bien continuará en funciones hasta que tome posesión la nueva Junta Directiva que resulte proclamada definitivamente en las elecciones.

[CAPÍTULO III.— DE LAS PERSONAS ASOCIADAS.](#id_capítulo_iii_de_las_personas_asociadas "#id_capítulo_iii_de_las_personas_asociadas")
------------------------------------------------------------------------------------------------------------------------------------

#### [Artículo 28.— Asociadas y asociados.](#id_artículo_28_asociadas_y_asociados "#id_artículo_28_asociadas_y_asociados")

Podrán pertenecer a la Asociación:

1. Las personas físicas, mayores de edad, con capacidad de obrar, no sujetas a ninguna condición legal para el ejercicio del derecho de asociación, que hayan cursado estudios o formado parte del personal docente o no docente «del centro».
2. Las personas jurídicas.

#### [Artículo 29.— Procedimiento de admisión.](#id_artículo_29_procedimiento_de_admisión "#id_artículo_29_procedimiento_de_admisión")

La condición de persona asociada se adquirirá, de forma provisional, a solicitud de la persona interesada, por escrito, dirigido a la Junta Directiva, manifestando su voluntad de contribuir al logro de los fines asociativos así como la aceptación de sus estatutos y reglamentos.

La Presidencia o la Secretaría, deberán entregar, a la persona interesada, constancia escrita de su solicitud e incluirá, en el orden del día de la próxima reunión de la Junta Directiva, la relación de todas las solicitudes presentadas.

Una vez comprobado el cumplimiento de los requisitos, podrá admitirse con carácter provisional, hasta su ratificación en la primera Asamblea General que se celebre, siendo, hasta ese momento, miembro con voz pero sin voto y correspondiendo a la Asamblea ratificar su admisión definitiva y con plenos derechos.

#### [Artículo 30.— Clases de Asociados.](#id_artículo_30_clases_de_asociados "#id_artículo_30_clases_de_asociados")

Las personas asociadas pueden ser:

1. Fundadoras o fundadores: Quienes suscribieron el Acta Fundacional de la Asociación.
2. De número: Quienes hayan ingresado con posterioridad a la firma del Acta Fundacional de la Asociación y se les admita como tales, conforme a estos Estatutos.
3. Honoríficas u honoríficos: Quienes por decisión de la Asamblea General, y a propuesta de sus miembros, se destaquen por su labor en la asociación o personas que sin ser miembros se distingan por su trabajo y compromiso social.
4. Colaboradores: personas físicas o jurídicas que, no cumpliendo los requisitos establecidos en el [Artículo 28](#articulo-28 "#articulo-28"), colaboren de forma notable en el desarrollo de los fines de la Asociación, o quienes destaquen por ayudar con medios económicos y materiales a sus actividades.

#### [Artículo 31.— Derechos de las asociadas y asociados, fundadores y/o de número.](#id_artículo_31_derechos_de_las_asociadas_y_asociados_fundadores_yo_de_número "#id_artículo_31_derechos_de_las_asociadas_y_asociados_fundadores_yo_de_número")

Las asociadas y asociados fundadores y/o de número tendrán los siguientes derechos:

1. Asistir, participar y votar en las Asambleas Generales.
2. Formar parte de los órganos de la Asociación.
3. Tener información del desarrollo de las actividades de la entidad, de su situación patrimonial y de la identidad de los asociados.
4. Participar en los actos de la Asociación.
5. Conocer los estatutos, los reglamentos y normas de funcionamiento de la Asociación.
6. Consultar los Libros de la Asociación, conforme a las normas que determinen su acceso a la documentación de la entidad.
7. Renunciar a la condición de socio.
8. Ser oída u oído con carácter previo a la adopción de medidas disciplinarias contra su persona y a ser informada o informado de los hechos que den lugar a tales medidas, debiendo ser motivado, en su caso, el acuerdo que imponga la sanción.
9. Impugnar los acuerdos de los órganos de la asociación, cuando los estimen contrarios a la Ley o a los Estatutos, haciendo uso de los procedimientos que, a tal fin, se establezcan en el Reglamento de Régimen Interno.

#### [Artículo 32.— Obligaciones de las asociadas y asociados, fundadores y/o de número.](#id_artículo_32_obligaciones_de_las_asociadas_y_asociados_fundadores_yo_de_número "#id_artículo_32_obligaciones_de_las_asociadas_y_asociados_fundadores_yo_de_número")

Serán obligaciones de las asociadas y asociados fundadores y/o de número :

1. Compartir las finalidades de la asociación y colaborar para la consecución de las mismas.
2. Pagar las cuotas, derramas y otras aportaciones que se determinen mediante acuerdo adoptado por la Asamblea General.
3. Cumplir el resto de obligaciones que resulten de las disposiciones estatutarias.
4. Acatar y cumplir los acuerdos válidamente adoptados por los órganos de gobierno y representación de la Asociación.

#### [Artículo 33.— Asociadas y asociados de honor.](#id_artículo_33_asociadas_y_asociados_de_honor "#id_artículo_33_asociadas_y_asociados_de_honor")

Las asociadas y asociados honoríficos tienen derecho a participar en las actividades de la Asociación, así como a asistir a las Asambleas, con derecho a voz, pero no a voto, salvo cuando ostenten también la condición de socios numerarios.

#### [Artículo 34.— Pérdida de la cualidad de persona asociada.](#id_artículo_34_pérdida_de_la_cualidad_de_persona_asociada "#id_artículo_34_pérdida_de_la_cualidad_de_persona_asociada")

Se perderá la condición de persona asociada:

1. Por voluntad de la persona, manifestada por escrito a la Junta Directiva.
2. Por acuerdo adoptado por el órgano competente de la Asociación, conforme al régimen disciplinario establecido en el [Capítulo 4](#capitulo-4 "#capitulo-4") de estos Estatutos.

[CAPÍTULO IV- RÉGIMEN DISCIPLINARIO: INFRACCIONES, SANCIONES, PROCEDIMIENTO Y PRESCRIPCIÓN.](#id_capítulo_iv_régimen_disciplinario_infracciones_sanciones_procedimiento_y_prescripción "#id_capítulo_iv_régimen_disciplinario_infracciones_sanciones_procedimiento_y_prescripción")
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#### [Artículo 35.— Normas generales.](#id_artículo_35_normas_generales "#id_artículo_35_normas_generales")

En el ejercicio de la potestad disciplinaria se respetarán los siguientes principios generales:

* Proporcionalidad con la gravedad de la infracción, atendiendo a la naturaleza de los hechos, las consecuencias de la infracción y la concurrencia de circunstancias atenuantes o agravantes.
* Inexistencia de doble sanción por los mismos hechos.
* Aplicación de los efectos retroactivos favorables.
* Prohibición de sancionar por infracciones no tipificadas en el momento de su comisión.

La responsabilidad disciplinaria se extingue en todo caso por:

* El cumplimiento de la sanción.
* La prescripción de la infracción.
* La prescripción de la sanción.
* El fallecimiento del infractor.

Para la imposición de las correspondientes sanciones disciplinarias se tendrán en cuenta las circunstancias agravantes de la reincidencia y atenuante de arrepentimiento espontáneo.

Hay reincidencia cuando la persona autora de la falta hubiese sido sancionada anteriormente por cualquier infracción de igual gravedad, o por dos o más que lo fueran de menor.

La reincidencia se entenderá producida en el transcurso de un año, contado a partir de la fecha en que se haya cometido la primera infracción.

#### [Artículo 36.— Infracciones.](#id_artículo_36_infracciones "#id_artículo_36_infracciones")

Las infracciones contra el buen orden social susceptibles de ser sancionadas se clasifican en leves, graves y muy graves.

#### [Artículo 37.— Infracciones Muy Graves.](#id_artículo_37_infracciones_muy_graves "#id_artículo_37_infracciones_muy_graves")

Tienen la consideración de infracciones disciplinarias muy graves:

1. Todas aquellas actuaciones que perjudiquen u obstaculicen la consecución de los fines de la Asociación, cuando tengan consideración de muy graves.
2. El incumplimiento o las conductas contrarias a las disposiciones estatutarias y/o reglamentarias de la Asociación, cuando se consideren como muy graves.
3. El incumplimiento de los acuerdos válidamente adoptados por los órganos de la Asociación, cuando se consideren muy graves.
4. La protesta o actuaciones airadas y ofensivas que impidan la celebración de asambleas o reuniones de la Junta Directiva.
5. Participar, formular o escribir, mediante cualquier medio de comunicación social, manifestaciones que perjudiquen de forma muy grave la imagen de la asociación.
6. La usurpación ilegítima de atribuciones o competencias sin contar con la preceptiva autorización del órgano competente de la entidad.
7. Agredir, amenazar o insultar gravemente a cualquier a cualquier miembro de la Asociación.
8. La inducción o complicidad, plenamente probada, a cualquier persona asociada, en la comisión de las faltas contempladas como muy graves.
9. El quebrantamiento de sanciones impuestas por falta grave o muy grave.
10. Todas las infracciones tipificadas como leves o graves y cuyas consecuencias físicas, morales o económicas, plenamente probadas, sean consideradas como muy graves.
11. En general, las conductas contrarias al buen orden social, cuando se consideren muy graves.

#### [Artículo 38.— Infracciones graves.](#id_artículo_38_infracciones_graves "#id_artículo_38_infracciones_graves")

Son infracciones punibles dentro del orden social y serán consideradas como graves:

1. El quebrantamiento de sanciones impuestas por infracciones leves.
2. Participar, formular o escribir mediante cualquier medio de comunicación social, manifestaciones que perjudiquen de forma grave la imagen de la Asociación.
3. La inducción o complicidad, plenamente probada, de cualquier asociado en la comisión de cualquiera de las faltas contempladas como graves.
4. Todas las infracciones tipificadas como leves y cuyas consecuencias físicas, morales o económicas, plenamente probadas, sean consideradas graves.
5. La reiteración de una falta leve.
6. El incumplimiento o las conductas contrarias a las disposiciones estatutarias y/o reglamentarias de la Asociación, cuando se consideren como graves.
7. El incumplimiento de los acuerdos válidamente adoptados por los órganos de la Asociación, cuando tengan la consideración de grave.
8. En general, las conductas contrarias al buen orden social, cuando se consideren como graves.

#### [Artículo 39.— Infracciones Leves.](#id_artículo_39_infracciones_leves "#id_artículo_39_infracciones_leves")

Se consideran infracciones disciplinarias leves:

1. La falta de asistencia durante ***tres ocasiones*** a las Asambleas Generales, sin justificación alguna.
2. El impago de ***tres cuotas consecutivas***, salvo que exista causa que lo justifique a criterio de la Junta Directiva.
3. Todas aquellas conductas que impidan el correcto desarrollo de las actividades propias de la asociación, cuando tengan la consideración de leve.
4. El maltrato de los bienes muebles o inmuebles de la Asociación.
5. Toda conducta incorrecta en las relaciones entre las personas asociadas.
6. La inducción o complicidad, plenamente probada, de cualquier persona asociada en la comisión de las faltas contempladas como leves.
7. El incumplimiento o las conductas contrarias a las disposiciones estatutarias y/o reglamentarias de la entidad, cuando se consideren como leves.
8. En general, las conductas contrarias al buen orden social, cuando se consideren como leves.

#### [Artículo 40.— Infracciones de las personas que integran la Junta Directiva.](#id_artículo_40_infracciones_de_las_personas_que_integran_la_junta_directiva "#id_artículo_40_infracciones_de_las_personas_que_integran_la_junta_directiva")

Se consideran infracciones muy graves susceptibles de ser cometidas por las personas que forman parte de la Junta Directiva:

1. La no convocatoria en los plazos y condiciones legales, de forma sistemática y reiterada, de los órganos de la asociación.
2. La incorrecta utilización de los (fondos)) de la entidad.
3. El abuso de autoridad y la usurpación ilegítima de atribuciones o competencias.
4. La inactividad o dejación de funciones que suponga incumplimiento muy grave de sus deberes estatutarios y/o reglamentarios.
5. La falta de asistencia, en ***tres ocasiones*** y sin causa justificada, a las reuniones de la Junta Directiva.

Se consideran infracciones graves:

1. No facilitar a las personas asociadas la documentación de la entidad que por estos le sea requerida (estatutos, actas, normas de régimen interno, etc.).
2. No facilitar el acceso de las personas asociadas a la documentación de la entidad.
3. La inactividad o dejación de funciones cuando causen perjuicios de carácter grave al correcto funcionamiento de la entidad.

Tienen la consideración de infracciones leves:

1. La inactividad o dejación de funciones, cuando no tengan la consideración de muy grave o grave.
2. La no convocatoria de los órganos de la asociación en los plazos y condiciones legales.
3. Las conductas o actuaciones contrarias al correcto funcionamiento de la Junta Directiva.
4. La falta de asistencia a una reunión de la Junta Directiva, sin causa justificada.

#### [Artículo 41.— Sanciones.](#id_artículo_41_sanciones "#id_artículo_41_sanciones")

Las sanciones susceptibles de aplicación por la comisión de infracciones muy graves, relacionadas en el [artículo 37](#articulo-37 "#articulo-37"), serán la pérdida de la condición de asociado o la suspensión temporal en tal condición durante un período ***de un año a cuatro años***, en adecuada proporción a la infracción cometida.

Las infracciones graves, relacionadas en el [artículo 38](#articulo-38 "#articulo-38"), darán lugar a la suspensión temporal en la condición de persona asociada durante un período ***de un mes a un año***.

La comisión de las infracciones de carácter leve dará lugar, por lo que a las relacionadas en el [artículo 39](#articulo-39 "#articulo-39") se refieren, a la amonestación o a la suspensión temporal de la persona asociada, por un período de ***un mes***.

Las infracciones señaladas en el [artículo 40](#articulo-40 "#articulo-40") darán lugar, en el caso de las muy graves, al cese en sus funciones de miembro de la Junta Directiva y, en su caso, a la inhabilitación para ocupar nuevamente cargos en el órgano de gobierno; en el caso de las graves, el cese durante un período ***de un mes a un año***, y si la infracción cometida tiene el carácter de leve en la amonestación o suspensión por el período de ***un mes***.

#### [Artículo 42.— Procedimiento sancionador.](#id_artículo_42_procedimiento_sancionador "#id_artículo_42_procedimiento_sancionador")

Para la adopción de las sanciones señaladas en los artículos anteriores, se tramitará un expediente disciplinario en el cual, de acuerdo con el [artículo 31](#articulo-31 "#articulo-31") de estos estatutos, la persona asociada tiene derecho a ser escuchada con carácter previo a la adopción de medidas disciplinarias contra su persona y a ser informada o informado de los hechos que den lugar a tales medidas, debiendo ser motivado, en su caso, el acuerdo que imponga la sanción.

La instrucción de los procedimientos sancionadores corresponde a la Junta Directiva, nombrándose, a tal efecto por esta, las personas de la misma que tengan encomendada dicha función.

Caso de tramitarse un expediente contra una persona perteneciente a la Junta Directiva, dicha persona no podrá formar parte del órgano instructor, debiendo abstenerse de intervenir y votar en la reunión de la Junta Directiva que decida la resolución provisional del mismo.

La persona que presida y la que ejerza la secretaría del órgano instructor de los procedimientos disciplinarios, realizarán las diligencias previas que consideren oportunas para obtener información sobre la posible infracción cometida por la persona asociada.

A la vista de esta información, la Junta Directiva podrá mandar archivar las actuaciones o acordar la incoación del expediente disciplinario.

En este último caso, la persona que ejerza la Secretaría del órgano instructor, pasará a la persona expedientada un escrito en el que se le comunicarán los cargos que se le imputan. La persona expedientada podrá contestar a dichos cargos, alegando en su defensa lo que estime oportuno en el plazo ***de 15 días***. Transcurrido dicho plazo, se pasará el asunto a la primera sesión de la Junta Directiva, la cual acordará lo que proceda. El acuerdo se adoptará por mayoría absoluta de las personas integrantes de dicha Junta Directiva.

La resolución que se adopte tendrá carácter provisional. La persona asociada podrá presentar recurso ante la Asamblea General en un plazo de ***quince días***, a contar desde el día siguiente al de la recepción de la resolución. Si no se presenta recurso en dicho plazo, la resolución se convierte en firme.

La Asamblea General adoptará la resolución que proceda en relación con el expediente disciplinario o sancionador.

#### [Artículo 43.— Prescripción.](#id_artículo_43_prescripción "#id_artículo_43_prescripción")

##### [De las infracciones](#id_de_las_infracciones "#id_de_las_infracciones")

Las infracciones prescribirán a los ***tres años***, ***al año*** o ***al mes***, según se trate de las *muy graves*, *graves* o *leves*, comenzándose a contar el plazo de prescripción al ***día siguiente*** a la comisión de la infracción.

El plazo de prescripción se interrumpirá por la iniciación del procedimiento sancionador, con conocimiento del interesado, pero si este permaneciese paralizado durante ***un mes*** por causa no imputable al asociado, volverá a correr el plazo correspondiente.

##### [De las sanciones](#id_de_las_sanciones "#id_de_las_sanciones")

Las sanciones prescribirán a los ***tres años***, ***al año*** o ***al mes***, según se trate de las que correspondan a infracciones *muy graves*, *graves* o *leves*, comenzándose a contar el plazo de prescripción desde el día siguiente a aquel en que adquiera firmeza la resolución por la que se impuso la sanción.

[CAPÍTULO V.— LIBROS Y DOCUMENTACIÓN.](#id_capítulo_v_libros_y_documentación "#id_capítulo_v_libros_y_documentación")
---------------------------------------------------------------------------------------------------------------------

#### [Artículo 44.— Libros y documentación contable.](#id_artículo_44_libros_y_documentación_contable "#id_artículo_44_libros_y_documentación_contable")

La Asociación dispondrá de un Libro de Registro de Socias y Socios, y de aquellos Libros de Contabilidad que permitan obtener la imagen fiel del patrimonio, del resultado y de la situación financiera de la entidad.

Llevará también un Libro de Actas de las reuniones de la Asamblea general y de la Junta Directiva, en las que constarán, al menos:

1. Todos los datos relativos a la convocatoria y a la constitución del órgano.
2. Un resumen de los asuntos debatidos.
3. Las intervenciones de las que se haya solicitado constancia.
4. Los acuerdos adoptados.
5. Los resultados de las votaciones.
6. Un registro de las infracciones disciplinarias y sus resoluciones.

#### [Artículo 45.— Derecho de acceso a los libros y documentación.](#id_artículo_45_derecho_de_acceso_a_los_libros_y_documentación "#id_artículo_45_derecho_de_acceso_a_los_libros_y_documentación")

La Junta Directiva, encargada de la custodia y llevanza de los libros, deberá tener a disposición de los socios y socias, los libros y documentación de la entidad, facilitando el acceso por su parte.

A tal efecto, una vez recibida la solicitud por parte de la Presidencia, se pondrá a disposición de la persona solicitante en el plazo máximo de ***diez días***.

[CAPÍTULO VI.— RÉGIMEN ECONÓMICO.](#id_capítulo_vi_régimen_económico "#id_capítulo_vi_régimen_económico")
---------------------------------------------------------------------------------------------------------

#### [Artículo 46.— Patrimonio Inicial.](#id_artículo_46_patrimonio_inicial "#id_artículo_46_patrimonio_inicial")

La Asociación no cuenta, en el momento de su constitución, con patrimonio inicial ni bienes.

#### [Artículo 47.— Ejercicio económico.](#id_artículo_47_ejercicio_económico "#id_artículo_47_ejercicio_económico")

El ejercicio económico será ***anual*** y su cierre tendrá lugar el ***31 de diciembre*** de cada año.

#### [Artículo 48.— Recursos económicos.](#id_artículo_48_recursos_económicos "#id_artículo_48_recursos_económicos")

Constituirán los recursos económicos de la Asociación:

* Las cuotas de los miembros, periódicas o extraordinarias.
* Las aportaciones, subvenciones, donaciones a título gratuito, herencias y legados recibidos.
* Bienes muebles e inmuebles.
* Cualquier otro recurso susceptible de valoración económica y admitido en derecho.

[CAPÍTULO VII.— MODIFICACIÓN DE ESTATUTOS Y NORMAS DE RÉGIMEN INTERNO.](#id_capítulo_vii_modificación_de_estatutos_y_normas_de_régimen_interno "#id_capítulo_vii_modificación_de_estatutos_y_normas_de_régimen_interno")
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#### [Artículo 49.— Modificación de Estatutos.](#id_artículo_49_modificación_de_estatutos "#id_artículo_49_modificación_de_estatutos")

Los Estatutos de la Asociación podrán ser modificados cuando resulte conveniente a los intereses de la misma, por acuerdo de la Asamblea General convocada *específicamente* al efecto.

El acuerdo de modificar los estatutos requiere mayoría absoluta de los socios presentes o representados.

#### [Artículo 50.— Normas de régimen interno.](#id_artículo_50_normas_de_régimen_interno "#id_artículo_50_normas_de_régimen_interno")

Los presentes estatutos podrán ser desarrollados mediante normas de régimen interno, aprobadas por acuerdo de la Asamblea General por mayoría simple de los socios presentes o representados.

A tal efecto, una vez constituida formalmente la Asociación y en el plazo de ***seis meses***, la Junta Directiva deberá presentar una propuesta de ***Reglamento de Régimen Interno*** que contendrá, entre otros, los procedimientos que regulen los procesos de reclamación e impugnación de las decisiones de sus órganos así como las actuaciones relacionadas con los procesos disciplinarios y sancionadores.

[CAPÍTULO VIII.— DISOLUCIÓN DE LA ASOCIACIÓN.](#id_capítulo_viii_disolución_de_la_asociación "#id_capítulo_viii_disolución_de_la_asociación")
---------------------------------------------------------------------------------------------------------------------------------------------

#### [Artículo 51.— Causas.](#id_artículo_51_causas "#id_artículo_51_causas")

La Asociación puede disolverse:

* Por Sentencia judicial firme.
* Por acuerdo de la Asamblea General Extraordinaria.
* Por las causas determinadas en el [artículo 39 del Código Civil.](https://www.conceptosjuridicos.com/codigo-civil-articulo-39/ "https://www.conceptosjuridicos.com/codigo-civil-articulo-39/").

#### [Artículo 52.— Comisión Liquidadora.](#id_artículo_52_comisión_liquidadora "#id_artículo_52_comisión_liquidadora")

Acordada la disolución de la Asociación, la Asamblea General Extraordinaria designará a una Comisión Liquidadora que tendrá las siguientes funciones:

* Velar por la integridad del patrimonio de la asociación.
* Concluir las operaciones pendientes y efectuar las nuevas, que sean precisas para la liquidación de la Asociación.
* Cobrar los créditos de la entidad.
* Liquidar el patrimonio y pagar a los acreedores.
* Aplicar los bienes sobrantes a los fines previstos en los presentes Estatutos.
* Solicitar la inscripción de la disolución en el Registro de Asociaciones.

[DISPOSICIÓN FINAL.](#id_disposición_final "#id_disposición_final")
-------------------------------------------------------------------

Los presentes Estatutos han sido aprobados el mismo día de celebración del Acta Fundacional, de cuyo contenido dan testimonio y firman, al margen de cada una de las hojas que lo integran, las siguientes personas.

| Vº Bº Presidente/a | Secretario/a |
| --- | --- |
| <br><br><br><br>Firma | <br><br><br><br>Firma |

<br><br>

<p style="text-align:right">Las Palmas de Gran Canaria, a <ins>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</ins> de Enero de 2024</p>

----

[^preliminar]: Versión previa a los estatutos finales.
[^nombre]: El nombre de la asociación será elegido en el momento de celebrar al acto fundacional.
[^domicilio]: El domicilio social será definido antes de la constitución de la asociación.
