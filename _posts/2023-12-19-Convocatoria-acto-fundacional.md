---
title: Convocatoria, Acto Fundacional
description: Convocatoria para la celebración del Acto Fundacional de la nueva asociación.
# author: Luis Cabrera
author_profile: true
date: 2023-12-20 23:56:00 +0100
last_modified_at: 2023-12-20 23:56:00 +0100
locale: es
count_visit: true
related: true

header:
  teaser: assets/images/reunion-de-trabajo.jpg
  og_image: assets/images/reunion-de-trabajo.jpg
  overlay_image: assets/images/reunion-de-trabajo.jpg
  overlay_color: "#333"
  overlay_filter: 0.3
  caption: "Créditos: [Reuniones de trabajo](https://www.pxfuel.com/es/search?q=reuniones+de+trabajo), via [pxfuel](https://www.pxfuel.com/)"
  #actions:
  # - label: "Versión PDF"
  #   url: /assets/files/2023-12-14-Boletín-informativo-Resumen-encuesta.pdf

excerpt: |
  Convocatoria al Acto Fundacional de la asociación.
tagline: |
  Convocatoria al Acto Fundacional de la asociación.
  {: style="text-align: justify; font-size:1.25em; color: #ffffff; background-color: #88888880; opacity: 1; padding: 12px;"}

show_date: true
show_time: true
# show_author: true
categories:
  - La Laboral
tags:
  - estatutos
  - acto fundacional
toc: true
# toc_label: Índice de contenidos
toc_icon: cog
toc_sticky: true
---

## Convocatoria, Acto Fundacional

Queridos compis, por fin estamos en condiciones de crear una asociación y queremos contar contigo para ello. Por eso te mandamos el boletín [^1] con los resultados de la encuesta que nos reafirma en la oportunidad de crearla.

También te mandamos los estatutos [^2] que hemos redactado para que les eches un vistazo y nos mandes tus opiniones, sugerencias y comentarios.

Por otra parte, entendemos que todos debemos participar en la elección del nombre de la asociación y es por ello que te animamos a que nos envíes tus propuestas para que entren en la selección, por votación, que celebraremos durante la reunión a la que te convocamos.

Existen normas para validar la denominación de la asociación. Al final te dejamos un enlace para que las puedas leer[^3].

Asimismo, si lo ves interesante, te animamos a que presentes tu propia candidatura para formar la primera Junta Directiva de nuestra asociación.

Para formar una candidatura, tienes que presentar un equipo de *entre 5 a 15 personas*, que cumplan los requisitos que exponemos en los estatutos preliminares.

Al igual que pasará con el nombre de la asociación, celebraremos una votación entre los asistentes al acto fundacional del que deberá salir conformada, de entre las propuestas para junta directiva que nos hagan llegar, la que será la primera Junta Directiva de la asociación.

Es imprescindible la presencia de las personas que quieran formar parte de la junta directiva que se pueda formar in situ o de presentar candidaturas para la misma.

Sin embargo, lo más importante sería poder contar contigo para ponerla en marcha y que dicha asociación sea resultado de un sentimiento y esfuerzo colectivo, es decir, contar con que desde el principio formes parte, si quieres, del grupo fundador y/o de la primera junta directiva de la asociación.

Para ello esperamos que confirmes que puedes asistir a la asamblea fundacional el próximo *viernes*, *19 de enero de 2024*, a las *17 horas*, en el *Aula de emprendimiento* del *Edificio de Dirección* de la antigua Universidad Laboral de Las Palmas.

<p style="text-align:center">(Se ruega puntualidad, la reunión deberá finalizar antes de las 19:30h)</p>

<p style="text-align:right">Un saludo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

Equipo de Trabajo para la creación de la Asociación.[^4]

**Nota**: Podremos aparcar en los aparcamientos del centro.

----

[^1]: Boletín 1: [*https://icorrecam.gitlab.io/la%20laboral/Boletin-i-resultados-encuesta/*](https://icorrecam.gitlab.io/la%20laboral/Boletin-i-resultados-encuesta/)

[^2]: Estatutos preliminares: [*https://icorrecam.gitlab.io/la%20laboral/Estatutos-version-preliminar/*](https://icorrecam.gitlab.io/la%20laboral/Estatutos-version-preliminar/)

[^3]: Reglas para nombres de asociaciones válidos (Ver Título Primero, «DE LAS ASOCIACIONES», Capítulo Primero, «De la denominación de las asociaciones», artículos del 3 al 6, ambos incluidos.): <a href="https://www.gobiernodecanarias.org/libroazul/pdf/56334.pdf" class="bare" target="_blank">https://www.gobiernodecanarias.org/libroazul/pdf/56334.pdf</a>

[^4]: WhatsApp del equipo de trabajo: [*https://chat.whatsapp.com/KSmpR1HppV41QCIzr0ZEIb*](https://chat.whatsapp.com/KSmpR1HppV41QCIzr0ZEIb) (por si tienes dudas o quieres colaborar, puedes unirte. Es un grupo abierto.)
