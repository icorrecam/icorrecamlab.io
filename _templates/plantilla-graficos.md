---
title: "Plantilla para: Gráficos"   # MODIFICAR
description: Descripción del gráfico   # MODIFICAR
# author: Luis Cabrera
# show_author: true
date:             2023-08-07T20:00:00+01:00     # MODIFICAR
last_modified_at: 2023-08-07T23:59:59+01:00     # MODIFICAR
show_date: true
show_time: true
locale: es
related: true
count_visit: true
class: wide
excerpt: #
  Excerpt del gráfico
header:
  # cta_url: URL
  # cta_label: LABEL
  overlay_color: #000000
  # overlay_filter: 0.1
  overlay_filter: rgba(0,175,255,0.3)
  show_overlay_excerpt: false
  overlay_image: assets/images/graficos.jpg
  teaser: assets/images/graficos.jpg
  caption: "Créditos: [Estadísticas](https://www.pxfuel.com/es/search?q=estad%C3%ADsticas), via [pxfuel](https://www.pxfuel.com/)"
  # actions:
  # - label: "Documento original"
  #   url: /assets/files/ESTATUTOS-UNIVERSIDAD-LABORAL-2006.pdf
categories:
  - La Laboral
tags:
  - diagramas
  - gráficos
  - infografías
---

## TagLine descriptiva

<!-- ![](/assets/images/cronograma-asamblea-general-ordinaria.svg)  -->
<!--
<figure>
 <a href="/assets/images/cronograma-asamblea-general-ordinaria.svg"><img src="/assets/images/cronograma-asamblea-general-ordinaria.svg"></a>
 <figcaption>Plazos, según los actuales estatutos, para organizar una Asambles General Ordinaria</figcaption>
</figure>
-->

{% include figure image_path="/assets/images/asociacion-vieja-vs-nueva-v2.webp" alt="Infografía informativa. Comparativa entre seguir con la antigua asociación, o crear una nueva, desde cero." caption="Infografía informativa. Comparativa entre seguir con la antigua asociación, o crear una nueva, desde cero." %}