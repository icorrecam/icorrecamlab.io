---
title: "Plantilla para: Reunión del grupo de trabajo"   # MODIFICAR
description: Reunión de trabajo, por videoconferencia   # MODIFICAR
# author: Luis Cabrera
# show_author: true
date:             2023-07-28T20:00:00+01:00     # MODIFICAR
last_modified_at: 2023-08-06T23:59:59+01:00     # MODIFICAR
show_date: true
show_time: true
locale: es
related: true
count_visit: true
excerpt: #
  Reunión del grupo de trabajo, por videoconferencia.
header:
  # cta_url: URL
  # cta_label: LABEL
  overlay_color: #FFFFFF
  # overlay_filter: 0.1
  overlay_filter: rgba(39,112,148,0.65)
  show_overlay_excerpt: false
  # overlay_image: https://upload.wikimedia.org/wikipedia/commons/b/bc/Michelle_Bachelet_visita_junta_de_vecinos_N%C2%BA_34_de_la_comuna_de_Lo_Espejo_%288672268049%29.jpg
  overlay_image: assets/images/reunion-de-trabajo.jpg
  # teaser: https://upload.wikimedia.org/wikipedia/commons/b/bc/Michelle_Bachelet_visita_junta_de_vecinos_N%C2%BA_34_de_la_comuna_de_Lo_Espejo_%288672268049%29.jpg
  teaser: assets/images/reunion-de-trabajo.jpg
  # caption: "Créditos: [Michelle Bachelet from Santiago, Chile, CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0), via Wikimedia Commons"
  caption: "Créditos: [Reuniones de trabajo](https://www.pxfuel.com/es/search?q=reuniones+de+trabajo), via [pxfuel](https://www.pxfuel.com/)"
  # actions:
  # - label: "Documento original"
  #   url: /assets/files/ESTATUTOS-UNIVERSIDAD-LABORAL-2006.pdf
toc: true
# toc_label: Índice de contenidos
toc_icon: cog
toc_sticky: true
categories:
  - La Laboral
tags:
  - videoconferencia
  - grupo de trabajo
  - reuniones
  - actas
---

## Reunión del grupo de trabajo  <!-- MODIFICAR, SI ES NECESARIO -->

<!--
Referencias:  
 - https://sonix.ai/resources/es/como-recordar-los-minutos-de-la-reunion/
 - https://www.appvizer.es/revista/organizacion-planificacion/gestion-proyectos/como-redactar-un-acta-de-reunion

Organización del contenido de una plantilla par actas
  
- Datos iniciales (a figurar en el acta)
    - La fecha (y lugar) de la reunión
    - Los nombres de los asistentes (incluidos los invitados)
    - Los nombres de los miembros ausentes (¿necesario?)
    - Una llamada al orden que enumere la hora de inicio de la reunión y cuál es el objetivo general  

- Inicio y desarrollo de la reunión**  

    - Orden del día  
        - Primer punto: es el indicado para contener apartados como  
            - La validación del acta de la reunión anterior.
            - El recuento de tareas y sus resultados.
            - Las acciones que quedaron pendientes,
            - Otros aspectos que requieren validación por parte de los asistentes a la reunión.
        - Punto 2 del orden del día.    

            Cada punto podría contener, inicialmente, información como la siguiente:  

            - Resumen
            - Un (pro)ponente o encargado de presentar.
            - En su caso, las mociones presentadas.
            - Otros detalles (los que se consideren necesarios; referencias, documentación, etc).

            Una vez terminado el punto, la información final de dicho punto podría contener:  

            - Resultado.
            - Acciones propuestas.
                - Acción
                - Encargado.

        - Puntos adicionales del orden del día
            -  Siguiendo el criterio mostrado en el desarrollo el punto anterior.
        - ...  
        - Último punto del día.  

              Si las reuniones no son periódicas, en este punto debería fijarse, antes que nada, la fecha y hora para la celebración de la siguiente reunión.  

              Si así se considera, este podría ser un punto abierto a temas que no hayan entrado en el orden del día.  

- Cierre de la reunión

- Datos de los que hay que dejar constancia tras la finalización de la reunión:
    - La hora de finalización de la reunión
    - Fecha y hora de la próxima reunión
    - Lista de los documentos distribuidos durante la reunión
    - Modificación o aceptación del acta de la reunión anterior
    - Mociones aceptadas o rechazadas
    - Resultados de las votaciones o elecciones

-->  

### 1\. Datos identificativos

| --              | --                |
|**Localización** | Ubicación/Sistema |
|**Fecha**        | dd/mm/AAAA        |
|**Inicio**       | 20:00h            |
|**Fin**          | 21:00h            |

----

| Participantes       |
| :------------------ |
| Fulano [^moderador] |
| Mengano             |
| Zetano              |

----

### 2\. Puntos del orden del día

#### Punto 1.- Aprobación del acta anterior y resumen de tareas pendientes

**La validación del acta de la reunión anterior**
: - El recuento de tareas y sus resultados.
: - Las acciones que quedaron pendientes,

**Fijar la siguiente reunión**
: Si las reuniones no son periódicas, en este punto debería fijarse, antes que nada, la fecha y hora para la celebración de la siguiente reunión.  

#### Punto 2.- Título del punto

**Impulsor:**
: Nombre de la persona que propone este punto

**Resumen:**  
: Lorem fistrum va usté muy cargadoo qué dise usteer ese pedazo de a peich.
: Tiene musho peligro nostrud laboris por la gloria de mi madre ese que llega et a gramenawer.  
: Occaecat hasta luego Lucas diodenoo officia cillum ese pedazo de apetecan adipisicing.
: Occaecat está la cosa muy malar torpedo caballo blanco caballo negroorl ese que llega occaecat.  
: Jarl aliqua de la pradera te voy a borrar el cerito de la pradera.

**Referencias:**  
: Cualquier lectura recomendada para entender mejor lo propuesto en este apartado.

##### Desarrollo del punto

Incluir aquí los detalles que surjan durante el debate de este apartado

**Conclusión:**
: Lorem fistrum hasta luego Lucas ese que llega diodenoo se calle ustée me cago en tus muelas está la cosa muy malar al ataquerl a gramenawer.  
: Jarl al ataquerl pecador qué dise usteer por la gloria de mi madre te va a hasé pupitaa jarl pecador de la pradera ahorarr al ataquerl.
: Sexuarl está la cosa muy malar llevame al sircoo no puedor a gramenawer. No te digo trigo por no llamarte Rodrigor va usté muy cargadoo a wan pecador mamaar a gramenawer la caidita se calle ustée a peich no puedor.
: Hasta luego Lucas por la gloria de mi madre torpedo torpedo no te digo trigo por no llamarte Rodrigor por la gloria de mi madre caballo blanco caballo negroorl se calle ustée.

##### Resultado

- Aprobado o no aprobado
- Resultado de la votación

Acciones propuestas:
  
**Acción:**
: Lorem fistrum al ataquerl ese hombree va usté muy cargadoo amatomaa no puedor a wan está la cosa muy malar. Pecador a peich te voy a borrar el cerito pupita al ataquerl ese que llega hasta luego Lucas diodenoo ese pedazo de caballo blanco caballo negroorl benemeritaar. Papaar papaar ese hombree no puedor al ataquerl papaar papaar.

**Persona encargada:**  
: Torpedo al ataquerl te voy a borrar el cerito no puedor

#### Punto 3.- Título del punto

Revisar las opciones del punto anterior

#### Punto 4.- Título del punto

Revisar las opciones del punto anterior

#### Punto 5.- Título del punto

Revisar las opciones del punto anterior

#### Punto final: ruegos y preguntas

**Otros temas a tratar**
: Si así se considera, este podría ser un punto abierto a temas que no hayan entrado en el orden del día.  

- Recoger lo que se trate en este apartado.

### 3\. Resumen final

El resumen final debe contener algo parecido a esto:

- La hora de finalización de la reunión
- Fecha y hora de la próxima reunión
- Lista de los documentos distribuidos durante la reunión
- Modificación o aceptación del acta de la reunión anterior
- Mociones aceptadas o rechazadas
- Resultados de las votaciones o elecciones

----
[^moderador]: Actua como moderador de la reunión.
