#!/usr/bin/env bash

# Comando útiles
# jekyll build: Genera el sitio estático en el directorio _site.
# jekyll serve: Construye el sitio e inicia un servidor web en tu máquina local, permitiéndote previsualizar el sitio en tu navegador en http://localhost:4000.
# jekyll new [site name]: Crea un nuevo sitio Jekyll en un nuevo directorio con el nombre de sitio especificado.
# jekyll doctor: Muestra cualquier problema de configuración o dependencia que pueda haber.
# jekyll clean: Elimina el directorio _site, que es donde se almacenan los archivos del sitio generado.
# jekyll help: Muestra la documentación de ayuda de Jekyll.
# jekyll serve --draft: Genera y sirve tu sitio Jekyll, incluidas las entradas que estén en el directorio _drafts.

# export JEKYLL_ENV=production
export JEKYLL_ENV=development

HOST="0.0.0.0"
PORT=4000

DEVSERVER() {
  bundle exec jekyll clean
  bundle exec jekyll doctor

  # bundle exec jekyll serve --host "${HOST}" --port "${PORT}" --draft --future --livereload
  bundle exec jekyll serve --host "${HOST}" --port "${PORT}" --draft --future --livereload --verbose
  # bundle exec jekyll serve --host "${HOST}" --port "${PORT}" --draft --future --livereload --verbose --trace
}

clear

sudo apt install python3 -y
sudo apt install python3-pip -y
sudo apt install python3.11-venv -y
sudo apt install apt-utils -y
sudo apt install sudo -y
sudo apt install graphviz cabal-install -y
sudo apt install plantuml ruby-asciidoctor-kroki ruby-asciidoctor-plantuml -y
python3 -m venv .venv
source .venv/bin/activate
pip3 install blockdiag seqdiag nwdiag
pip3 install actdiag
pip3 install "Pillow<=9.5.0"
pip3 install nodeenv
nodeenv -p
npm install -g mermaid.cli
npm install -g --force @mermaid-js/mermaid-cli
npm install -g nomnoml
npm install -g state-machine-cat
npm install -g vega-cli vega-lite
npm install -g wavedrom-cli
npm install -g markdownlint-cli
npm fund
node -v
npm -v
bundle install
# - bundle exec asciidoctor-revealjs -a revealjsdir=https://cdn.jsdelivr.net/npm/reveal.js@4.5.0 -o _slides/00000000/index.html _slides/00000000/index.adoc
bundle exec asciidoctor-revealjs -a revealjsdir=https://cdn.jsdelivr.net/npm/reveal.js@4.5.0 -o _slides/20230718/index.html _slides/20230718/index.adoc

clear

if [[ "${VIRTUAL_ENV}" != "" ]]; then
  # Lanzamos directamente Jekyll
  DEVSERVER
else
  # shellcheck source=/dev/null
  source ./.venv/bin/activate

  DEVSERVER

fi
