﻿#!/usr/bin/env python
"""_summary_

Returns:
    _type_: _description_

Yields:
    _type_: _description_
"""

import pathlib

# import sys


# folder path
DIR_PATH = r"_layouts"
EXT = ".html"
CADENA = '{% include'


def echo(text: str, repetitions: int = 3) -> str:
    """Imitate a real-world echo."""
    echoed_text = ""
    for i in range(repetitions, 0, -1):
        echoed_text += f"{text[-i:]}\n"
    return f"{echoed_text.lower()}."


def get_files(path, ext):
    """_summary_

    Args:
        path (_type_): _description_

    Returns:
        _type_: _description_
    """
    res = []
    directory = pathlib.Path(path)
    for entry in directory.iterdir():
        if entry.is_file() and entry.suffix == ext:
            res.append(entry)

    return res


def get_substrings(file, string):
    """_summary_

    https://www.computerhope.com/issues/ch001721.htm#

    Args:
        file (_type_): _description_
        string (_type_): _description_

    Returns:
        _type_: _description_
    """
    matches = []  # The list where we will store results.
    linenum = 0
    substr = string.lower()  # Substring to search for.
    with open(file, "rt") as myfile:
        for line in myfile:
            linenum += 1
            if line.lower().find(substr) != -1:  # if case-insensitive match,
                matches.append("Line " + str(linenum) + ": " + line.rstrip("\n"))

    # for err in matches:
    #    print(err)

    return matches


def main() -> None:
    """..."""
    res = get_files(path=DIR_PATH, ext=EXT)

    print(res)
    print(type(res))

    for file in res:
        get_substrings(file, CADENA)


if __name__ == "__main__":
    main()
